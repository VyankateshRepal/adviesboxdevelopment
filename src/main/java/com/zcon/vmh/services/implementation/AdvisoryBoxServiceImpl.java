package com.zcon.vmh.services.implementation;

import java.sql.SQLException;
import java.util.List;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcon.vmh.dao.interfaces.IAdvisoryBoxDao;
import com.zcon.vmh.dto.AdviseDataDto;
import com.zcon.vmh.dto.ConnectionTestDto;
import com.zcon.vmh.dto.HypoteekDataDto;
import com.zcon.vmh.dto.IncomeDataDto;
import com.zcon.vmh.dto.InsertContactDto;
import com.zcon.vmh.dto.PensionDataDto;
import com.zcon.vmh.dto.PersonalDataDto;
import com.zcon.vmh.dto.ProductsDataDto;
import com.zcon.vmh.dto.ProductsDataToSaveDTO;
import com.zcon.vmh.dto.ProductsDataToSaveUpdatedDTO;
import com.zcon.vmh.dto.SearchContactDto;
import com.zcon.vmh.dto.XMLDataDTO;
import com.zcon.vmh.services.interfaces.IAdvisoryBox;

@Service("advisoryBoxService")
public class AdvisoryBoxServiceImpl implements IAdvisoryBox {

	@Autowired
	IAdvisoryBoxDao advisoryBoxDao;

	@Override
	public String connectionTest(ConnectionTestDto connectionTestDto) throws SQLException {
		return advisoryBoxDao.connectionTest( connectionTestDto);
	}

	@Override
	public String insertContact(InsertContactDto insertContactDto) throws SQLException {
		return advisoryBoxDao.insertContact(insertContactDto);
	}

	@Override
	public JSONObject searchContact(SearchContactDto searchContactDto) {
		return advisoryBoxDao.searchContact(searchContactDto);
	}

	@Override
	public JSONObject personalData(PersonalDataDto personalDataDto) {
		return advisoryBoxDao.personalData(personalDataDto);
	}

	@Override
	public JSONObject pensionData(PensionDataDto pensionDataDto) {
		return advisoryBoxDao.pensionData( pensionDataDto);
	}

	@Override
	public JSONObject incomeData(IncomeDataDto incomeDataDto) {
		return advisoryBoxDao.incomeData( incomeDataDto);
	}

	@Override
	public JSONObject adviseData(AdviseDataDto adviseDataDto) {
		return advisoryBoxDao.adviseData( adviseDataDto);
	}

	@Override
	public JSONObject singleAdviseData(AdviseDataDto adviseDataDto) {
		return advisoryBoxDao.singleAdviseData( adviseDataDto);
	}

	@Override
	public JSONObject getProductsData(ProductsDataDto productsDataDto) {
		return  advisoryBoxDao.getProductsData( productsDataDto);
	}

	@Override
	public String savePersonalData(PersonalDataDto personalDataDto) throws SQLException {
		return advisoryBoxDao.savePersonalData( personalDataDto);
	}

	@Override
	public String saveProductData(ProductsDataToSaveDTO productsDataDto) throws SQLException {
		return advisoryBoxDao.saveProductData(   productsDataDto);
	}
	
	@Override
	public JSONObject getHypoteekData(HypoteekDataDto hypoteekDataDto) {
		// TODO Auto-generated method stub
		return advisoryBoxDao.getHypoteekData(   hypoteekDataDto);
	}

	@Override
	public JSONObject getProductsDataUpdated(ProductsDataDto productsDataDto) {
		// TODO Auto-generated method stub
		return advisoryBoxDao.getProductsDataUpdated(productsDataDto);
	}

	@Override
	public String saveProductDataUpdated(ProductsDataToSaveUpdatedDTO productsDataToSaveUpdatedDto) throws SQLException {
		// TODO Auto-generated method stub
		return advisoryBoxDao.saveProductDataUpdated( productsDataToSaveUpdatedDto);
	}

	@Override
	public String updateXmlData(XMLDataDTO xmlData) {
		// TODO Auto-generated method stub
		return advisoryBoxDao.updateXmlData(xmlData);
	}

	@Override
	public String saveProductDataUpdatedProposal(ProductsDataToSaveUpdatedDTO productsDataToSaveUpdatedDto) throws SQLException {
		// TODO Auto-generated method stub
		return advisoryBoxDao.saveProductDataUpdatedProposal(productsDataToSaveUpdatedDto);
	}

	@Override
	public JSONObject getKlnForAdviseData(AdviseDataDto adviseDataDto) {
		// TODO Auto-generated method stub
		return advisoryBoxDao.getKlnForAdviseData( adviseDataDto);
	}

	@Override
	public JSONObject getListOfAdviseData(AdviseDataDto adviseDataDto) {
		// TODO Auto-generated method stub
		return advisoryBoxDao.getListOfAdviseData(  adviseDataDto);
	}

	@Override
	public String checkServicesStatus() {
		// TODO Auto-generated method stub
		return advisoryBoxDao.checkServicesStatus();
	}

	@Override
	public JSONObject getPartnerNawAnvId(PersonalDataDto personalDataDto) {
		// TODO Auto-generated method stub
		return advisoryBoxDao.getPartnerNawAnvId(personalDataDto);
	}




}
