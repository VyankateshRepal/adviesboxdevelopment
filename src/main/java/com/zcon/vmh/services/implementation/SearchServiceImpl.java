package com.zcon.vmh.services.implementation;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcon.vmh.dao.interfaces.ISearchDao;
import com.zcon.vmh.dto.SearchParametersDto;
import com.zcon.vmh.services.interfaces.ISearchServices;

/**
 * @author Vyankatesh
 *
 */
@Service("blobSearchService")
public class SearchServiceImpl implements ISearchServices {

	@Autowired
	ISearchDao searchServiceDao;

	@Override
	public JSONObject listDatasources() {
		return searchServiceDao.listDatasources();
	}

	@Override
	public JSONObject createDatasource(JSONObject jsonInput) {
		return searchServiceDao.createDatasource(jsonInput);
	}

	@Override
	public JSONObject createIndex(JSONObject jsonInput) {
		return searchServiceDao.createIndex(jsonInput);
	}

	@Override
	public JSONObject createIndexer(JSONObject jsonInput) {
		return searchServiceDao.createIndexer(jsonInput);
	}

	@Override
	public JSONObject createIndexerFieldMappings(JSONObject jsonInput) {
		return searchServiceDao.createIndexerFieldMappings(jsonInput);
	}

	@Override
	public JSONObject search(SearchParametersDto searchInBlobDto) throws JSONException {
		return searchServiceDao.search( searchInBlobDto);
	}

	@Override
	public JSONObject inTable(SearchParametersDto searchInTableDto) {
		// TODO Auto-generated method stub
		return searchServiceDao.inTable( searchInTableDto);
	}

	@Override
	public JSONArray global(SearchParametersDto searchInTableDto) {
		// TODO Auto-generated method stub
		return searchServiceDao.global( searchInTableDto);
	}

	@Override
	public JSONArray getGlobal(SearchParametersDto searchInTableDto) {
		// TODO Auto-generated method stub
		return searchServiceDao.getGlobal( searchInTableDto);
	}

}
