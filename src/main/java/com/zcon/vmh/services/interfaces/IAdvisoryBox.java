package com.zcon.vmh.services.interfaces;

import java.sql.SQLException;
import java.util.List;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import com.zcon.vmh.dto.AdviseDataDto;
import com.zcon.vmh.dto.ConnectionTestDto;
import com.zcon.vmh.dto.HypoteekDataDto;
import com.zcon.vmh.dto.IncomeDataDto;
import com.zcon.vmh.dto.InsertContactDto;
import com.zcon.vmh.dto.PensionDataDto;
import com.zcon.vmh.dto.PersonalDataDto;
import com.zcon.vmh.dto.ProductsDataDto;
import com.zcon.vmh.dto.ProductsDataToSaveDTO;
import com.zcon.vmh.dto.ProductsDataToSaveUpdatedDTO;
import com.zcon.vmh.dto.SearchContactDto;
import com.zcon.vmh.dto.XMLDataDTO;

public interface IAdvisoryBox {

	public String connectionTest(ConnectionTestDto connectionTestDto) throws SQLException;

	public String insertContact(InsertContactDto insertContactDto) throws SQLException;

	public JSONObject searchContact(SearchContactDto searchContactDto);

	public JSONObject personalData(PersonalDataDto personalDataDto);

	public JSONObject pensionData(PensionDataDto pensionDataDto);

	public JSONObject incomeData(IncomeDataDto incomeDataDto);

	public JSONObject adviseData(AdviseDataDto adviseDataDto);

	public JSONObject singleAdviseData(AdviseDataDto adviseDataDto);

	public JSONObject getProductsData(ProductsDataDto productsDataDto);

	public String savePersonalData(PersonalDataDto personalDataDto) throws SQLException;

	public String saveProductData(ProductsDataToSaveDTO productsDataDto) throws SQLException;
	
	public JSONObject getHypoteekData(HypoteekDataDto hypoteekDataDto);

	public JSONObject getProductsDataUpdated(ProductsDataDto productsDataDto);

	public String saveProductDataUpdated(ProductsDataToSaveUpdatedDTO productsDataToSaveUpdatedDto) throws SQLException;

	public String updateXmlData(XMLDataDTO xmlData);

	public String saveProductDataUpdatedProposal(ProductsDataToSaveUpdatedDTO productsDataToSaveUpdatedDto) throws SQLException;

	public JSONObject getKlnForAdviseData(AdviseDataDto adviseDataDto);

	public JSONObject getListOfAdviseData(AdviseDataDto adviseDataDto);

	public String checkServicesStatus();

	public JSONObject getPartnerNawAnvId(PersonalDataDto personalDataDto);

	
}
