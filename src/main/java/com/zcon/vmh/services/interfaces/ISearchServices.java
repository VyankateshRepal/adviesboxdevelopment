package com.zcon.vmh.services.interfaces;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.zcon.vmh.dto.SearchParametersDto;


/**
 * @author Vyankatesh
 *
 */
public interface ISearchServices {

	public JSONObject listDatasources();

	public JSONObject createDatasource(JSONObject jsonInput);

	public JSONObject createIndex(JSONObject jsonInput);

	public JSONObject createIndexer(JSONObject jsonInput);

	public JSONObject createIndexerFieldMappings(JSONObject jsonInput);

	public JSONObject search(SearchParametersDto searchInBlobDto) throws JSONException;

	public JSONObject inTable(SearchParametersDto searchInTableDto);

	public JSONArray global(SearchParametersDto searchInTableDto);

	public JSONArray getGlobal(SearchParametersDto searchInTableDto);

}
