package com.zcon.vmh.controller;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zcon.vmh.dto.AdviseDataDto;
import com.zcon.vmh.dto.ConnectionTestDto;
import com.zcon.vmh.dto.HypoteekDataDto;
import com.zcon.vmh.dto.IncomeDataDto;
import com.zcon.vmh.dto.InsertContactDto;
import com.zcon.vmh.dto.PensionDataDto;
import com.zcon.vmh.dto.PersonalDataDto;
import com.zcon.vmh.dto.ProductsDataDto;
import com.zcon.vmh.dto.ProductsDataToSaveDTO;
import com.zcon.vmh.dto.ProductsDataToSaveUpdatedDTO;
import com.zcon.vmh.dto.SearchContactDto;
import com.zcon.vmh.dto.XMLDataDTO;
import com.zcon.vmh.services.interfaces.IAdvisoryBox;

/**
 * @author Vyankatesh
 * 
 *
 */
@RestController
@RequestMapping("/contact")
public class AdviesboxController {
	@Autowired
	IAdvisoryBox advisoryBoxService;

	@RequestMapping(value = "/connectionTest", method = RequestMethod.POST)
	public ResponseEntity<String> connectionTest(@RequestBody ConnectionTestDto connectionTestDto)
			throws NumberFormatException, Exception {
		String result = advisoryBoxService.connectionTest(connectionTestDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertContact", method = RequestMethod.POST)
	public ResponseEntity<String> insertContact(@RequestBody InsertContactDto insertContactDto)
			throws NumberFormatException, Exception {
		String result = advisoryBoxService.insertContact(insertContactDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/searchContact", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> searchContact(@RequestBody SearchContactDto searchContactDto)
			throws NumberFormatException, Exception {
		JSONObject result = advisoryBoxService.searchContact(searchContactDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getPersonalData", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> personalData(@RequestBody PersonalDataDto personalDataDto)
			throws NumberFormatException, Exception {
		JSONObject result = advisoryBoxService.personalData(personalDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getPensionData", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> pensionData(@RequestBody PensionDataDto pensionDataDto)
			throws NumberFormatException, Exception {
		JSONObject result = advisoryBoxService.pensionData(pensionDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getIncomeData", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> incomeData(@RequestBody IncomeDataDto incomeDataDto)
			throws NumberFormatException, Exception {
		JSONObject result = advisoryBoxService.incomeData(incomeDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllAdviseData", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> adviseData(@RequestBody AdviseDataDto adviseDataDto)
			throws NumberFormatException, Exception {
		JSONObject result = advisoryBoxService.adviseData(adviseDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getSingleAdviseData", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> singleAdviseData(@RequestBody AdviseDataDto adviseDataDto)
			throws NumberFormatException, Exception {
		JSONObject result = advisoryBoxService.singleAdviseData(adviseDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getProductsData", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> getProductsData(@RequestBody ProductsDataDto productsDataDto)
			throws NumberFormatException, Exception {
		JSONObject result = advisoryBoxService.getProductsData(productsDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/savePersonalData", method = RequestMethod.POST)
	public ResponseEntity<String> savePersonalData(@RequestBody PersonalDataDto personalDataDto)
			throws NumberFormatException, Exception {
		String result = advisoryBoxService.savePersonalData( personalDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveProductData", method = RequestMethod.POST)
	public ResponseEntity<String> saveProductData(@RequestBody ProductsDataToSaveDTO productsDataDto)
	//public ResponseEntity<JSONObject> saveProductData(@RequestBody org.json.JSONObject jsonInput)
			throws NumberFormatException, Exception {
		String result = advisoryBoxService.saveProductData(  productsDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "development/savePersonalData", method = RequestMethod.POST)
	public ResponseEntity<String> savePersonalDataDevelopment(@RequestBody PersonalDataDto personalDataDto)
			throws NumberFormatException, Exception {
		String result = advisoryBoxService.savePersonalData( personalDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getHypoteekData", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> getHypoteekData(@RequestBody HypoteekDataDto hypoteekDataDto)
			throws NumberFormatException, Exception {
		JSONObject result = advisoryBoxService.getHypoteekData(hypoteekDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getProductsData/Updated", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> getProductsDataUpdated(@RequestBody ProductsDataDto productsDataDto)
			throws NumberFormatException, Exception {
		JSONObject result = advisoryBoxService.getProductsDataUpdated(productsDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveProductData/Updated", method = RequestMethod.POST)
	public ResponseEntity<String> saveProductDataUpdated(@RequestBody ProductsDataToSaveUpdatedDTO productsDataToSaveUpdatedDto)
	//public ResponseEntity<JSONObject> saveProductData(@RequestBody org.json.JSONObject jsonInput)
			throws NumberFormatException, Exception {
		String result = advisoryBoxService.saveProductDataUpdated(productsDataToSaveUpdatedDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateXmlData", method = RequestMethod.POST)
	public ResponseEntity<String> updateXmlData(@RequestBody XMLDataDTO xmlData)
	//public ResponseEntity<JSONObject> saveProductData(@RequestBody org.json.JSONObject jsonInput)
			throws NumberFormatException, Exception {
		String result = advisoryBoxService.updateXmlData(xmlData);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveProductData/Updated/Proposal", method = RequestMethod.POST)
	public ResponseEntity<String> saveProductDataUpdatedProposal(@RequestBody ProductsDataToSaveUpdatedDTO productsDataToSaveUpdatedDto)
	//public ResponseEntity<JSONObject> saveProductData(@RequestBody org.json.JSONObject jsonInput)
			throws NumberFormatException, Exception {
		String result = advisoryBoxService.saveProductDataUpdatedProposal(productsDataToSaveUpdatedDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/checkServicesStatus", method = RequestMethod.GET)
	public ResponseEntity<String> checkServicesStatus()
	//public ResponseEntity<JSONObject> saveProductData(@RequestBody org.json.JSONObject jsonInput)
			throws NumberFormatException, Exception {
		String result = advisoryBoxService.checkServicesStatus();
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getPartnerNawAnvId", method = RequestMethod.POST)
	//public ResponseEntity<String> checkServicesStatus()
	public ResponseEntity<JSONObject> getPartnerNawAnvId(@RequestBody PersonalDataDto personalDataDto)
			throws NumberFormatException, Exception {
		JSONObject result = advisoryBoxService.getPartnerNawAnvId( personalDataDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
