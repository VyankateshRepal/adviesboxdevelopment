package com.zcon.vmh.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zcon.vmh.dto.SearchParametersDto;
import com.zcon.vmh.services.interfaces.ISearchServices;

/**
 * @author Vyankatesh
 *
 */
@RestController
@RequestMapping("/search")
public class SearchController {
	@Autowired
	ISearchServices searchService;
	DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	Calendar calobj = Calendar.getInstance();

	@RequestMapping(value = "/listDatasources", method = RequestMethod.GET)
	public ResponseEntity<JSONObject> listDatasources() throws NumberFormatException, Exception {
		JSONObject result = searchService.listDatasources();
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/createDatasource", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> createDatasource(@RequestBody JSONObject jsonInput) throws Exception {
		JSONObject result = searchService.createDatasource(jsonInput);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/createIndex", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> createIndex(@RequestBody JSONObject jsonInput) throws Exception {
		JSONObject result = searchService.createIndex(jsonInput);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/createIndexer", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> createIndexer(@RequestBody JSONObject jsonInput) throws Exception {
		JSONObject result = searchService.createIndexer(jsonInput);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/createIndexer/fieldMappings", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> createIndexerFieldMappings(@RequestBody JSONObject jsonInput) throws Exception {
		JSONObject result = searchService.createIndexerFieldMappings(jsonInput);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/inBlob", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> search(@RequestBody SearchParametersDto searchInBlobDto) throws Exception {
		JSONObject result = searchService.search(searchInBlobDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/inTable", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> inTable(@RequestBody SearchParametersDto searchInTableDto) throws Exception {
		JSONObject result = searchService.inTable(searchInTableDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/global", method = RequestMethod.POST)
	public ResponseEntity<JSONArray> global(@RequestBody SearchParametersDto searchInTableDto) throws Exception {
		JSONArray result = searchService.global(searchInTableDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getGlobal", method = RequestMethod.POST)
	public ResponseEntity<JSONArray> getGlobal(@RequestBody SearchParametersDto searchInTableDto) throws Exception {
		JSONArray result = searchService.getGlobal(searchInTableDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
