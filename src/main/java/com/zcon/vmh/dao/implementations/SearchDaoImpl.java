package com.zcon.vmh.dao.implementations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Repository;

import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.zcon.vmh.dao.interfaces.ISearchDao;
import com.zcon.vmh.dto.SearchParametersDto;
import com.zcon.vmh.exceptions.SpringAppRuntimeException;

/**
 * @author Vyankatesh
 *
 */
@SuppressWarnings({ "deprecation", "unused" })
@Repository("blobSearchServiceDao")
public class SearchDaoImpl implements ISearchDao {
	protected CloudBlobContainer container;
	protected CloudBlockBlob blob;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.zcon.vmh.dao.interfaces.IFileHandlingDao#listDatasources() Demo
	 * method to check REST API for data source.
	 */
	@SuppressWarnings({ "resource" })
	@Override
	public JSONObject listDatasources() {
		JSONObject jsonOutput = null;
		try {
			String url = "https://zconazuresearch.search.windows.net/datasources?api-version=2016-09-01";
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(url);
			get.setHeader("api-key", "FA8DB2551CCFAF757D9A6AF0BDE73C3D");
			HttpResponse response = client.execute(get);
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			System.out.println(result.toString());
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject) parser.parse(result.toString());
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	@SuppressWarnings({ "resource" })
	@Override
	public JSONObject createDatasource(JSONObject jsonInput) {
		JSONObject jsonOutput = new JSONObject();
		try {
			String url = "https://" + jsonInput.get("searchServiceName")
					+ ".search.windows.net/datasources?api-version=2016-09-01";
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");
			post.setHeader("api-key", jsonInput.get("apiKey").toString());
			jsonInput.remove("apiKey");
			jsonInput.remove("searchServiceName");
			post.setEntity(new StringEntity(jsonInput.toString(), ContentType.create("application/json")));
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(result.toString());
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	@SuppressWarnings({ "resource" })
	@Override
	public JSONObject createIndex(JSONObject jsonInput) {
		JSONObject jsonOutput = new JSONObject();
		try {
			String url = "https://" + jsonInput.get("searchServiceName")
					+ ".search.windows.net/indexes?api-version=2016-09-01";
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");
			post.setHeader("api-key", jsonInput.get("apiKey").toString());
			jsonInput.remove("apiKey");
			jsonInput.remove("searchServiceName");
			post.setEntity(new StringEntity(jsonInput.toString(), ContentType.create("application/json")));
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(result.toString());
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	@SuppressWarnings({ "resource" })
	@Override
	public JSONObject createIndexer(JSONObject jsonInput) {
		JSONObject jsonOutput = new JSONObject();
		try {
			String url = "https://" + jsonInput.get("searchServiceName")
					+ ".search.windows.net/indexers?api-version=2016-09-01";
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");
			post.setHeader("api-key", jsonInput.get("apiKey").toString());
			jsonInput.remove("apiKey");
			jsonInput.remove("searchServiceName");
			post.setEntity(new StringEntity(jsonInput.toString(), ContentType.create("application/json")));
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(result.toString());
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	@SuppressWarnings({ "resource" })
	@Override
	public JSONObject createIndexerFieldMappings(JSONObject jsonInput) {
		JSONObject jsonOutput = new JSONObject();
		try {
			String url = "https://" + jsonInput.get("searchServiceName")
					+ ".search.windows.net/indexers/blob-indexer?api-version=2016-09-01";
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");
			post.setHeader("api-key", jsonInput.get("apiKey").toString());
			jsonInput.remove("apiKey");
			jsonInput.remove("searchServiceName");
			post.setEntity(new StringEntity(jsonInput.toString(), ContentType.create("application/json")));
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(result.toString());
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	@SuppressWarnings({ "unchecked", "resource", "rawtypes" })
	@Override
	public JSONObject search(SearchParametersDto searchInBlobDto) throws JSONException {
		JSONObject jsonOutput = new JSONObject();
		ArrayList encodedURLList = new ArrayList<>();
		try {
			String url = "https://" + searchInBlobDto.getServiceName().toString() + ".search.windows.net/indexes/"
					+ searchInBlobDto.getIndexName().toString() + "/docs/search?api-version=2016-09-01";
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");
			post.setHeader("api-key", searchInBlobDto.getApiKey().toString());
			JSONObject jsonInput = new JSONObject();
			jsonInput.put("search", searchInBlobDto.getSearch().toString());
			post.setEntity(new StringEntity(jsonInput.toString(), ContentType.create("application/json")));
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(result.toString());

			org.json.simple.JSONArray valuesArray = (org.json.simple.JSONArray) jsonOutput.get("value");
			for (int i = 0; i < valuesArray.size(); i++) {
				JSONObject valuesObject = (JSONObject) valuesArray.get(i);
				String encodedUrl = valuesObject.get("metadata_storage_path").toString();
				Base64 decoder = new Base64(true);
				byte[] decodedBytes = decoder.decode(encodedUrl);
				String encodedURL = new String(decodedBytes);
				encodedURLList.add(encodedURL.replace("\r", ""));
			}
			jsonOutput.put("encodedURL", encodedURLList);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	public static String getErrorContainingClassAndMethod() {
		final StackTraceElement e = Thread.currentThread().getStackTrace()[2];
		final String s = e.getClassName();
		String errorInMethod = s.substring(s.lastIndexOf('.') + 1, s.length()) + "." + e.getMethodName();
		return "Error in " + errorInMethod + " : ";
	}

	@Override
	public JSONObject inTable(SearchParametersDto searchInTableDto) {
		JSONObject jsonOutput = new JSONObject();
		ArrayList encodedURLList = new ArrayList<>();
		try {
			String url = "https://" + searchInTableDto.getServiceName().toString() + ".search.windows.net/indexes/"
					+ searchInTableDto.getIndexName().toString() + "/docs/search?api-version=2016-09-01";
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");
			post.setHeader("api-key", searchInTableDto.getApiKey().toString());
			String clientId = searchInTableDto.getClientId().toString();
			String updatedClientId = " \"+" + clientId + "\" ";
			JSONObject jsonInput = new JSONObject();
			jsonInput.put("search", searchInTableDto.getSearch().toString() + "+" + updatedClientId.replace("+", ""));
			post.setEntity(new StringEntity(jsonInput.toString(), ContentType.create("application/json")));
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(result.toString());

			org.json.simple.JSONArray valuesArray = (org.json.simple.JSONArray) jsonOutput.get("value");
			jsonOutput.put("searchResult", valuesArray);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	@Override
	public JSONArray global(SearchParametersDto searchInTableDto) {
		JSONObject jsonOutput = new JSONObject();
		JSONArray jsonArrayOutput = new JSONArray();
		ArrayList encodedURLList = new ArrayList<>();
		org.json.simple.JSONArray valuesArray = new JSONArray();
		try {
			String url = "https://" + searchInTableDto.getServiceName().toString() + ".search.windows.net/indexes/"
					+ searchInTableDto.getIndexName().toString() + "/docs/search?api-version=2016-09-01";
			System.out.println(url);
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");
			post.setHeader("api-key", searchInTableDto.getApiKey().toString());
			String clientId = searchInTableDto.getClientId().toString();
			String updatedClientId = " \"+" + clientId + "\" ";
			JSONObject jsonInput = new JSONObject();

			// logic to get a string for search
			String s = searchInTableDto.getQuery().toString();
			String b = null;
			String[] array = s.split("AND");
			List parameters = new ArrayList<>();
			String parameter;
			String sub = null;
			String c = null;
			String requiredParameters = null;
			String firstParameter = null;
			String lastParameter = null;
			String middleParameters = null;
			List middleParametersList = new ArrayList<>();
			String paramtersQuery = null;
			String columnsQuery = null;
			String finalQuery = null;
			for (int i = 0; i < array.length; i++) {
				if (i > 0) {
					sub = array[i];

					String[] subArray = sub.split("like");
					for (int j = 1; j < subArray.length; j++) {
						parameter = subArray[j];
						String requiredString = parameter.substring(parameter.indexOf("'%") + 2,
								parameter.indexOf("%'"));
						parameters.add(requiredString);
					}
				}
			}
			for (int k = 0; k < parameters.size(); k++) {
				requiredParameters = parameters.get(k).toString() + "||";
				if (k == 0) {
					firstParameter = "(" + parameters.get(k) + "||";
				} else if (k == parameters.size() - 1) {
					lastParameter = parameters.get(k) + ")+";
				} else {
					middleParameters = parameters.get(k) + "||";
					middleParametersList.add(middleParameters);
				}
			}
			String result = String.join("", middleParametersList);
			paramtersQuery = firstParameter + result + lastParameter;
			String[] columnNames = s.split("AND RecordType IN");

			for (int a = 0; a < columnNames.length; a++) {
			}
			String columnNamesString = columnNames[columnNames.length - 1];
			columnsQuery = columnNamesString.replace("('", "\"").replace("')", "\"").replaceAll("'", "\"")
					.replaceAll(",", "+");
			finalQuery = paramtersQuery + columnsQuery;
			// regular logic
			jsonInput.put("search", finalQuery.replaceAll("\"", ""));
			
			int topValue = (int) searchInTableDto.getTop();
			if(topValue==0)
			{
				jsonInput.put("top", 10);
			}
			int skipValue = (int) searchInTableDto.getSkip() ;
			
			String orderby = searchInTableDto.getOrderby();
			if(orderby==""||orderby==null)
			{
				jsonInput.put("orderby", "CreatedDateTime");
			}
			
			jsonInput.put("top", topValue);
			jsonInput.put("skip", skipValue);
			jsonInput.put("orderby", orderby);
			
			//jsonInput.put("facets", "["+"Contacts"+"]");

			
			System.out.println(jsonInput);
			post.setEntity(new StringEntity(jsonInput.toString(), ContentType.create("application/json")));
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer resultOutput = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				resultOutput.append(line);
			}
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(resultOutput.toString());
			//jsonOutput.put("top", jsonInput.get("top").toString());
			//jsonOutput.put("skip", jsonInput.get("skip").toString());

			 valuesArray = (org.json.simple.JSONArray) jsonOutput.get("value");
			// valuesArray.add(jsonOutput.get("top").toString());
			// valuesArray.add(jsonOutput.get("skip").toString());
			 /*	if (valuesArray != null) {
				getNextPageSearchData(jsonOutput, jsonArrayOutput, client, post, jsonInput, valuesArray);
				if (valuesArray != null) {
					getNextPageSearchData(jsonOutput, jsonArrayOutput, client, post, jsonInput, valuesArray);
				}
				if (valuesArray != null) {
					getNextPageSearchData(jsonOutput, jsonArrayOutput, client, post, jsonInput, valuesArray);
				}
				//jsonArrayOutput.add(valuesArray);
			}*/
			
		 	//String jsonOutputArray = jsonArrayOutput.toJSONString().replaceAll("[[","[").replaceAll("]]","]").replaceAll("],[", ",");
		 	//System.out.println(jsonOutputArray);
		 	//jsonArrayOutput = (JSONArray) parser.parse(jsonOutputArray.toString());
		 	// jsonOutput.put("searchResult", valuesArray);
			System.out.println(valuesArray.size());
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return valuesArray;
	}

	/**
	 * @param jsonOutput
	 * @param jsonArrayOutput
	 * @param client
	 * @param post
	 * @param jsonInput
	 * @param valuesArray
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws ParseException
	 */
	private void getNextPageSearchData(JSONObject jsonOutput, JSONArray jsonArrayOutput, HttpClient client,
			HttpPost post, JSONObject jsonInput, org.json.simple.JSONArray valuesArray)
			throws IOException, ClientProtocolException, ParseException {
		HttpResponse response;
		BufferedReader rd;
		StringBuffer resultOutput;
		String line;
		JSONParser parser;
		jsonArrayOutput.add(valuesArray);
		System.out.println(valuesArray.size());
		int topValue = (int) jsonOutput.get("top");
		int skipValue = (int) jsonOutput.get("skip");

		int currentTopValue = topValue + 500;
		int currentSkipValue = skipValue + 500;

		jsonInput.put("top", currentTopValue);
		jsonInput.put("skip", currentSkipValue);

		post.setEntity(new StringEntity(jsonInput.toString(), ContentType.create("application/json")));
		response = client.execute(post);
		rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		resultOutput = new StringBuffer();
		line = "";
		while ((line = rd.readLine()) != null) {
			resultOutput.append(line);
		}
		parser = new JSONParser();
		jsonOutput = (JSONObject) parser.parse(resultOutput.toString());
		jsonOutput.put("top", currentTopValue);
		jsonOutput.put("skip", currentSkipValue);
System.out.println(jsonOutput);
		valuesArray = (org.json.simple.JSONArray) jsonOutput.get("value");
	}

	@Override
	public JSONArray getGlobal(SearchParametersDto searchInTableDto) {
		// TODO Auto-generated method stub
		
		return null;
	}
}