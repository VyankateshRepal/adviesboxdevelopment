package com.zcon.vmh.dao.implementations;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONTokener;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zcon.vmh.dao.interfaces.IAdvisoryBoxDao;
import com.zcon.vmh.dto.AdviseDataDto;
import com.zcon.vmh.dto.ConnectionTestDto;
import com.zcon.vmh.dto.HypoteekDataDto;
import com.zcon.vmh.dto.IncomeDataDto;
import com.zcon.vmh.dto.InsertContactDto;
import com.zcon.vmh.dto.PensionDataDto;
import com.zcon.vmh.dto.PersonalDataDto;
import com.zcon.vmh.dto.ProductsDataDto;
import com.zcon.vmh.dto.ProductsDataToSaveDTO;
import com.zcon.vmh.dto.ProductsDataToSaveUpdatedDTO;
import com.zcon.vmh.dto.ProductsList;
import com.zcon.vmh.dto.ProductsListUpdated;
import com.zcon.vmh.dto.SearchContactDto;
import com.zcon.vmh.dto.XMLDataDTO;
import com.zcon.vmh.exceptions.SpringAppRuntimeException;

/**
 * @author Vyankatesh
 *
 */
@SuppressWarnings({ "unused" })
@Repository("advisoryBoxDao")
public class AdvisoryBoxDaoImpl implements IAdvisoryBoxDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdvisoryBoxDaoImpl.class);
	public static String dateFormat = "dd-MM-yyyy hh:mm:ss";
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
	String jdbcSqlserver = "jdbc:sqlserver://";
	String user = ";user=";
	String password = ";password=";
	String databaseName = ";database=";
	BufferedWriter output = null;

	@Override
	public String connectionTest(ConnectionTestDto connectionTestDto) throws SQLException {
		String output = "";
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(connectionTestDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				System.out.println("Driver name: " + dm.getDriverName());
				System.out.println("Driver version: " + dm.getDriverVersion());
				System.out.println("Product name: " + dm.getDatabaseProductName());
				System.out.println("Product version: " + dm.getDatabaseProductVersion());
				output = dm.getDriverName().toString();
			}
			String SPsql = "EXEC sp_searchcontact ?,?,?";
			PreparedStatement ps = conn.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setString(1, connectionTestDto.getLastName().toString());
			ps.setString(2, connectionTestDto.getPrimaryEmailAddress().toString());
			ps.setString(3, connectionTestDto.getPrimaryAddressPostalCode().toString());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				output = rs.getString(1);
				System.out.println(output);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.toString());
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return output;
	}

	@Override
	public String insertContact(InsertContactDto insertContactDto) throws SQLException {
		String output = "";
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(insertContactDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			String SPsql = "EXEC sp_insertcont ?,?,?,?,?,?,?,?,?,?,?,?,?,?";
			PreparedStatement ps = conn.prepareStatement(SPsql);
			ps.setEscapeProcessing(true);
			ps.setString(1, insertContactDto.getFirstName());
			ps.setString(2, insertContactDto.getMiddleName());
			ps.setString(3, insertContactDto.getLastName());
			ps.setDate(4, insertContactDto.getDateOfBirth());
			ps.setString(5, insertContactDto.getPlaceOfBirth());
			ps.setString(6, insertContactDto.getHomePhone());
			ps.setString(7, insertContactDto.getOfficePhone());
			ps.setString(8, insertContactDto.getMobilePhone());
			ps.setString(9, insertContactDto.getPrimaryEmailAddress());
			ps.setString(10, insertContactDto.getPrimaryAddressStreet());
			ps.setString(11, insertContactDto.getPrimaryAddressHouseNumber());
			ps.setString(12, insertContactDto.getPrimaryAddressCity());
			ps.setString(13, insertContactDto.getPrimaryAddressPostalCode());
			ps.setString(14, insertContactDto.getPrimaryAddressCountry());
			System.out.println(SPsql);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				output = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return output;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject searchContact(SearchContactDto searchContactDto) {
		System.out.println("request found===========================");
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(searchContactDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			 Statement sta = conn.createStatement();

			String lastName = searchContactDto.getLastName();
			String email = searchContactDto.getEmail();
			String postCode = searchContactDto.getPostCode().toString();
			String Sql = null;
			if (postCode.length() > 0) {
				StringBuilder result = new StringBuilder(postCode);
				result.insert(4, " ");
				postCode = result.toString();
			}
			if (lastName != "" && email != "" && postCode != "") {
				Sql = "select n.Naw_Om_Voornamen,A.Kln_id,A.Anv_Id, n.Naw_Om_Voorletters, n.Naw_Om_Naam, A.Anv_Om_TelefoonPrive, A.Anv_Om_TelefoonWerk, A.Anv_Om_TelefoonMobiel, A.Anv_Om_Email, n.Naw_Om_Postcode, n.Naw_Om_Straat, n.Naw_Nr_Huis, A.Anv_Dt_Geboorte, A.Anv_Om_PlaatsGeboorte, A.Anv_Om_LegitimatieAfgifte, A.Anv_Cd_Nationaliteit, n.naw_om_iban, N.Naw_Id,  isnull((select p.naw_id from Aanvrager p where p.kln_id = A.Kln_Id and p.Naw_Id <> N.Naw_Id ),0) as partner_naw_Id from naw N  inner join Aanvrager A on N.Naw_Id = A.Naw_Id  where Naw_Om_Naam like '%"
						+ searchContactDto.getLastName() + "%' or A.Anv_Om_Email like '%" + searchContactDto.getEmail()
						+ "%' or n.Naw_Om_Postcode like '%" + postCode + "%'";

				procedureQueryLogs(Sql);
				System.out.println(Sql);

			} else if (lastName != "" && email != "" && postCode == "") {
				Sql = "select n.Naw_Om_Voornamen,A.Kln_id,A.Anv_Id, n.Naw_Om_Voorletters, n.Naw_Om_Naam, A.Anv_Om_TelefoonPrive, A.Anv_Om_TelefoonWerk, A.Anv_Om_TelefoonMobiel, A.Anv_Om_Email, n.Naw_Om_Postcode, n.Naw_Om_Straat, n.Naw_Nr_Huis, A.Anv_Dt_Geboorte, A.Anv_Om_PlaatsGeboorte, A.Anv_Om_LegitimatieAfgifte, A.Anv_Cd_Nationaliteit, n.naw_om_iban, N.Naw_Id,  isnull((select p.naw_id from Aanvrager p where p.kln_id = A.Kln_Id and p.Naw_Id <> N.Naw_Id ),0) as partner_naw_Id from naw N  inner join Aanvrager A on N.Naw_Id = A.Naw_Id  where Naw_Om_Naam like '%"
						+ searchContactDto.getLastName() + "%' or A.Anv_Om_Email like '%" + searchContactDto.getEmail()
						+ "%'";

				procedureQueryLogs(Sql);
				System.out.println(Sql);
			} else if (lastName != "" && email == "" && postCode != "") {
				Sql = "select n.Naw_Om_Voornamen,A.Kln_id,A.Anv_Id, n.Naw_Om_Voorletters, n.Naw_Om_Naam, A.Anv_Om_TelefoonPrive, A.Anv_Om_TelefoonWerk, A.Anv_Om_TelefoonMobiel, A.Anv_Om_Email, n.Naw_Om_Postcode, n.Naw_Om_Straat, n.Naw_Nr_Huis, A.Anv_Dt_Geboorte, A.Anv_Om_PlaatsGeboorte, A.Anv_Om_LegitimatieAfgifte, A.Anv_Cd_Nationaliteit, n.naw_om_iban, N.Naw_Id,  isnull((select p.naw_id from Aanvrager p where p.kln_id = A.Kln_Id and p.Naw_Id <> N.Naw_Id ),0) as partner_naw_Id from naw N  inner join Aanvrager A on N.Naw_Id = A.Naw_Id  where Naw_Om_Naam like '%"
						+ searchContactDto.getLastName() + "%' or n.Naw_Om_Postcode like '%" + postCode + "%'";
				procedureQueryLogs(Sql);
				System.out.println(Sql);
			} else if (lastName == "" && email != "" && postCode != "") {
				Sql = "select n.Naw_Om_Voornamen,A.Kln_id,A.Anv_Id, n.Naw_Om_Voorletters, n.Naw_Om_Naam, A.Anv_Om_TelefoonPrive, A.Anv_Om_TelefoonWerk, A.Anv_Om_TelefoonMobiel, A.Anv_Om_Email, n.Naw_Om_Postcode, n.Naw_Om_Straat, n.Naw_Nr_Huis, A.Anv_Dt_Geboorte, A.Anv_Om_PlaatsGeboorte, A.Anv_Om_LegitimatieAfgifte, A.Anv_Cd_Nationaliteit, n.naw_om_iban, N.Naw_Id,  isnull((select p.naw_id from Aanvrager p where p.kln_id = A.Kln_Id and p.Naw_Id <> N.Naw_Id ),0) as partner_naw_Id from naw N  inner join Aanvrager A on N.Naw_Id = A.Naw_Id  where A.Anv_Om_Email like '%"
						+ searchContactDto.getEmail() + "%' or n.Naw_Om_Postcode like '%" + postCode + "%'";
				procedureQueryLogs(Sql);
				System.out.println(Sql);
			} else if (lastName == "" && email == "" && postCode != "") {
				Sql = "select n.Naw_Om_Voornamen,A.Kln_id,A.Anv_Id, n.Naw_Om_Voorletters, n.Naw_Om_Naam, A.Anv_Om_TelefoonPrive, A.Anv_Om_TelefoonWerk, A.Anv_Om_TelefoonMobiel, A.Anv_Om_Email, n.Naw_Om_Postcode, n.Naw_Om_Straat, n.Naw_Nr_Huis, A.Anv_Dt_Geboorte, A.Anv_Om_PlaatsGeboorte, A.Anv_Om_LegitimatieAfgifte, A.Anv_Cd_Nationaliteit, n.naw_om_iban, N.Naw_Id,  isnull((select p.naw_id from Aanvrager p where p.kln_id = A.Kln_Id and p.Naw_Id <> N.Naw_Id ),0) as partner_naw_Id from naw N  inner join Aanvrager A on N.Naw_Id = A.Naw_Id  where n.Naw_Om_Postcode like '%"
						+ postCode + "%'";
				procedureQueryLogs(Sql);
				System.out.println(Sql);
			} else if (lastName != "" && email == "" && postCode == "") {
				Sql = "select n.Naw_Om_Voornamen,A.Kln_id,A.Anv_Id, n.Naw_Om_Voorletters, n.Naw_Om_Naam, A.Anv_Om_TelefoonPrive, A.Anv_Om_TelefoonWerk, A.Anv_Om_TelefoonMobiel, A.Anv_Om_Email, n.Naw_Om_Postcode, n.Naw_Om_Straat, n.Naw_Nr_Huis, A.Anv_Dt_Geboorte, A.Anv_Om_PlaatsGeboorte, A.Anv_Om_LegitimatieAfgifte, A.Anv_Cd_Nationaliteit, n.naw_om_iban, N.Naw_Id,  isnull((select p.naw_id from Aanvrager p where p.kln_id = A.Kln_Id and p.Naw_Id <> N.Naw_Id ),0) as partner_naw_Id from naw N  inner join Aanvrager A on N.Naw_Id = A.Naw_Id  where Naw_Om_Naam like '%"
						+ searchContactDto.getLastName() + "%'";
				procedureQueryLogs(Sql);
				System.out.println(Sql);

			} else if (lastName == "" && email != "" && postCode == "") {
				Sql = "select n.Naw_Om_Voornamen,A.Kln_id,A.Anv_Id, n.Naw_Om_Voorletters, n.Naw_Om_Naam, A.Anv_Om_TelefoonPrive, A.Anv_Om_TelefoonWerk, A.Anv_Om_TelefoonMobiel, A.Anv_Om_Email, n.Naw_Om_Postcode, n.Naw_Om_Straat, n.Naw_Nr_Huis, A.Anv_Dt_Geboorte, A.Anv_Om_PlaatsGeboorte, A.Anv_Om_LegitimatieAfgifte, A.Anv_Cd_Nationaliteit, n.naw_om_iban, N.Naw_Id,  isnull((select p.naw_id from Aanvrager p where p.kln_id = A.Kln_Id and p.Naw_Id <> N.Naw_Id ),0) as partner_naw_Id from naw N  inner join Aanvrager A on N.Naw_Id = A.Naw_Id  where A.Anv_Om_Email like '%"
						+ searchContactDto.getEmail() + "%' or n.Naw_Om_Postcode like '%" + postCode + "%'";
				procedureQueryLogs(Sql);
				System.out.println(Sql);
			}
			if (lastName != "" || email != "" || postCode != "") {
				List<Map<String, Object>> resultList = executeQueryGetAllData(sta, Sql);
				jsonOutput.put("searchResult", resultList);
				System.out.println("request completed");
			} else {
				jsonOutput.put("message", "put at least a value");
				System.out.println("request completed");
			}
		} catch (Exception e) {
			System.out.println("request completed with error");
			e.printStackTrace();
			LOGGER.error("Customized Error=====" + e.toString());
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			closeConnection(conn);
		}
		return jsonOutput;
	}

	/**
	 * @param Sql
	 * @throws IOException
	 */
	private void procedureQueryLogs(String Sql) throws IOException {
		File file = new File("D:\\example.txt");
		FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		fw.write("\r\n");
		fw.write("\r\n");
		fw.write(dateFormat.format(date));
		fw.write("\r\n");
		fw.write(Sql);
		fw.close();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public JSONObject personalData(PersonalDataDto personalDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(personalDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			Statement sta = conn.createStatement();
			String Sql = "select n.NAW_CD_BIC, n.Naw_Cd_Geslacht, n.Naw_Om_Plaats, n.NAW_OM_IBAN, n.NAW_OM_GEBOORTEVOORVOEGSEL,n.NAW_OM_GEBOORTENAAM, n.Naw_Om_Voornamen, n.Naw_Om_Voorvoegsel,n.Naw_Om_Voorletters, n.Naw_Om_Naam, A.Anv_Cd_BurgelijkeStaat,A.Anv_Cd_Legitimatiebewijs,  A.Anv_Cd_OpleidingsNiveau, A.Anv_In_Gescheiden, A.Anv_Om_Sofinummer, A.Anv_Om_TelefoonPrive, A.Anv_Om_TelefoonWerk, A.Anv_Om_TelefoonMobiel, A.Anv_Om_Email, n.Naw_Om_Postcode, n.Naw_Om_Straat, n.Naw_Nr_Huis, A.Anv_Dt_Geboorte,A.Anv_In_Roken, A.Anv_Om_PlaatsGeboorte, A.Anv_Om_LegitimatieAfgifte, A.Anv_Cd_Nationaliteit, n.naw_om_iban, N.Naw_Id from naw N inner join Aanvrager A on N.Naw_Id = A.Naw_Id where N.Naw_Id="
					+ personalDataDto.getNawId();
			ResultSet m_ResultSet = sta.executeQuery(Sql);
			Map<String, Object> row = null;
			ResultSetMetaData metaData = m_ResultSet.getMetaData();
			Integer columnCount = metaData.getColumnCount();
			List personalDataList = new ArrayList();
			String postalCode = null;
			String streetName = null;
			String houseNumber = null;
			String city = null;

			while (m_ResultSet.next()) {
				row = new HashMap<String, Object>();
				for (int i = 1; i <= columnCount; i++) {
					row.put("naw_Om_Voorletters", m_ResultSet.getObject("Naw_Om_Voorletters"));
					row.put("naw_Om_Naam", m_ResultSet.getObject("Naw_Om_Naam"));
					row.put("anv_Dt_Geboorte", m_ResultSet.getObject("Anv_Dt_Geboorte"));
					row.put("anv_Om_Email", m_ResultSet.getObject("Anv_Om_Email"));
					row.put("anv_Om_TelefoonPrive", m_ResultSet.getObject("Anv_Om_TelefoonPrive"));
					row.put("anv_Om_LegitimatieAfgifte", m_ResultSet.getObject("Anv_Om_LegitimatieAfgifte"));
					row.put("anv_Om_TelefoonWerk", m_ResultSet.getObject("Anv_Om_TelefoonWerk"));
					row.put("naw_Om_Voornamen", m_ResultSet.getObject("Naw_Om_Voornamen"));
					row.put("naw_Id", m_ResultSet.getObject("Naw_Id"));
					row.put("anv_Cd_Nationaliteit", m_ResultSet.getObject("Anv_Cd_Nationaliteit"));
					row.put("anv_Om_PlaatsGeboorte", m_ResultSet.getObject("Anv_Om_PlaatsGeboorte"));
					row.put("naw_Om_Postcode", m_ResultSet.getObject("Naw_Om_Postcode"));
					row.put("naw_om_iban", m_ResultSet.getObject("naw_om_iban"));
					row.put("naw_Nr_Huis", m_ResultSet.getObject("Naw_Nr_Huis"));
					row.put("naw_Om_Straat", m_ResultSet.getObject("Naw_Om_Straat"));
					row.put("anv_Om_TelefoonMobiel", m_ResultSet.getObject("Anv_Om_TelefoonMobiel"));
					row.put("naw_Om_Voorvoegsel", m_ResultSet.getObject("Naw_Om_Voorvoegsel"));
					row.put("naw_Om_Geboortenaam", m_ResultSet.getObject("NAW_OM_GEBOORTENAAM"));
					row.put("naw_Om_Geboortevoorvoegsel", m_ResultSet.getObject("NAW_OM_GEBOORTEVOORVOEGSEL"));
					row.put("naw_Om_Iban", m_ResultSet.getObject("NAW_OM_IBAN"));
					row.put("naw_Om_Plaats", m_ResultSet.getObject("Naw_Om_Plaats"));
					row.put("naw_Cd_Geslacht", m_ResultSet.getObject("Naw_Cd_Geslacht"));
					row.put("anv_Om_Sofinummer", m_ResultSet.getObject("Anv_Om_Sofinummer"));
					row.put("naw_Cd_Bic", m_ResultSet.getObject("NAW_CD_BIC"));
					row.put("anv_Cd_BurgelijkeStaat", m_ResultSet.getObject("Anv_Cd_BurgelijkeStaat"));
					row.put("anv_In_Roken", m_ResultSet.getObject("Anv_In_Roken"));
					row.put("anv_In_Gescheiden", m_ResultSet.getObject("Anv_In_Gescheiden"));
					row.put("anv_Cd_Legitimatiebewijs", m_ResultSet.getObject("Anv_Cd_Legitimatiebewijs"));
					row.put("anv_Cd_OpleidingsNiveau", m_ResultSet.getObject("Anv_Cd_OpleidingsNiveau"));

				}
				postalCode = m_ResultSet.getObject("Naw_Om_Postcode").toString();
				streetName = m_ResultSet.getObject("naw_Om_Straat").toString();
				houseNumber = m_ResultSet.getObject("naw_Nr_Huis").toString();
				city = m_ResultSet.getObject("naw_Om_Plaats").toString();

			}

			String postalCodePartner = null;
			String streetNamePartner = null;
			String houseNumberPartner = null;
			String cityPartner = null;

			String SqlPartnerData = "select n.NAW_CD_BIC, n.Naw_Cd_Geslacht, n.Naw_Om_Plaats, n.NAW_OM_IBAN, n.NAW_OM_GEBOORTEVOORVOEGSEL,n.NAW_OM_GEBOORTENAAM, n.Naw_Om_Voornamen, n.Naw_Om_Voorvoegsel,n.Naw_Om_Voorletters, n.Naw_Om_Naam, A.Anv_Cd_BurgelijkeStaat,A.Anv_Cd_Legitimatiebewijs,  A.Anv_Cd_OpleidingsNiveau, A.Anv_In_Gescheiden, A.Anv_Om_Sofinummer, A.Anv_Om_TelefoonPrive, A.Anv_Om_TelefoonWerk, A.Anv_Om_TelefoonMobiel, A.Anv_Om_Email, n.Naw_Om_Postcode, n.Naw_Om_Straat, n.Naw_Nr_Huis, A.Anv_Dt_Geboorte,A.Anv_In_Roken, A.Anv_Om_PlaatsGeboorte, A.Anv_Om_LegitimatieAfgifte, A.Anv_Cd_Nationaliteit, n.naw_om_iban, N.Naw_Id from naw N inner join Aanvrager A on N.Naw_Id = A.Naw_Id where N.Naw_Id="
					+ personalDataDto.getPartner_naw_Id();

			ResultSet m_ResultSet_PartnerData = sta.executeQuery(SqlPartnerData);
			Map<String, Object> rowPartnerData = null;
			ResultSetMetaData metaDataPartner = m_ResultSet_PartnerData.getMetaData();
			Integer columnCountPartner = metaDataPartner.getColumnCount();
			while (m_ResultSet_PartnerData.next()) {
				/// row = new HashMap<String, Object>();
				for (int i = 1; i <= columnCountPartner; i++) {

					postalCodePartner = m_ResultSet_PartnerData.getObject("Naw_Om_Postcode").toString();
					streetNamePartner = m_ResultSet_PartnerData.getObject("naw_Om_Straat").toString();
					houseNumberPartner = m_ResultSet_PartnerData.getObject("naw_Nr_Huis").toString();
					cityPartner = m_ResultSet_PartnerData.getObject("naw_Om_Plaats").toString();
				}

			}

			if (postalCode.isEmpty() || streetName.isEmpty() || houseNumber.isEmpty() || city.isEmpty()) {
				row.put("naw_Om_Postcode", postalCodePartner);
				row.put("naw_Om_Plaats", cityPartner);
				row.put("naw_Nr_Huis", houseNumberPartner);
				row.put("naw_Om_Straat", streetNamePartner);
			}

			if (postalCode.isEmpty() == false && (streetName.isEmpty() || houseNumber.isEmpty() || city.isEmpty())) {
				if (postalCode == postalCodePartner) {
					row.put("naw_Om_Postcode", postalCodePartner);
					row.put("naw_Om_Plaats", cityPartner);
					row.put("naw_Nr_Huis", houseNumberPartner);
					row.put("naw_Om_Straat", streetNamePartner);
				}
			}

			personalDataList.add(row);
			jsonOutput.put("personalData", personalDataList);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			closeConnection(conn);
		}
		return jsonOutput;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject pensionData(PensionDataDto pensionDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		String kln_Id = null, anv_Id = null, adv_Id = null;
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(pensionDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			Statement sta = conn.createStatement();
			String Sql = "select Kln_Id,Anv_Id from Aanvrager where Naw_Id=" + pensionDataDto.getNawId();
			ResultSet m_ResultSet = sta.executeQuery(Sql);
			while (m_ResultSet.next()) {
				kln_Id = m_ResultSet.getString("Kln_Id");
				anv_Id = m_ResultSet.getString("Anv_Id");
			}
			String queryAdv_Id = "select Adv_Id from Advies where Kln_Id=" + kln_Id;
			ResultSet query_ResultSet = sta.executeQuery(queryAdv_Id);
			while (query_ResultSet.next()) {
				adv_Id = query_ResultSet.getString("Adv_Id");
			}
			String queryPensionData = "select * from Pensioen where Adv_Id=" + adv_Id + " and Anv_Id=" + anv_Id;
			List<Map<String, Object>> resultListquery = executeQueryGetAllData(sta, queryPensionData);
			jsonOutput.put("pensionData", resultListquery);

			String pensionQuery = "first query=" + Sql + "second query=" + queryAdv_Id + "third query="
					+ queryPensionData;
			procedureQueryLogs(pensionQuery);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			closeConnection(conn);
		}
		return jsonOutput;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject incomeData(IncomeDataDto incomeDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(incomeDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			Statement sta = conn.createStatement();
			String Sql = "select * from Aanvrager where Naw_Id=" + incomeDataDto.getNawId();
			List<Map<String, Object>> resultList = executeQueryGetAllData(sta, Sql);
			jsonOutput.put("incomeData", resultList);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			closeConnection(conn);
		}
		return jsonOutput;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public JSONObject adviseData(AdviseDataDto adviseDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		String kln_Id = null, anv_Id = null, adv_Id = null;
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(adviseDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			Statement sta = conn.createStatement();
			String Sql = "select Kln_Id from Aanvrager where Naw_Id=" + adviseDataDto.getNawId();
			ResultSet m_ResultSet = sta.executeQuery(Sql);
			while (m_ResultSet.next()) {
				kln_Id = m_ResultSet.getString("Kln_Id");
			}
			String queryAdviseData = "select * from Advies where Kln_Id=" + kln_Id;
			ResultSet m_ResultSetAdviseData = sta.executeQuery(queryAdviseData);
			List adviseList = new ArrayList();
			Map<String, Object> row = null;
			ResultSetMetaData metaData = m_ResultSetAdviseData.getMetaData();
			Integer columnCount = metaData.getColumnCount();
			while (m_ResultSetAdviseData.next()) {
				row = new HashMap<String, Object>();
				for (int i = 1; i <= columnCount; i++) {
					row.put("Adv_Id", m_ResultSetAdviseData.getObject("Adv_Id"));
					row.put("Adv_Dt_Aanmaak", m_ResultSetAdviseData.getObject("Adv_Dt_Aanmaak").toString());
					row.put("Adv_Om", m_ResultSetAdviseData.getObject("Adv_Om"));
					row.put("ADV_OM_PAKKETNAAM", m_ResultSetAdviseData.getObject("ADV_OM_PAKKETNAAM"));
					row.put("Adv_Dt_Mutatie", m_ResultSetAdviseData.getObject("Adv_Dt_Mutatie").toString());
					row.put("Adv_Om_Klant", m_ResultSetAdviseData.getObject("Adv_Om_Klant"));
				}
				row.put("klnId", kln_Id);
				adviseList.add(row);
			}
			System.out.println(adviseList);
			jsonOutput.put("allAdviseData", adviseList);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			closeConnection(conn);
		}
		return jsonOutput;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject singleAdviseData(AdviseDataDto adviseDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(adviseDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			Statement sta = conn.createStatement();
			String Sql = "select * from Advies where Adv_Id=" + adviseDataDto.getAdvId();
			List<Map<String, Object>> resultList = executeQueryGetAllData(sta, Sql);
			jsonOutput.put("sinngleAdviseData", resultList);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			closeConnection(conn);
		}
		return jsonOutput;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public JSONObject getProductsData(ProductsDataDto productsDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		Calendar calendar = Calendar.getInstance();
		org.json.JSONArray productList = new org.json.JSONArray();
		JSONArray array = new JSONArray();
		List productsList = new ArrayList<>();
		org.json.JSONArray productsArray;
		String jsonData;
		String name = null;
		JSONParser parser = new JSONParser();
		JSONObject objectOutput = new JSONObject();
		List debtorList = new ArrayList<>();
		List debtorListRequired = new ArrayList<>();
		int debtorListSize = 0;
		org.json.JSONObject productsObject = new org.json.JSONObject();
		int count = 0;
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(productsDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			Statement sta = conn.createStatement();
			String nameFirstInsured = "";
			String nawQuery = "select * from naw where Naw_Id=" + productsDataDto.getNawId();
			ResultSet nawResultSet = sta.executeQuery(nawQuery);
			ResultSetMetaData metaDataNaw = nawResultSet.getMetaData();
			List personalDataList = new ArrayList();
			while (nawResultSet.next()) {
				nameFirstInsured = nawResultSet.getObject("Naw_Om_Naam").toString();
			}
			String Sql = "select * from Dekking where Adv_Id=" + productsDataDto.getAdvId() + "and Len_Id="
					+ productsDataDto.getLenId();
			ResultSet m_ResultSet = sta.executeQuery(Sql);
			Map<String, Object> row = null;
			ResultSetMetaData metaData = m_ResultSet.getMetaData();
			Integer columnCount = metaData.getColumnCount();
			System.out.println("columnCount =============" + columnCount);
			while (m_ResultSet.next()) {
				row = new HashMap<String, Object>();
				String productCode = null;
				for (int i = 1; i <= columnCount; i++) {
					row.put("nameFirstInsured", nameFirstInsured);
					row.put("serviceProvider", "");
					prodcutClosedStatus(productsDataDto, row);
					String productname = m_ResultSet.getObject("Dek_Om_Produktnaam").toString();
					row.put("productCategory", productCode);
					row.put("productName", productname);
					row.put("productSubcategory", productname);
					row.put("productId", m_ResultSet.getObject("Dek_Id"));
					row.put("providerName", m_ResultSet.getObject("Dek_Om_Mij"));
					productCode = m_ResultSet.getObject("Dek_Om_Produktcode").toString();
					// String date = m_ResultSet.getObject("Dek_Dt_Aanvang");
					row.put("policyNumber", m_ResultSet.getObject("Dek_Om_Polisnummer"));

					String date = m_ResultSet.getObject("Dek_Dt_Aanvang").toString().replaceAll("-", "/")
							.replaceAll("00:00:00.0", "00:00:00");
					System.out.println(date);

					String dateInMillis = convertDateInMilliseconds(date);
					System.out.println(dateInMillis);

					row.put("startDate", dateInMillis);
					System.out.println();
					if (productCode.equalsIgnoreCase("0/BEL")) {
						productCode = "Vermogen";
					} else if (productCode.equalsIgnoreCase("0/UIT")) {
						productCode = "Uitvaart";
					} else if (productCode.equalsIgnoreCase("0/LEV")) {
						productCode = "Kapitaalverz";
					} else if (productCode.equalsIgnoreCase("0/ORV") || productCode.equalsIgnoreCase("1/ORV")) {
						productCode = "ORV";
					} else if (productCode.equalsIgnoreCase("0/HLV")) {
						productCode = "AOV";
					} else if (productCode.equalsIgnoreCase("0/LYF")) {
						productCode = "Uit.lijfr";
					} else if (productCode.equalsIgnoreCase("0/SDV")) {
						productCode = "Schade";
					}
				}
				productList.put(row);
			}
			String creditsQuery = "select * from Kredieten where Adv_Id=" + productsDataDto.getAdvId() + "and Len_Id="
					+ productsDataDto.getLenId();
			ResultSet m_ResultSetCreditsData = sta.executeQuery(creditsQuery);
			List creditsList = new ArrayList();
			Map<String, Object> rowCredits = null;
			ResultSetMetaData metaDataCredits = m_ResultSetCreditsData.getMetaData();
			System.out.println(metaDataCredits.getColumnCount());
			Integer columnCountCredits = metaDataCredits.getColumnCount();
			while (m_ResultSetCreditsData.next()) {
				rowCredits = new HashMap<String, Object>();
				for (int i = 1; i <= columnCountCredits; i++) {
					// String date =
					// m_ResultSetCreditsData.getObject("Kre_Dt_Ingang").toString();
					rowCredits.put("policyNumber", m_ResultSetCreditsData.getObject("Kre_Nr_Leningnummer"));
					rowCredits.put("productName", m_ResultSetCreditsData.getObject("KRE_OM_PRODUCTNAAM"));

					String date = m_ResultSetCreditsData.getObject("Kre_Dt_Ingang").toString().replaceAll("-", "/")
							.replaceAll("00:00:00.0", "00:00:00");

					String dateInMillis = convertDateInMilliseconds(date);

					rowCredits.put("startDate", dateInMillis);
					rowCredits.put("productCategory", "Kredieten");
					rowCredits.put("productSubcategory", m_ResultSetCreditsData.getObject("KRE_OM_PRODUCTNAAM"));
					rowCredits.put("creditType", m_ResultSetCreditsData.getObject("KRE_OM_TYPE"));
					rowCredits.put("providerName", m_ResultSetCreditsData.getObject("Kre_Om_Geldverstrekker"));
				}
				rowCredits.put("nameFirstInsured", nameFirstInsured);
				rowCredits.put("serviceProvider", "");
				prodcutClosedStatus(productsDataDto, rowCredits);
				productList.put(rowCredits);
			}
			String hypoteekQuery = "select * from Leningdeel where Adv_Id=" + productsDataDto.getAdvId() + "and Len_Id="
					+ productsDataDto.getLenId();
			ResultSet m_ResultSetHypoteekData = sta.executeQuery(hypoteekQuery);
			List hypoteekList = new ArrayList();
			Map<String, Object> rowHypoteek = null;
			ResultSetMetaData metaDataHypoteek = m_ResultSetHypoteekData.getMetaData();
			Integer columnCountHypoteek = metaDataHypoteek.getColumnCount();
			while (m_ResultSetHypoteekData.next()) {
				rowHypoteek = new HashMap<String, Object>();
				for (int i = 1; i <= columnCountHypoteek; i++) {
					// String date =
					// m_ResultSetHypoteekData.getObject("Lnd_Dt_Aanvang").toString();

					String date = m_ResultSetHypoteekData.getObject("Lnd_Dt_Aanvang").toString().replaceAll("-", "/")
							.replaceAll("00:00:00.0", "00:00:00");

					String dateInMillis = convertDateInMilliseconds(date);

					rowHypoteek.put("startDate", dateInMillis);
					rowHypoteek.put("productName", m_ResultSetHypoteekData.getObject("Lnd_Om_ProduktNaam"));
					rowHypoteek.put("policyNumber", m_ResultSetHypoteekData.getObject("Lnd_Om_LeningNummer"));
					rowHypoteek.put("productCategory", "Hypoteek");
					rowHypoteek.put("productSubcategory", m_ResultSetHypoteekData.getObject("Lnd_Om_ProduktNaam"));
					rowHypoteek.put("providerName", m_ResultSetHypoteekData.getObject("Lnd_Om_Mij"));
				}
				rowHypoteek.put("nameFirstInsured", nameFirstInsured);
				rowHypoteek.put("serviceProvider", "");
				prodcutClosedStatus(productsDataDto, rowHypoteek);
				productList.put(rowHypoteek);
			}
			jsonOutput.put("productsData", productList);
			productsArray = (org.json.JSONArray) jsonOutput.get("productsData");
			for (int l = 0; l < productsArray.length(); l++) {
				String nameHypoteek = "";
				System.out.println(productsArray.getJSONObject(l));

				Integer aanvragerId = 0;
				String debtorName = "";
				productsObject = productsArray.getJSONObject(l);
				if (productsObject.get("productCategory").toString().equalsIgnoreCase("ORV")) {
					String sqlFirstInsurerName = "select v.dek_id, v.Anv_Id, a.Kln_Id,b.Naw_Id, n.Naw_Om_Naam from Verzekerde v inner join advies A on a.Adv_Id = v.Adv_Id inner join Aanvrager b on a.Kln_Id = b.Kln_Id and b.anv_id = v.Anv_Id inner join naw n on n.Naw_Id = b.Naw_Id where v.Adv_Id ="
							+ productsDataDto.getAdvId() + "and v.Len_Id =" + productsDataDto.getLenId()
							+ "and v.Dek_Id =" + productsObject.get("productId");
					ResultSet resultSetNames = sta.executeQuery(sqlFirstInsurerName);
					List nameList = new ArrayList();
					Map<String, Object> rowName = null;
					ResultSetMetaData metaDataName = resultSetNames.getMetaData();
					while (resultSetNames.next()) {
						name = resultSetNames.getString("Naw_Om_Naam");
						Integer anvId = resultSetNames.getInt("Anv_Id");
						nameList.add(name);
						if (nameList.size() == 1 && anvId > 1) {
							productsObject.put("nameFirstInsurer", name);
						} else {
							productsObject.put("nameFirstInsurer", nameList.get(0));
						}
					}
				} else if (productsObject.get("productCategory").toString().equalsIgnoreCase("Hypoteek")) {
					count++;
					String sqlDebtorName = "select LND_NR_AANVRAGER, n.Naw_Om_Naam, * from Leningdeel v inner join advies A on a.Adv_Id = v.Adv_Id left outer join Aanvrager b on b.Kln_Id = a.Kln_Id and b.anv_id = v.LND_NR_AANVRAGER left outer join naw n on n.Naw_Id = b.Naw_Id where v.Adv_Id = "
							+ productsDataDto.getAdvId() + " and v.len_id =" + productsDataDto.getLenId();
					ResultSet resultSetDebtorNames = sta.executeQuery(sqlDebtorName);
					List debtorNameList = new ArrayList();
					Map<String, Object> rowName = null;
					ResultSetMetaData metaDataName = resultSetDebtorNames.getMetaData();
					while (resultSetDebtorNames.next()) {

						aanvragerId = resultSetDebtorNames.getInt("LND_NR_AANVRAGER");
						if (aanvragerId == 0) {
							debtorName = name;
							debtorList.add(debtorName);
						} else {
							nameHypoteek = resultSetDebtorNames.getString("Naw_Om_Naam");
							debtorName = nameHypoteek;
							debtorList.add(debtorName);
						}

					}
					debtorListSize = debtorList.size();
				}
			}
			int a = 0;
			int b = -1;
			for (a = 0; a < count; a++) {
				String debtorRequiredName = debtorList.get(a).toString();
				debtorListRequired.add(debtorRequiredName);
			}
			productsArray = (org.json.JSONArray) jsonOutput.get("productsData");
			for (int l = 0; l < productsArray.length(); l++) {
				productsObject = productsArray.getJSONObject(l);
				if (productsObject.get("productCategory").toString().equalsIgnoreCase("Hypoteek")) {
					b++;
					productsObject.put("nameFirstDebtor", debtorListRequired.get(b));
				}
			}
			Gson gsonData = new Gson();
			jsonData = gsonData.toJson(productsArray);
			Object obj = parser.parse(jsonData);
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray arr = (JSONArray) jsonObject.get("myArrayList");
			JSONArray newArr = new JSONArray();
			for (int i = 0; i < arr.size(); i++) {
				JSONObject object = (JSONObject) arr.get(i);
				JSONObject eachMapObject = (JSONObject) object.get("map");
				// added if temporary
				String providerName = eachMapObject.get("providerName").toString();
				System.out.println(providerName.length());
				// if (providerName.length() >= 1) {
				// if(eachMapObject.get("productCategory").toString().equalsIgnoreCase("Schade")||eachMapObject.get("productCategory").toString().equalsIgnoreCase("Hypoteek")||eachMapObject.get("productCategory").toString().equalsIgnoreCase("ORV")||eachMapObject.get("productCategory").toString().equalsIgnoreCase("AOV"))
				{
					newArr.add(eachMapObject);
				}
				// }
			}
			objectOutput.put("productsList", newArr);
			if (newArr.isEmpty()) {
				jsonOutput.put("message", "No data present for nawId " + productsDataDto.getNawId());
			} else {
				jsonOutput.put("message", "Data present for nawId " + productsDataDto.getNawId());
			}
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			closeConnection(conn);
		}
		return objectOutput;
	}

	@Override
	public JSONObject getHypoteekData(HypoteekDataDto hypoteekDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(hypoteekDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			String advId = hypoteekDataDto.getAdvId();
			String lenId = hypoteekDataDto.getLenId();
			Statement sta = conn.createStatement();

			String Sql = "select distinct B.zkn_id,  l.Lnd_Om_Mij from Leningdeel L inner join Bestaand B on b.Adv_Id = l.Adv_Id and B.ZKN_GUID = L.LND_GUID_BESTAAND inner join Scenario S on S.Adv_Id = L.Adv_Id and s.Len_Id = 100 and s.Lnd_Id = L.Lnd_Id where L.adv_id = 18 and L.len_id = 100 order by B.zkn_id, l.Lnd_Om_Mij";
			List<Map<String, Object>> resultList = executeQueryGetAllData(sta, Sql);
			List<Map<String, Object>> internalResultList = new ArrayList<>();
			/// List outputList = new ArrayList<>();
			for (int i = 0; i < resultList.size(); i++) {
				Map<String, Object> hypoteekObject = resultList.get(i);
				String zknId = (String) hypoteekObject.get("zkn_id");
				String lndOmMij = (String) hypoteekObject.get("Lnd_Om_Mij");

				Sql = "select B.zkn_id, l.LND_NR_RANGORDE, l.Lnd_Om_ProduktNaam, l.Lnd_Om_Mij,  l.Lnd_Bd_BoeteHoofdsom, Lnd_Bd_Aftrekbaar,Lnd_Pc_RenteAftrek, l.Lnd_Om_LeningNummer ,  l.Lnd_An_Maanden, l.Lnd_Om_Rente, Lnd_An_RentevastMnd,    s.Sce_Value,  l.Lnd_Bd_Aflossing_Maand, l.Lnd_Dt_Rentevast, l.Lnd_Dt_Opgave, l.LND_CD_GARANTIE from Leningdeel L inner join Bestaand B on b.Adv_Id = l.Adv_Id and B.ZKN_GUID = L.LND_GUID_BESTAAND inner join Scenario S on S.Adv_Id = L.Adv_Id and s.Len_Id ="
						+ lenId + " and s.Lnd_Id = L.Lnd_Id where L.adv_id =" + advId + "and L.len_id =" + lenId
						+ " and zkn_Id =" + zknId + "and Lnd_Om_Mij like '%" + lndOmMij
						+ "%'  order by B.zkn_id, l.Lnd_Om_Mij";
				internalResultList = executeQueryGetAllData(sta, Sql);
				int j = i + 1;
				/// outputList.add(internalResultList);
				jsonOutput.put("Output" + j, internalResultList);
			}


			/// jsonOutput.put("searchResult", outputList);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Customized Error=====" + e.toString());
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			closeConnection(conn);
		}
		return jsonOutput;

	}

	@Override
	public String savePersonalData(PersonalDataDto personalDataDto) throws SQLException {
		String output = "";
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(personalDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			System.out.println(jsonInput);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			/// String SPsql = "{call SP_UpdateAdvisoryBoxPersonalData
			/// (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			String SPsql = "{call SP_UpdateAdvisoryBoxPersonalData (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			PreparedStatement ps = conn.prepareStatement(SPsql);
			ps.setString(1, personalDataDto.getClientId());
			ps.setString(2, personalDataDto.getDivisionId());
			ps.setString(3, personalDataDto.getClientRoleId());
			ps.setString(4, personalDataDto.getUserID());
			ps.setString(5, personalDataDto.getUserLoginDetailId());
			ps.setString(6, personalDataDto.getPersonalData().getAnv_Om_PlaatsGeboorte());
			ps.setString(7, personalDataDto.getPersonalData().getNaw_Om_Postcode());
			ps.setString(8, personalDataDto.getPersonalData().getNaw_Om_Voorletters());
			ps.setString(9, personalDataDto.getPersonalData().getNaw_Om_Naam());
			ps.setDate(10, personalDataDto.getPersonalData().getAnv_Dt_Geboorte());
			ps.setString(11, personalDataDto.getPersonalData().getAnv_Om_Email());
			ps.setString(12, personalDataDto.getPersonalData().getAnv_Om_TelefoonPrive());
			ps.setString(13, personalDataDto.getPersonalData().getAnv_Om_TelefoonWerk());
			ps.setString(14, personalDataDto.getPersonalData().getNaw_Om_Voornamen());
			ps.setString(15, personalDataDto.getPersonalData().getNaw_om_iban());
			ps.setString(16, personalDataDto.getPersonalData().getAnv_Om_LegitimatieAfgifte());
			ps.setInt(17, personalDataDto.getPersonalData().getNaw_Id());
			ps.setString(18, personalDataDto.getPersonalData().getNaw_Nr_Huis());
			ps.setString(19, personalDataDto.getPersonalData().getNaw_Om_Straat());
			ps.setString(20, personalDataDto.getPersonalData().getAnv_Om_TelefoonMobiel());
			ps.setString(21, personalDataDto.getPersonalData().getAnv_Cd_Nationaliteit());
			ps.setString(22, personalDataDto.getContactID());

			ps.setString(23, personalDataDto.getPersonalData().getNaw_Cd_Geslacht());
			ps.setString(24, personalDataDto.getPersonalData().getAnv_Cd_BurgelijkeStaat());
			ps.setString(25, personalDataDto.getPersonalData().getCity());
			ps.setString(26, personalDataDto.getPersonalData().getLegitimationIssue());
			ps.setString(27, personalDataDto.getPersonalData().getToBIC());
			ps.setString(28, personalDataDto.getPersonalData().getSocialSecurityNumber());


			String booleanValue = personalDataDto.getPersonalData().getAnv_In_Roken();
			if (booleanValue.equalsIgnoreCase("false")) {
				ps.setObject(29, 1);
			} else {
				ps.setObject(29, 0);
			}
			if (personalDataDto.getPersonalData().getNaw_Om_Voorvoegsel().toString() != null) {
				ps.setString(30, personalDataDto.getPersonalData().getNaw_Om_Voorvoegsel());
			} else {
				ps.setString(30, null);
			}

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				output = rs.getString(1);
			}
		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return output;
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public String saveProductData(ProductsDataToSaveDTO ProductsDataToSaveDTO) throws SQLException {
		String output = "";
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(ProductsDataToSaveDTO);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();

			String SPsql = "{call SP_InsertUpdateAdvisoryBoxProductDetails (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			PreparedStatement ps = conn.prepareStatement(SPsql);
			System.out.println(jsonInput);
			org.json.JSONArray productsList = (org.json.JSONArray) jsonInput.get("productsList");

			ps.setString(1, ProductsDataToSaveDTO.getClientId());
			ps.setString(2, ProductsDataToSaveDTO.getDivisionId());
			ps.setString(3, ProductsDataToSaveDTO.getClientRoleId());
			ps.setString(4, ProductsDataToSaveDTO.getUserID());
			ps.setString(5, ProductsDataToSaveDTO.getUserLoginDetailId());

			for (ProductsList productsListBO : ProductsDataToSaveDTO.getProductsList()) {

				String output2 = productsListBO.getProductSituation();
				if (productsListBO.getProductCategory().equalsIgnoreCase("Hypoteek")) {
					ps.setString(6, "Hypotheken");
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Schade")) {
					ps.setString(6, "Schade");
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Kredieten")) {
					ps.setString(6, "Consumptief krediet");
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Kapitaalverz")) {
					ps.setString(6, "Kapitaalverzekering");
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Uitvaart")) {
					ps.setString(6, "Uitvaart");
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Uit.lijfr")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uitkerende lijfrente")) {
					ps.setString(6, "Uitkerende lijfrente");
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Vermogen")) {
					ps.setString(6, "Vermogen");
				} else {
					ps.setString(6, productsListBO.getProductCategory());
				}
				ps.setString(7, productsListBO.getProductSubcategory());
				ps.setString(8, productsListBO.getProviderName());
				ps.setString(9, productsListBO.getProductName());
				ps.setString(10, productsListBO.getServiceProvider());
				ps.setString(11, productsListBO.getPolicyNumber());
				ps.setString(12, productsListBO.getProductClosed());
				ps.setString(13, productsListBO.getNameFirstInsured());
				ps.setString(14, ProductsDataToSaveDTO.getContactID());
				ps.setString(15, productsListBO.getProductSituation());
				ps.setDate(16, productsListBO.getStartDate());
				if (productsListBO.getProductCategory().equalsIgnoreCase("ORV")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Schade")) {
					ps.setString(17, ProductsDataToSaveDTO.getNameFirstInsurer());
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Hypoteek")
						|| productsListBO.getProductCategory().equalsIgnoreCase("AOV")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Kapitaalverz")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Kredieten")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uitvaart")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uit.lijfr")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Vermogen")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Schade")) {
					ps.setString(17, null);
				}

				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					// output = rs.getString(1);
					output = "Product(s) saved/updated succesfully";
					// output = rs.getString(1);
					jsonOutput.put("status", output);
				}
			}
		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return output;
	}

	public static JSONObject objectToJSONObject(Object object) {
		Object json = null;
		JSONObject jsonObject = null;
		try {
			json = new JSONTokener(object.toString()).nextValue();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (json instanceof JSONObject) {
			jsonObject = (JSONObject) json;
		}
		return jsonObject;
	}

	public static String ConvertMilliSecondsToFormattedDate(String milliSeconds) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Long.parseLong(milliSeconds));
		return simpleDateFormat.format(calendar.getTime());
	}

	public static String getErrorContainingClassAndMethod() {
		final StackTraceElement e = Thread.currentThread().getStackTrace()[2];
		final String s = e.getClassName();
		String errorInMethod = s.substring(s.lastIndexOf('.') + 1, s.length()) + "." + e.getMethodName();
		return "Error in " + errorInMethod + " : ";
	}

	private String convertDateInMilliseconds(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		java.util.Date dateFormat = sdf.parse(date);
		long millis = dateFormat.getTime();

		String dateInMillis = Long.toString(millis);
		return dateInMillis;
	}

	private String convertDateInMillisecondsUpdated(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		java.util.Date dateFormat = sdf.parse(date);
		long millis = dateFormat.getTime();

		String dateInMillis = Long.toString(millis);
		return dateInMillis;
	}

	private void prodcutClosedStatus(ProductsDataDto productsDataDto, Map<String, Object> row) {
		String lenId = productsDataDto.getLenId().toString();
		String productCloseValue = "";
		String productSituation = "";
		if (lenId.equalsIgnoreCase("100")) {
			productCloseValue = "YES";
			productSituation = "CS";
		} else if (lenId.equalsIgnoreCase("1")) {
			productCloseValue = "NO";
			productSituation = "P1";
		} else if (lenId.equalsIgnoreCase("2")) {
			productCloseValue = "NO";
			productSituation = "P2";
		} else if (lenId.equalsIgnoreCase("3")) {
			productCloseValue = "NO";
			productSituation = "P3";
		}
		row.put("productClosed", productCloseValue);
		row.put("productSituation", productSituation);
	}

	private void closeConnection(Connection conn) throws SpringAppRuntimeException {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
				throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
			}
		}
	}

	private List<Map<String, Object>> executeQueryGetAllData(Statement sta, String Sql) throws SQLException {
		ResultSet m_ResultSet = sta.executeQuery(Sql);
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		Map<String, Object> row = null;
		ResultSetMetaData metaData = m_ResultSet.getMetaData();
		Integer columnCount = metaData.getColumnCount();
		while (m_ResultSet.next()) {
			row = new HashMap<String, Object>();
			for (int i = 1; i <= columnCount; i++) {
				if (m_ResultSet.getObject(i) != null) {
					row.put(metaData.getColumnName(i), m_ResultSet.getObject(i).toString());
				} else {
					row.put(metaData.getColumnName(i), null);
				}
			}
			resultList.add(row);
		}
		return resultList;
	}

	@Override
	public JSONObject getProductsDataUpdated(ProductsDataDto productsDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		JSONObject jsonOutputCredit = new JSONObject();
		JSONObject jsonOutputHypoteek = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(productsDataDto);
			System.out.println(json);
			JSONArray outputArray = new JSONArray();
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			Statement sta = conn.createStatement();
			String lenId = jsonInput.get("lenId").toString();
			String Sql = "select d.Dek_Id as productId, av.Kln_Id, av.Anv_Id, d.Adv_Id, d.Dek_Om_Produktnaam as productName, d.Dek_Om_Produktnaam as productSubCategory, case when d.Dek_Om_Produktcode like '%LEV%' then 'Kapitaalverz' when d.Dek_Om_Produktcode like '%UIT%' then 'Uitvaart' when d.Dek_Om_Produktcode like '%BEL%' then 'Vermogen' when d.Dek_Om_Produktcode like '%ORV%' then 'ORV' when d.Dek_Om_Produktcode like '%HLV%' then 'AOV' when d.Dek_Om_Produktcode like '%LYF%' then 'Uit.lijfr' when d.Dek_Om_Produktcode like '%SDV%' then 'Schade' end as productCategory, case when left(d.Dek_Om_Produktcode,1)=0 then 'Exisiting' when left(d.Dek_Om_Produktcode,1)=1 then 'New' when left(d.Dek_Om_Produktcode,1)=2 then 'New' when left(d.Dek_Om_Produktcode,1)=3 then 'New' end as productCodeProposal, d.Dek_Om_Polisnummer as policyNumber, d.Dek_Om_Mij as providerName, convert(Date,d.Dek_Dt_Aanvang,101) as startDate, d.Dek_IN_UWBEMIDDELING as productCloseValue, case d.len_id when 100 then 'CS' when 1 then 'P1' when 2 then 'P2' when 3 then 'P3' end as productSituation, d.dek_An_LooptijdMnd as endDatePolicyDuration, d.dek_In_VerzorgersClausule, d. Dek_Bd_PolisWaarde as policyValue , convert(Date,d.Dek_Dt_PolisWaarde,101) as dt_policyValue, d.Dek_Bd_KapitaalOpgebouwd as capitalBuilt, convert(Date,d.Dek_Dt_KapitaalOpgebouwd,101) as dt_captitalBuilt, d.dek_Om_DekkingNaam, d.Dek_Cd_Lijfrenteclausule as annuityClause, d.dek_Guid_Origineel, d.DEK_IN_UITGEBREID as eXTENDED, d.dek_Om_Bestedingsdoel, d.dek_Om_SoortDepot,d.Dek_In_IsFiscaleVoortzetting,d.dek_cd_Situatie , d.dek_An_Mnd_Hooglaag,k.Kap_An_Mnd as DurationInYears, k.kap_Nr_Aanvrager Avn1, k.kap_Bd_1 as kap1, k.kap_Pc_Doelkapitaal, k.kap_Bd_Voorbeeld, k.kap_Pc_RekenRente, k.kap_Bd_Garantie, k.Kap_Cd_BelastingBox as kapbox, l.kap_Nr_Aanvrager Avn2, l.Kap_Bd_1 as kap2, l.kap_An_Mnd, l.kap_An_Termijn, a.kap_Nr_Aanvrager Anv3, a.Kap_Bd_1 as kap3, a.Kap_An_Mnd as kap_Mnd, a.Kap_An_Termijn as term, b.Kap_Nr_Aanvrager Anv4, b.Kap_Bd_1 as kap4, b.Kap_An_Mnd as kap_Mnd, b.Kap_An_Termijn as terms, c.kap_Nr_Aanvrager Anv5, c.kap_Bd_1 as kap5, c.Kap_An_Mnd as kap_Mnd, c.Kap_An_Termijn as terms, g.kap_Nr_Aanvrager Anv6, g.Kap_Bd_1 as kap6, g.Kap_An_Mnd as kap_Mnd, g.Kap_An_Termijn as terms, NA.naw_Id, (select concat(n.naw_om_naam,' ',n.Naw_Om_Voorletters,' ',n.Naw_Om_Voornamen) from naw n inner join Aanvrager a on a.naw_id=n.naw_id inner join advies ad on ad.kln_id=a.kln_id where a.Anv_Id=2 and ad.adv_id="+ productsDataDto.getAdvId() + ") as nameFirstInsurer, concat(NA.naw_om_naam,' ',NA.Naw_Om_Voorletters,' ',Na.Naw_Om_Voornamen) as nameFirstInsured, isnull(m.Vzn_Id,1) as partnerId, v.anv_Id, w.anv_Id, p.pre_bd as deposit, p.pre_An_LooptijdMnd as endDateCoverage, p.pre_Cd_Frequentie as frequency, p.pre_Pc_Stijging, p.pre_An_Mnd_Stijging, q.pre_Bd as deposit2, q.Pre_An_LooptijdMnd as pre_An_LooptijdMnd, q.Pre_Cd_Frequentie as pre_Cd_Frequency, r.Pre_Bd as deposit3, r.Pre_An_LooptijdMnd as pre_An_LooptijdMnd, r.Pre_Cd_Frequentie as pre_Cd_Frequency, s.Pre_Bd as deposit4, s.Pre_An_LooptijdMnd as pre_An_LooptijdMnd, s.Pre_Cd_Frequentie as pre_Cd_Frequency, lf.LFR_BD_KOOPSOM as lFR_BD_KOOPSOM, lf.LFR_CD_SOORT as lFR_CD_SOORT, lf.Lfr_An_LooptijdMnd as pre_An_LooptijdMnd, lf.Lfr_Pc_Rendement as lfr_Pc_Rentement, lf.LFR_CD_BOXKEUZE as lfr_CD_BOXKEUZE, lf.Lfr_Pc_OvergangPartner as lfr_Pc_OvergangPartner, lf.LFR_CD_UITGANGSPUNT as cd_UITGANGSPUNT, lf.lfr_Bd_Op as bd_Op, (lf.Lfr_Bd_Op +lf.Lfr_Bd_Extra ) as bd_Op_, Lfr_An_LooptijdMndExtra as EndDtPaymentExtra, Lfr_PC_HOOGLAAG as HighLowRatio, lf.Lfr_Cd_Scenario as cd_Scenario, k.Kap_Cd_UitkeringAO ,k.Kap_Dt_Uitkering as EndDatePayment,k.kap_PC_INDEXATIE,k.kap_LT_EIND,v.vrz_Cd_AoPremieVrijstelling,v.vrz_CD_AOPVVERVOLG,v.vrz_An_AoWachttijdWkn,k.kap_Cd_uitgangspunt,v.vrz_Cd_Beroepsklasse, k.kap_cd_Indexatie from Dekking d left outer join Kapitaal k on d.Adv_Id = k.Adv_Id and d.Len_Id = k.Len_Id and d.Dek_Id = k.Dek_Id and k.Kap_Id = 1 left outer join Kapitaal l on d.Adv_Id = l.Adv_Id and d.Len_Id = l.Len_Id and d.Dek_Id = l.Dek_Id and l.Kap_Id = 2 left outer join Kapitaal a on d.Adv_Id = a.Adv_Id and d.Len_Id = a.Len_Id and d.Dek_Id = a.Dek_Id and a.Kap_Id = 3 left outer join Kapitaal b on d.Adv_Id = b.Adv_Id and d.Len_Id = b.Len_Id and d.Dek_Id = b.Dek_Id and b.Kap_Id = 4 left outer join Kapitaal c on d.Adv_Id = c.Adv_Id and d.Len_Id = c.Len_Id and d.Dek_Id = c.Dek_Id and c.Kap_Id = 5 left outer join Kapitaal g on d.Adv_Id = g.Adv_Id and d.Len_Id = g.Len_Id and d.Dek_Id = g.Dek_Id and g.Kap_Id = 6 left outer join VerzNemer n on d.Adv_Id = n.Adv_Id and d.Len_Id = n.Len_Id and d.Dek_Id = n.Dek_Id and n.Vzn_Id = 1 left outer join VerzNemer m on d.Adv_Id = m.Adv_Id and d.Len_Id = m.Len_Id and d.Dek_Id = m.Dek_Id and m.Vzn_Id = 2 left outer join Verzekerde v on d.Adv_Id = v.Adv_Id and d.Len_Id = v.Len_Id and d.Dek_Id = v.Dek_Id and v.Anv_Id = 1 left outer join Verzekerde w on d.Adv_Id = w.Adv_Id and d.Len_Id = w.Len_Id and d.Dek_Id = w.Dek_Id and w.Anv_Id = 2 left outer join Premie p on d.Adv_Id = p.Adv_Id and d.Len_Id = p.Len_Id and d.Dek_Id = p.Dek_Id and p.Pre_Id = 1 left outer join Premie q on d.Adv_Id = q.Adv_Id and d.Len_Id = q.Len_Id and d.Dek_Id = q.Dek_Id and q.Pre_Id = 2 left outer join Premie r on d.Adv_Id = r.Adv_Id and d.Len_Id = r.Len_Id and d.Dek_Id = r.Dek_Id and r.Pre_Id = 3 left outer join Premie s on d.Adv_Id = s.Adv_Id and d.Len_Id = s.Len_Id and d.Dek_Id = s.Dek_Id and s.Pre_Id = 4 left outer join Lijfrente lf on d.Adv_Id = lf.Adv_Id and d.Len_Id = lf.Len_Id and d.Dek_Id = lf.Dek_Id left outer join Advies adv on d.Adv_Id=adv.Adv_Id left outer join Aanvrager av on adv.Kln_Id=av.Kln_Id and av.Anv_Id=1 left outer join Aanvrager avn on adv.Kln_Id=avn.Kln_Id and av.Anv_Id=2 left outer join Naw NA on av.Naw_Id=NA.Naw_Id where d.Adv_Id = "+ productsDataDto.getAdvId() + " and d.len_id = "+productsDataDto.getLenId()+" and (d.Dek_Om_Produktcode like '%LEV%' or d.Dek_Om_Produktcode like '%ORV%' or d.Dek_Om_Produktcode like '%UIT%' or d.Dek_Om_Produktcode like '%LYF%' or d.Dek_Om_Produktcode like '%BEL%' or d.Dek_Om_Produktcode like '%HLV%' ) and NA.naw_id="+productsDataDto.getNawId();
			List<Map<String, Object>> resultList = executeQueryGetAllData(sta, Sql);
			jsonOutput.put("productsListUpdated", resultList);
			//// outputArray.add(resultList);
			String SqlHypotek = null;
//			if(lenId.equalsIgnoreCase("100")){
//				SqlHypotek = "select B.zkn_id, l.lND_NR_RANGORDE,  l.Lnd_Om_ProduktNaam AS productName, 'Hypoteek' AS productCategory, l.Lnd_Om_ProduktNaam AS productSubCategory, l.Lnd_Om_Mij as providerName, (select naw_om_naam from naw left outer join Aanvrager av on av.naw_id=naw.Naw_Id and av.Anv_id=1 left outer join advies adv on adv.Kln_Id=av.Kln_Id where adv.adv_id="+ productsDataDto.getAdvId() + ") as nameFirstInsured, (select naw_om_naam from naw left outer join Aanvrager av on av.naw_id=naw.Naw_Id and av.Anv_id=2 left outer join advies adv on adv.Kln_Id=av.Kln_Id where adv.adv_id="+ productsDataDto.getAdvId() + ") as nameFirstInsurer, case l.len_id when 100 then 'YES' when 1 then 'NO' when 2 then 'NO' when 3 then 'NO' end as productCloseValue, case l.len_id when 100 then 'CS' when 1 then 'P1' when 2 then 'P2' when 3 then 'P3' end as productSituation, iif((L.LND_GUID_BESTAAND is not null),'Exisiting','New') as productCodeProposal, l.lnd_Om_LeningNummer as policyNumber , l.lND_NR_AANVRAGER, l.lND_IN_UWBEMIDDELING, l.lnd_Bd_BoeteHoofdsom, convert(Date,l.Lnd_Dt_Aanvang,101) as startDate, lnd_Bd_Aftrekbaar,lnd_Pc_RenteAftrek, l.lnd_An_Maanden AS endDatePolicyDuration, l.lnd_Om_Rente, lnd_An_RentevastMnd, s.sce_Value, iif((lND_OM_RENTERISICOOPSLAGTABEL like ''),0,1) as auto_rentedaling, l.lnd_Bd_Aflossing_Maand, convert(Date,l.lnd_Dt_Rentevast,101) as lnd_Dt_Rentevast, convert(Date,l.lnd_Dt_Opgave,101) AS lnd_Dt_Opgave, l.lND_CD_GARANTIE from Leningdeel L inner join Bestaand B on b.Adv_Id = l.Adv_Id inner join Scenario S on S.Adv_Id = L.Adv_Id and s.Lnd_Id = L.Lnd_Id and s.Len_Id= "+productsDataDto.getLenId()+" and s.sce_cd_type=2  where L.adv_id = "+ productsDataDto.getAdvId() + " and L.len_id ="+productsDataDto.getLenId()+" and L.Lnd_Om_ProduktNaam!='' order by B.zkn_id, l.lND_NR_RANGORDE";
//			}
//			else{
//				SqlHypotek = "select B.zkn_id, l.lND_NR_RANGORDE,  l.Lnd_Om_ProduktNaam AS productName, 'Hypoteek' AS productCategory, l.Lnd_Om_ProduktNaam AS productSubCategory, l.Lnd_Om_Mij as providerName, (select naw_om_naam from naw left outer join Aanvrager av on av.naw_id=naw.Naw_Id and av.Anv_id=1 left outer join advies adv on adv.Kln_Id=av.Kln_Id where adv.adv_id="+ productsDataDto.getAdvId() + ") as nameFirstInsured, (select naw_om_naam from naw left outer join Aanvrager av on av.naw_id=naw.Naw_Id and av.Anv_id=2 left outer join advies adv on adv.Kln_Id=av.Kln_Id where adv.adv_id="+ productsDataDto.getAdvId() + ") as nameFirstInsurer, case l.len_id when 100 then 'YES' when 1 then 'NO' when 2 then 'NO' when 3 then 'NO' end as productCloseValue, case l.len_id when 100 then 'CS' when 1 then 'P1' when 2 then 'P2' when 3 then 'P3' end as productSituation, iif((L.LND_GUID_BESTAAND is not null),'Exisiting','New') as productCodeProposal, l.lnd_Om_LeningNummer as policyNumber , l.lND_NR_AANVRAGER, l.lND_IN_UWBEMIDDELING, l.lnd_Bd_BoeteHoofdsom, convert(Date,l.Lnd_Dt_Aanvang,101) as startDate, lnd_Bd_Aftrekbaar,lnd_Pc_RenteAftrek, l.lnd_An_Maanden AS endDatePolicyDuration, l.lnd_Om_Rente, lnd_An_RentevastMnd, s.sce_Value, iif((lND_OM_RENTERISICOOPSLAGTABEL like ''),0,1) as auto_rentedaling, l.lnd_Bd_Aflossing_Maand, convert(Date,l.lnd_Dt_Rentevast,101) as lnd_Dt_Rentevast, convert(Date,l.lnd_Dt_Opgave,101) AS lnd_Dt_Opgave, l.lND_CD_GARANTIE from Leningdeel L inner join Bestaand B on b.Adv_Id = l.Adv_Id inner join Scenario S on S.Adv_Id = L.Adv_Id and s.Lnd_Id = L.Lnd_Id and s.Len_Id= "+productsDataDto.getLenId()+" and s.sce_cd_type=2 and s.Div_Id!=0  where L.adv_id = "+ productsDataDto.getAdvId() + " and L.len_id ="+productsDataDto.getLenId()+" and L.Lnd_Om_ProduktNaam!='' order by B.zkn_id, l.lND_NR_RANGORDE";
//			}
			/*String SqlHypotek = "select B.zkn_id, l.lND_NR_RANGORDE, 'Hypoteek' AS  productName, l.Lnd_Om_ProduktNaam AS productCategory, 'Hypoteek' AS productSubCategory, l.Lnd_Om_Mij as providerName, (select naw_om_naam from naw left outer join Aanvrager av on av.naw_id=naw.Naw_Id and av.Anv_id=1 left outer join Aanvrager avn on avn.naw_id=naw.Naw_Id and avn.Anv_id=2 left outer join advies adv on adv.Kln_Id=av.Kln_Id where adv.adv_id="
					+ productsDataDto.getAdvId()
					+ ") as firstInsuredName, case l.len_id when 100 then 'YES' when 1 then 'NO' when 2 then 'NO' when 3 then 'NO' end as productCloseValue, case l.len_id when 100 then 'CS' when 1 then 'P1' when 2 then 'P2' when 3 then 'P3' end as productSituation, l.lnd_Om_LeningNummer as policyNumber , l.lND_NR_AANVRAGER, l.lND_IN_UWBEMIDDELING, l.lnd_Bd_BoeteHoofdsom, convert(Date,l.Lnd_Dt_Aanvang,101) as startDate, lnd_Bd_Aftrekbaar,lnd_Pc_RenteAftrek, l.lnd_An_Maanden, l.lnd_Om_Rente, lnd_An_RentevastMnd, s.sce_Value, iif((lND_OM_RENTERISICOOPSLAGTABEL like ''),0,1) as auto_rentedaling, l.lnd_Bd_Aflossing_Maand, convert(Date,l.lnd_Dt_Rentevast,101) as lnd_Dt_Rentevast, convert(Date,l.lnd_Dt_Opgave,101) AS lnd_Dt_Opgave, l.lND_CD_GARANTIE from Leningdeel L inner join Bestaand B on b.Adv_Id = l.Adv_Id and B.ZKN_GUID = L.LND_GUID_BESTAAND inner join Scenario S on S.Adv_Id = L.Adv_Id and s.Lnd_Id = L.Lnd_Id and s.Len_Id= "
					+ productsDataDto.getLenId() + "  and s.sce_cd_type=2  where L.adv_id = "
					+ productsDataDto.getAdvId() + " and L.len_id = " + productsDataDto.getLenId()
					+ "   order by B.zkn_id, l.lND_NR_RANGORDE";*/
			
			
			/* ERKA 01-08-2018 */
			SqlHypotek = "select 'B.zkn_id' as 'B.zkn_id' , l.lND_NR_RANGORDE, "
					+ " l.Lnd_Om_Mij AS productName, "
					+ "'Hypoteek' AS productCategory, "
					+ "'Hypoteek' AS productSubCategory, "
					+ "l.Lnd_Om_Mij as providerName, (select concat(naw_om_naam,' ',Naw_Om_Voorletters,' ',"
					+ "Naw_Om_Voornamen) from naw left outer join Aanvrager av on av.naw_id=naw.Naw_Id "
					+ "and av.Anv_id=1 left outer join advies adv on adv.Kln_Id=av.Kln_Id where adv.adv_id="
					+ productsDataDto.getAdvId() + ") as nameFirstInsured, "
					+ "(select concat(naw_om_naam,' ',Naw_Om_Voorletters,' ',"
					+ "Naw_Om_Voornamen) from naw left outer join Aanvrager av on av.naw_id=naw.Naw_Id and "
					+ "av.Anv_id=2 left outer join advies adv on adv.Kln_Id=av.Kln_Id where adv.adv_id="
					+ productsDataDto.getAdvId()
					+ ") as nameFirstInsurer, case l.len_id when 100 then 'YES' when 1 then 'NO' when 2 "
					+ "then 'NO' when 3 then 'NO' end as productCloseValue, case l.len_id when 100 "
					+ "then 'CS' when 1 then 'P1' when 2 then 'P2' when 3 then 'P3' "
					+ "end as productSituation, iif((L.LND_GUID_BESTAAND is not null),'Exisiting','New') "
					+ "as productCodeProposal, l.lnd_in_huidig, l.lnd_Om_LeningNummer as policyNumber , "
					+ "l.lND_NR_AANVRAGER, l.lND_IN_UWBEMIDDELING, l.lnd_Bd_BoeteHoofdsom, "
					+ "convert(Date,l.Lnd_Dt_Aanvang,101) as startDate, lnd_Bd_Aftrekbaar,"
					+ "lnd_Pc_RenteAftrek, l.lnd_An_Maanden AS endDatePolicyDuration, l.lnd_Om_Rente, "
					+ "lnd_An_RentevastMnd, 's.sce_Value' as 's.sce_Value', iif((lND_OM_RENTERISICOOPSLAGTABEL like ''),0,1)"
					+ " as auto_rentedaling, l.lnd_Bd_Aflossing_Maand, convert(Date,l.lnd_Dt_Rentevast,101)"
					+ " as lnd_Dt_Rentevast, convert(Date,l.lnd_Dt_Opgave,101) AS lnd_Dt_Opgave, "
					+ "l.lND_CD_GARANTIE from Leningdeel L "
					+ " where L.adv_id = " + productsDataDto.getAdvId()
					+ " and L.len_id =" + productsDataDto.getLenId()
					+ " order by l.lND_NR_RANGORDE"; 			
			
			/* ERKA END */
			
			
			List<Map<String, Object>> resultListHypoteek = executeQueryGetAllData(sta, SqlHypotek);
			jsonOutputHypoteek.put("hypoteekListUpdated", resultListHypoteek);
			
			
			String sqlCredits = "SELECT k.Kre_Nr_Leningnummer as policyNumber, k.Kre_Om_Geldverstrekker as providerName, k.KRE_OM_PRODUCTNAAM as productName, convert(date,k.Kre_Dt_Ingang,101) as startDate, Kre_An_Mnd_Looptijd as endDatePolicyDuration, (select naw_om_naam from naw left outer join Aanvrager av on av.naw_id=naw.Naw_Id and av.Anv_id=1 left outer join advies adv on adv.Kln_Id=av.Kln_Id where adv.adv_id="+productsDataDto.getAdvId()+") as nameFirstInsured, (select naw_om_naam from naw left outer join Aanvrager av on av.naw_id=naw.Naw_Id and av.Anv_id=2 left outer join advies adv on adv.Kln_Id=av.Kln_Id where adv.adv_id="+productsDataDto.getAdvId()+") as nameFirstInsurer, k.KRE_OM_PRODUCTNAAM as productSubCategory, 'Kredieten' AS productCategory, 'FINCCRED' AS produktcode , case k.len_id when 100 then 'YES' when 1 then 'NO' when 2 then 'NO' when 3 then 'NO' end as productCloseValue, case k.len_id when 100 then 'CS' when 1 then 'P1' when 2 then 'P2' when 3 then 'P3' end as productSituation, k.kRE_PC_AFLOSSING, k.kRE_IN_UWBEMIDDELING, k.kre_Bd_Hoofdsom, k.kre_Bd_Restandhoofdsom, k.kre_Pc_Rente, k.kre_Bd_Maandlast, k.kre_Bd_Box1, k.kre_Pc_Box1, k.kre_Bd_Box3, k.kre_Pc_Box3, k.Kre_Dt_EinddatumAftrek as endDatePremium, iif((k.Kre_Cd_Beeindigen1='N'),'Exisiting','New') as productCodeProposal, k.KRE_CD_BESTEDINGSDOEL as creditType, G.anv_Id as geldNemer1, G2.anv_Id as geldNemer2 FROM Kredieten k left outer join GeldNemer G on g.Adv_Id = k.Adv_Id and G.Len_Id = k.Len_Id and G.kre_id=k.Kre_id and G.Gen_Id = 1 left outer join GeldNemer G2 on g2.Adv_Id = k.Adv_Id and G2.Len_Id = k.Len_Id and G.kre_id=k.Kre_id and G2.Gen_Id = 2 left join Advies Adv on adv.adv_id=k.adv_id where k.Adv_Id ="+productsDataDto.getAdvId()+" and k.Len_Id ="+productsDataDto.getLenId()+" order by k.Len_Id" ;
			List<Map<String, Object>> resultListCredits = executeQueryGetAllData(sta, sqlCredits);
			jsonOutputCredit.put("creditsListUpdated", resultListCredits);

			// changes to send all the inputs in one array of
			// productsListUpdated

			java.util.ArrayList creditArray = (ArrayList) jsonOutputCredit.get("creditsListUpdated");
			int creditSize = creditArray.size();

			java.util.ArrayList productsArray = (ArrayList) jsonOutput.get("productsListUpdated");
			for (int i = 0; i < creditSize; i++) {
				Object creditJsonObject = creditArray.get(i);
				productsArray.add(creditJsonObject);
			}
			jsonOutput.remove(creditArray);

			java.util.ArrayList hypoteekArray = (ArrayList) jsonOutputHypoteek.get("hypoteekListUpdated");
			int hypoteekSize = hypoteekArray.size();
			/// JSONArray productsArray = (JSONArray)
			/// jsonOutput.get("productsListUpdated");
			for (int i = 0; i < hypoteekSize; i++) {
				Object hypoteekJsonObject = hypoteekArray.get(i);
				productsArray.add(hypoteekJsonObject);
			}
			jsonOutput.remove(hypoteekArray);

			jsonOutput.put("productsListUpdated", productsArray);

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			closeConnection(conn);
		}
		return jsonOutput;
	}

	private Connection createConnection(org.json.JSONObject jsonInput)
			throws ClassNotFoundException, SQLException, JSONException {
		Connection conn;
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		conn = DriverManager.getConnection(
				jdbcSqlserver + jsonInput.getString("serverName") + user + jsonInput.getString("userName") + password
						+ jsonInput.getString("password") + databaseName + jsonInput.getString("databaseName"));
		return conn;
	}

	@Override
	public String saveProductDataUpdated(ProductsDataToSaveUpdatedDTO productsDataToSaveUpdatedDto)
			throws SQLException {
		String output = "";

		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(productsDataToSaveUpdatedDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			String SPsql = "{call sp_InsertUpdateAdvisoryBoxProductDetails(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

			PreparedStatement ps = conn.prepareStatement(SPsql);
			System.out.println(jsonInput);

			ps.setObject(1, productsDataToSaveUpdatedDto.getClientId());
			ps.setObject(2, productsDataToSaveUpdatedDto.getDivisionId());
			ps.setObject(3, productsDataToSaveUpdatedDto.getClientRoleId());
			ps.setObject(4, productsDataToSaveUpdatedDto.getUserID());
			ps.setObject(5, productsDataToSaveUpdatedDto.getUserLoginDetailId());

			for (ProductsListUpdated productsListBO : productsDataToSaveUpdatedDto.getProductsListUpdated()) {
				String parameterSix = null;
				String output2 = productsListBO.getProductSituation();
				if (productsListBO.getProductCategory().equalsIgnoreCase("Hypoteek")) {
					/// ps.setString(6, "Hypotheken");
					ps.setString(6, "FINCMOR");
					parameterSix = "FINCMOR";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Schade")) {
					/// ps.setString(6, "Schade");
					ps.setString(6, "FINCDAM");
					parameterSix = "FINCDAM";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Kredieten")) {
					/// ps.setString(6, "Consumptief krediet");
					ps.setString(6, "FINCCRED");
					parameterSix = "FINCCRED";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Kapitaalverz")) {
					/// ps.setString(6, "Kapitaalverzekering");
					ps.setString(6, "FINKAP");
					parameterSix = "FINKAP";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Uitvaart")) {
					/// ps.setString(6, "Uitvaart");
					ps.setString(6, "FINCFUN");
					parameterSix = "FINCFUN";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Uit.lijfr")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uitkerende lijfrente")) {
					/// ps.setString(6, "Uitkerende lijfrente");
					parameterSix = "FINCPEN";
					ps.setString(6, "FINCPEN");
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Vermogen")) {
					/// ps.setString(6, "Vermogen");
					ps.setString(6, "ASSSAV");
					parameterSix = "ASSSAV";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("ORV")) {
					/// ps.setString(6, "Vermogen");
					ps.setString(6, "FINCORV");
					parameterSix = "FINCORV";
				}
				else if (productsListBO.getProductCategory().equalsIgnoreCase("AOV")) {
					/// ps.setString(6, "Vermogen");
					ps.setString(6, "FINCAOV");
					parameterSix = "FINCAOV";

				} else {
					ps.setString(6, productsListBO.getProductCategory());
					parameterSix = productsListBO.getProductCategory();
				}
				ps.setString(7, productsListBO.getProductSubCategory());
				ps.setString(8, productsListBO.getProviderName());
				/// ps.setString(9, productsListBO.getProductName());
				ps.setString(9, productsListBO.getProductName());
				// add serviceProvider here
				ps.setString(10, null);
				ps.setString(11, productsListBO.getPolicyNumber());
				ps.setString(12, productsListBO.getProductCloseValue());
				ps.setString(13, productsListBO.getNameFirstInsured());
				ps.setString(14, productsDataToSaveUpdatedDto.getContactID());
				ps.setString(15, productsListBO.getProductSituation());

				String date = productsListBO.getStartDate().toString().replaceAll("-", "/").replaceAll(" 00:00:00.0",
						"");
				// date
				ps.setString(16, date);
				String parameterSeventeen = null;
				/*if (productsListBO.getProductCategory().equalsIgnoreCase("ORV")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Schade")) {
					ps.setString(17, productsDataToSaveUpdatedDto.getNameFirstInsurer());
					parameterSeventeen = productsDataToSaveUpdatedDto.getNameFirstInsurer();
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Hypoteek")
						|| productsListBO.getProductCategory().equalsIgnoreCase("AOV")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Kapitaalverz")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Kredieten")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uitvaart")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uit.lijfr")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Vermogen")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Schade")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uitkerende lijfrente")) {
					ps.setString(17, null);
					parameterSeventeen = null;
				}
				else {
					ps.setString(17, null);
					parameterSeventeen = null;
				}*/
				/*ps.setString(17, productsDataToSaveUpdatedDto.getNameFirstInsurer());
				parameterSeventeen = productsDataToSaveUpdatedDto.getNameFirstInsurer();*/
				
				ps.setString(17, productsListBO.getNameFirstInsurer());
				ps.setString(19, productsListBO.getDek_In_IsFiscaleVoortzetting());
				ps.setString(18, productsListBO.getAnnuityClause());
				ps.setObject(20, null);
				ps.setObject(21, productsListBO.getEndDateCoverage());
				ps.setObject(22, null);
				ps.setObject(23, null);
				ps.setObject(24, productsListBO.getDeposit());
				ps.setObject(25, productsListBO.getFrequency());
				ps.setObject(26, productsListBO.getDeposit());
				// purchase money
				ps.setObject(27, productsListBO.getDeposit2());
				// premium end date
				ps.setString(28, productsListBO.getDek_An_LooptijdMnd());
				// partner insured amount
				ps.setObject(29, productsListBO.getKap2());
				// application insured amount
				ps.setObject(30, productsListBO.getKap1());

				ps.setObject(31, productsListBO.getLfr_CD_BOXKEUZE());
				ps.setObject(32, productsListBO.getPre_An_LooptijdMnd());
				ps.setObject(33, productsListBO.getEndDtPaymentExtra());
				ps.setObject(34, productsListBO.getPolicyEndDateDurationInMonths());
				ps.setObject(35, productsListBO.getLfr_Pc_Rentement());
				ps.setObject(36, productsListBO.getBd_Op());

				// null required here
				if (productsListBO.getProductCategory().equalsIgnoreCase("Vermogen")) {
				ps.setObject(37, productsListBO.getDek_Om_DekkingNaam());
				ps.setObject(38, productsListBO.getDeposit());
				ps.setObject(39, productsListBO.getDek_Om_SoortDepot());
				ps.setObject(40, productsListBO.getKap_Bd_Voorbeeld());
				ps.setObject(41, productsListBO.getCapitalBuilt());
				ps.setObject(42, productsListBO.getDt_captitalBuilt());
				ps.setObject(43, productsListBO.getFrequency());
				ps.setObject(44, productsListBO.getKapbox());
				// to attach pre_bd, can't find
				ps.setObject(45,  productsListBO.getAnnuityClause());
				ps.setObject(46, productsListBO.getDek_Guid_Origineel());
				}
				
				else 
				{
					for (int i = 37; i < 47; i++) {
						ps.setObject(i, null);
					}
				}
				ps.setObject(47, productsListBO.getKap_Bd_Voorbeeld());
				
				ps.setObject(48, productsListBO.getCapitalBuilt());
				/// ps.setObject(48, productsListBO.getDt_policyValue());
				ps.setObject(49, productsListBO.getPolicyValue());

				// ps.setObject(49, productsListBO.getKap_Bd_Voorbeeld());
				ps.setObject(50, productsListBO.getKap1());
				// Integer so put null here
				// ps.setString(51, productsListBO.getPre_An_LooptijdMnd());

				ps.setObject(51, productsListBO.getKap_Bd_Garantie());
				/// ps.setString(51, null);
				ps.setObject(52, null);
				// ps.setObject(53, productsListBO.getDek_An_Mnd_Hooglaag());
				ps.setObject(53, null);
				/// ps.setObject(53, null);
				ps.setObject(54, productsListBO.getFrequency());
				ps.setObject(55, productsListBO.getDeposit2());
				ps.setObject(56, productsListBO.getDeposit());
				/// ps.setObject(56, null);
				ps.setObject(57, null);
				/// ps.setObject(58, productsListBO.getDek_An_LooptijdMnd());
				ps.setObject(58, productsListBO.getEndDatePolicyDuration());

				/// ps.setObject(58, null);
				// hypoteek parameters
				String fiftyNine = null, sixty = null, sixtyOne = null, sixtyTwo = null, sixtyThree = null,
						sixtyFour = null, sixtyFive = null, sixtySix = null, sixtySeven = null, sixtyEight = null,
						sixtyNine = null, seventy = null;
				if (productsListBO.getProductCategory().equalsIgnoreCase("Hypoteek")) {
					ps.setObject(59, productsListBO.getLnd_Bd_BoeteHoofdsom());
					fiftyNine = productsListBO.getLnd_Bd_BoeteHoofdsom();
					ps.setObject(60, productsListBO.getlND_CD_GARANTIE());
					sixty = productsListBO.getlND_CD_GARANTIE();
					ps.setObject(61, productsListBO.getLnd_Bd_Aftrekbaar());
					sixtyOne = productsListBO.getLnd_Bd_Aftrekbaar();
					ps.setObject(62, productsListBO.getLnd_Dt_Opgave());
					sixtyTwo = productsListBO.getLnd_Bd_Aftrekbaar();
					ps.setObject(63, productsListBO.getLnd_Om_Rente());
					sixtyThree = productsListBO.getLnd_Om_Rente();
					ps.setObject(64, productsListBO.getLnd_An_RentevastMnd());
					sixtyFour = productsListBO.getLnd_An_RentevastMnd();
					ps.setObject(65, productsListBO.getLnd_Dt_Rentevast());
					sixtyFive = productsListBO.getLnd_Dt_Rentevast();
					ps.setObject(66, productsListBO.getSce_Value());
					sixtySix = productsListBO.getLnd_Dt_Rentevast();
					ps.setObject(67, null);

					ps.setObject(68, productsListBO.getLnd_Bd_Aftrekbaar());
					sixtyEight = productsListBO.getLnd_Dt_Rentevast();
					ps.setObject(69, productsListBO.getProductSubCategory());
					sixtyNine = productsListBO.getProductSubCategory();
					// ps.setObject(69, null);
				} else {
					for (int i = 59; i < 70; i++) {
						ps.setObject(i, null);
					}
				}
				
				// credits parameters
				String oneHundredFourteen = null, seventyOne = null, seventyTwo = null, seventyThree = null,
						seventyFour = null, seventyFive = null, seventySix = null;
				if (productsListBO.getProductCategory().equalsIgnoreCase("Kredieten")) {
					ps.setObject(70, productsListBO.getKre_Bd_Box3());
					seventy = productsListBO.getKre_Bd_Box3();
					ps.setObject(71, productsListBO.getKre_Bd_Hoofdsom());
					seventyOne = productsListBO.getKre_Bd_Hoofdsom();
					ps.setObject(72, productsListBO.getKre_Bd_Restandhoofdsom());
					seventyTwo = productsListBO.getKre_Bd_Restandhoofdsom();
					ps.setObject(73, productsListBO.getKre_Pc_Rente());
					seventyThree = productsListBO.getKre_Pc_Rente();
					ps.setObject(74, productsListBO.getKre_Bd_Maandlast());
					seventyFour = productsListBO.getKre_Bd_Maandlast();
					ps.setObject(75, productsListBO.getKre_Bd_Box1());
					seventyFive = productsListBO.getKre_Bd_Box1();
					ps.setObject(76, productsListBO.getKre_Bd_Box3());
					seventySix = productsListBO.getKre_Bd_Box3();
					ps.setObject(114, productsListBO.getEndDateCoverage());
					oneHundredFourteen = productsListBO.getKre_Bd_Box3();
				} else {
					for (int i = 70; i < 77; i++) {
						ps.setObject(i, null);
					}
					ps.setObject(114, null);
				}
				/*
				 * for(int j= 77; j<76+17;j++){ ps.setObject(j, null);
				 * 
				 * }
				 */
				String seventySeven = null, seventyEight = null, seventyNine = null, eighty = null, eightyOne = null,
						eightyTwo = null, eightyThree = null, eightyFour = null, eightyFive = null, eightySix = null,
						eightySeven = null, eightyEight = null, eightyNine = null, ninty = null, nintyOne = null,
						nintyTwo = null,hundredAndFifteen = null;
				if (productsListBO.getProductCategory().equalsIgnoreCase("AOV")) {
					ps.setObject(77, productsListBO.getDeposit());
					seventySeven = productsListBO.getDeposit();
					ps.setObject(78, productsListBO.getKap1());
					ps.setObject(79, productsListBO.getKap_An_Mnd());
					ps.setObject(80, productsListBO.getVrz_An_AoWachttijdWkn());
					ps.setObject(81, productsListBO.getKap2());
					ps.setObject(82, productsListBO.getKap3());
					eightyTwo = productsListBO.getKap3();
					ps.setObject(83, productsListBO.getDeposit2());
					eightyThree = productsListBO.getDeposit2();
					ps.setObject(84, productsListBO.getKap_cd_Indexatie());
					eightyFour = productsListBO.getKap_cd_Indexatie();
					ps.setObject(85, productsListBO.getKap_An_Mnd());
					ps.setObject(86, null);
					eightySix =null;
					ps.setObject(87, productsListBO.getKap_Cd_uitgangspunt());
					eightySeven = productsListBO.getFrequency();
					//add here the new commented parameter
					ps.setObject(88,productsListBO.getKap_PC_INDEXATIE());
					eightyEight = productsListBO.getKap_PC_INDEXATIE();
					ps.setObject(89, productsListBO.getKap_An_Mnd());
					ps.setObject(90, productsListBO.getKap_Cd_uitgangspunt());
					ps.setObject(91, productsListBO.getPre_Pc_Stijging());
					nintyOne = productsListBO.getPre_Pc_Stijging();
					ps.setObject(92, productsListBO.getKre_Bd_Box3());
					nintyTwo = productsListBO.getKre_Bd_Box3();
					
					ps.setObject(124, productsListBO.getKap_Cd_UitkeringAO());
					ps.setObject(125, productsListBO.getVrz_Cd_AoPremieVrijstelling());
					ps.setObject(126, productsListBO.getVrz_CD_AOPVVERVOLG());
					ps.setObject(127, productsListBO.getKap_LT_EIND());
					ps.setObject(128, productsListBO.getFrequency());
					ps.setObject(129, productsListBO.getVrz_Cd_Beroepsklasse());
					ps.setObject(130, productsListBO.getKap_Mnd());
					ps.setObject(116, productsListBO.getKap_Cd_UitkeringAO());
					
				}

				else {
					for (int i = 77; i < 93; i++) {
						ps.setObject(i, null);
					}
					for (int i = 124; i < 131; i++) {
						ps.setObject(i, null);
					}
					ps.setObject(116, null);
				}
				String nintyThree = null, nintyFour = null, nintyFive = null, nintySix = null, nintySeven = null,
						nintyEight = null;
				if (productsListBO.getProductCategory().equalsIgnoreCase("ORV")) {
					ps.setObject(122, null);
					if (productsListBO.getDeposit2() != null) {
						BigDecimal bd = new BigDecimal(productsListBO.getDeposit2().toString());
						Long bdValue = bd.longValue();
						ps.setObject(93, bdValue);
					}

					else {
						ps.setObject(93, null);
					}
					nintyThree = productsListBO.getDeposit2();
					ps.setObject(94, productsListBO.getPre_Pc_Stijging());
					nintyFour = productsListBO.getPre_Pc_Stijging();
					ps.setObject(96, productsListBO.getKap_Pc_RekenRente());
					nintySix = productsListBO.getKap_Pc_RekenRente();
					ps.setObject(95, productsListBO.getFrequency());
					nintyFive = productsListBO.getFrequency();
					// may need to change

					if (productsListBO.getKap1() != null) {
						BigDecimal bd2 = new BigDecimal(productsListBO.getKap1());
						Long bdValue2 = bd2.longValue();
						ps.setObject(97, bdValue2);
					}

					else {
						ps.setObject(97, null);
					}
					nintySeven = productsListBO.getKap1();

					if (productsListBO.getKap2() != null) {
						BigDecimal bd3 = new BigDecimal(productsListBO.getKap2());
						Long bdValue3 = bd3.longValue();
						ps.setObject(98, bdValue3);
					} else {
						ps.setObject(98, null);
					}
					nintyEight = productsListBO.getKap2();
					ps.setObject(115, productsListBO.getDurationInYears());
					hundredAndFifteen = productsListBO.getDurationInYears();
					String endDateTime = productsListBO.getEndDatePayment().toString();
					String[] dateTimeArray = endDateTime.split(" ");
					ps.setObject(123, dateTimeArray[0].toString());
					System.out.println(dateTimeArray[0]);
				}

				else {
					for (int i = 93; i < 99; i++) {
						ps.setObject(i, null);
					}
					ps.setObject(115, null);
					ps.setObject(122, null);
					ps.setObject(123, null);
				}
				ps.setObject(122, null);
				String nintyNine = null, hundred = null, hundredOne = null, hundredTwo = null, hundredThree = null,
						hundredFour = null, hundredFive = null,hundredAndSixteen = null,hundredSeventeen=null,hundredEighteen=null,hundredNineteen=null,hundredTwenty=null,hundredTwentyOne=null;
				if (productsListBO.getProductCategory().equalsIgnoreCase("Kapitaalverz")) {
					ps.setObject(100, productsListBO.getKap2());
					hundred = productsListBO.getKap2();
					ps.setObject(101, productsListBO.getKap3());
					hundredOne = productsListBO.getKap3();
					ps.setObject(102, productsListBO.getDek_Om_DekkingNaam());
					hundredTwo = productsListBO.getDek_Om_DekkingNaam();
					ps.setObject(99, productsListBO.getNameFirstInsured());
					nintyNine = productsListBO.getNameFirstInsured();
					ps.setObject(103, productsListBO.getKap_Pc_Doelkapitaal());
					ps.setObject(105, productsListBO.getDt_captitalBuilt());
					hundredFive = productsListBO.getDt_captitalBuilt();
					ps.setObject(104, productsListBO.getDt_policyValue());
					hundredFour = productsListBO.getDt_policyValue();
					ps.setObject(121, productsListBO.getKapbox());
					hundredTwentyOne = productsListBO.getKapbox();
					/*ps.setObject(19,productsListBO.getDek_In_IsFiscaleVoortzetting());
					ps.setObject(18,productsListBO.getAnnuityClause());*/
				}

				else {
					for (int i = 99; i < 106; i++) {
						ps.setObject(i, null);
					}
					ps.setObject(121, null);
				}
				String hundredSix = null;
				if (productsListBO.getProductCategory().equalsIgnoreCase("Vermogen")) {
					ps.setObject(106, productsListBO.getDek_Om_Bestedingsdoel());
					hundredSix = productsListBO.getDek_Om_Bestedingsdoel();
					ps.setObject(117, productsListBO.getKap1());
					hundredSeventeen = productsListBO.getKap1();
					ps.setObject(118, productsListBO.getKap_Pc_Doelkapitaal());
					hundredEighteen = productsListBO.getKap_Pc_Doelkapitaal();
					ps.setObject(119, productsListBO.getPre_An_LooptijdMnd());
					hundredNineteen = productsListBO.getPre_An_LooptijdMnd();
					ps.setObject(120, productsListBO.getDurationInYears());
					hundredNineteen = productsListBO.getDurationInYears();
					/*ps.setObject(19,productsListBO.getDek_In_IsFiscaleVoortzetting());
					ps.setObject(18,productsListBO.getAnnuityClause());*/
					
				}

				else {
					for (int i = 117; i < 121; i++) {
						ps.setObject(i, null);
					}
					ps.setObject(106, null);
					/*ps.setObject(18, null);
					ps.setObject(19, null);*/
				}
				String hundredSeven = null, hundredEight = null, hundredNine = null, hundredTen = null,
						hundredEleven = null, hundredTwelve = null, hundredThirteen = null;
				if (productsListBO.getProductCategory().equalsIgnoreCase("Uit.lijfr")) {
					ps.setObject(107, productsListBO.getLfr_CD_SOORT());
					hundredSeven = productsListBO.getLfr_CD_SOORT();
					ps.setObject(108, productsListBO.getCd_UITGANGSPUNT());
					hundredEight = productsListBO.getCd_UITGANGSPUNT();
					ps.setObject(109, productsListBO.getBd_Op());
					hundredNine = productsListBO.getBd_Op();
					ps.setObject(110, productsListBO.getLfr_Pc_OvergangPartner());
					hundredTen = productsListBO.getLfr_Pc_OvergangPartner();
					ps.setObject(111, productsListBO.getLfr_Pc_OvergangPartner());
					hundredEleven = productsListBO.getLfr_Pc_OvergangPartner();
					ps.setObject(112, productsListBO.getLfr_BD_KOOPSOM());
					hundredTwelve = productsListBO.getLfr_BD_KOOPSOM();
					ps.setObject(113, productsListBO.getCd_Scenario());
					hundredThirteen = productsListBO.getCd_Scenario();
					ps.setObject(116, productsListBO.getKap_Cd_UitkeringAO());
					hundredAndSixteen = productsListBO.getKap_Cd_UitkeringAO();
					
				} else {
					for (int i = 107; i < 114; i++) {
						ps.setObject(i, null);
					}
				}
				ps.setObject(122, null);
				String executionCall = "EXEC sp_InsertUpdateAdvisoryBoxProductDetails @Clientid = "
						+ productsDataToSaveUpdatedDto.getClientId() + ", @Divisionid ="
						+ productsDataToSaveUpdatedDto.getDivisionId() + ", @ClientRoleId ="
						+ productsDataToSaveUpdatedDto.getClientRoleId() + ", @USerId ="
						+ productsDataToSaveUpdatedDto.getUserID() + ", @UserLoginDetailId ="
						+ productsDataToSaveUpdatedDto.getUserLoginDetailId() + ", @CategoryName =" + parameterSix
						+ ", @SubCategoryName = " + productsListBO.getProductSubCategory() + ", @ProviderName ="
						+ productsListBO.getProviderName() + ", @ProductName =" + productsListBO.getProductName()
						+ ", @ServiceProviderName =" + "NULL" + ", @PolicyNumber =" + productsListBO.getPolicyNumber()
						+ ", @ProductClosedBy =" + productsListBO.getProductCloseValue() + ", @FirstInsuredName ="
						+ productsListBO.getNameFirstInsured() + ", @ContactID ="
						+ productsDataToSaveUpdatedDto.getContactID() + ", @ProductSituation ="
						+ productsListBO.getProductSituation() + ", @PolicyStartDate =" + date + ", @FirstInsurerName ="
						+ parameterSeventeen + ", @AnnuityClauseListValueId =" + productsListBO.getAnnuityClause()
						+ ", @FiscalContinuationListValueId =" + "NULL" + ", @GuaranteeCapital = " + "NULL"
						+ ", @ORVCollectiveInsured = " + "NULL" + ", @PremiumAnnualIndex = " + "NULL"
						+ ", @CoveragePercentage = " + "NULL" + ", @ORVPremium =" + productsListBO.getDeposit()
						+ ", @PremiumTerms =" + productsListBO.getPre_Cd_Frequency() + ", @UITPremium ="
						+ productsListBO.getDeposit() + ", @PurchaseMonery =" + productsListBO.getDeposit2()
						+ ", @PremiumEndDate =" + "NULL" + ", @PartnerInsuredAmount =" + productsListBO.getKap2()
						+ ", @ApplicantInsuredAmount =" + productsListBO.getKap1()
						+ ", @UIT_LIJPolicyTaxboxListValueId = " + productsListBO.getLfr_CD_BOXKEUZE()
						+ ", @EndDateOfPayingPensionPlan =" + productsListBO.getPre_An_LooptijdMnd()
						+ ", @HighPaymentEndDate =" + productsListBO.getDeposit() + ", @HighLowRatio ="
						+ productsListBO.getDurationInMonths() + ", @PayingPensionTransitionPercentage ="
						+ productsListBO.getLfr_Pc_Rentement() + ", @PayingPeriodHigh =" + productsListBO.getBd_Op()
						+ ", @PolicyTaxBoxListValueId =" + productsListBO.getDek_Om_DekkingNaam() + ", @Premium ="
						+ productsListBO.getCapitalBuilt() + ", @TypeOfDepotListValueId ="
						+ productsListBO.getDek_Om_SoortDepot() + ", @ReturnTargetCapital ="
						+ productsListBO.getKap_Bd_Voorbeeld() + ", @Vermogen_AccumulatedValue ="
						+ productsListBO.getDeposit() + ", @Vermogen_DateOfAccumulatedValue ="
						+ productsListBO.getDt_captitalBuilt() + ", @Vermogen_TermsOfPayment ="
						+ productsListBO.getFrequency() + ", @TermOfWithdrawals =" + "NULL"
						+ ", @PeriodicWithdrawalAmount = " + "NULL" + ", @ExtraWithdrawals = " + "NULL"
						+ ", @Characteristics = " + "NULL" + ", @AccumulatedValue =" + productsListBO.getCapitalBuilt()
						+ ", @RedemptionValue =" + productsListBO.getPolicyValue() + ", @GoalCapital ="
						+ productsListBO.getKap1() + ", @EndDateOfPremiumPayments ="
						+ productsListBO.getKap_Bd_Garantie() + ", @HighLowPremiumSetup =" + "NULL"
						+ ", @HighLowRelation = " + "NULL" + ", @TermsOfPremiumPaymentId ="
						+ productsListBO.getPre_Cd_Frequency() + ", @PremiumRiskPart =" + productsListBO.getDeposit2()
						+ ", @PremiumDepot =" + productsListBO.getDeposit() + ", @PremiumDeposite =" + "NULL"
						+ ", @PolicyEndDuration =" + productsListBO.getDurationInMonths() + ", @PrincipalAmount ="
						+ fiftyNine + ", @GuranteeListValueId =" + productsListBO.getlND_CD_GARANTIE() + ", @CurrentRemainingDebt = " + sixtyOne
						+ ", @DateOfRemainingDebt =" + productsListBO.getLnd_Dt_Opgave() + ", @InterestTypeListValueId =" + sixtyThree
						+ ", @InterestFixPeriodListValueId =" + sixtyFour + ", @EndDateInterestFix =" + sixtyFive
						+ ", @InterestPercentage = " + "NULL" + ", @NoOfLoanParts =" + "NULL" + ", @PartBox1 ="
						+ " NULL" + ", @PaymentScheme = " + sixtyNine + ", @Partbox3 ="
						+ productsListBO.getKre_Bd_Box3() + ", @Kredit_PrincipleAmount =" + seventyOne
						+ ", @RemainigPrincipalAmount =" + seventyTwo + ", @RedemeptionPercentage =" + seventyThree
						+ ", @MonthlyCharges =" + seventyFour + ", @Krediet_PartBox1 =" + seventyFive
						+ ", @Krediet_PartBox3 =" + seventySix + ", @AOVPremium =" + seventySeven + ", @income ="
						+ "NULL" + ", @PaymentCoverage = " + "NULL" + ", @OwnRiskPeriodA = " + "NULL"
						+ ", @AOTypeOfMonthlyPaymentA = " + "NULL" + ", @insuredAmtA =" + eightyTwo
						+ ", @FollowUpPremium =" + eightyThree + ", @PaymentTermListValueId =" + eightyFour
						+ ", @AOVCollectiveInsured =" + "NULL" + ", @ExtendedCoverage =" + eightySix
						+ ", @CoverageFixedAmt =" + eightySeven + ", @AnnualIndexA =" + eightyEight + ", @InsuredAmtB ="
						+ "NULL" + ", @EndAgePaymentsB =" + "NULL" + ", @PremiumAnnualIndex =" + nintyOne
						+ ", @HousingCoverageByInabilityBenifitPArtiallyAO =" + nintyTwo + ", @ORVInitialDeposit ="
						+ nintyThree + ", @ORVIncreasePercentage =" + nintyFour + ", @ORVPaymentTerms =" + nintyFive
						+ ", @ORVAnnuityPercentage =" + nintySix + ", @ORVInsuredAmtApp1 =" + nintySeven
						+ ", @ORVInsuredAmtApp2 =" + nintyEight + ", @NameOfContracterInsured =" + nintyNine
						+ ", @DeathBenefitTerm1 =" + hundred + ", @DeathBenefitTerm2 =" + hundredOne
						+ ", @KaptiaalType =" + hundredTwo + ", @KapAnnuityClause =" + "NULL"
						+ " @DateOfAccumulatedCapital =" + hundredFour + ", @DateOfRedemptionCaptial =" + hundredFive
						+ ", @Goal =" + hundredSix + ", @UIT_LIFTypeOfPensionSavingPlan =" + hundredSeven
						+ ", @TypeOfCalculation =" + hundredEight + ", @AnnuityAmount =" + hundredNine
						+ ", @AnnutiyPercentage =" + hundredTen + ", @TransitionAmount =" + hundredEleven
						+ ", @UIT_LIFPremium =" + hundredTwelve + ", @UIT_LIFTermsOfPayment =" + hundredThirteen
						+ ",@EndDatePremium_Kredit =" + oneHundredFourteen+"@EndDatePremiumORV="+hundredAndFifteen;
				procedureQueryLogs(executionCall);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					// output = rs.getString(1);
					// output = "Product(s) saved/updated succesfully";
					output = rs.getString(1);
					jsonOutput.put("status", output);
				}
			}
		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return output;
	}

	@Override
	public String updateXmlData(XMLDataDTO xmlData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveProductDataUpdatedProposal(ProductsDataToSaveUpdatedDTO productsDataToSaveUpdatedDto)
			throws SQLException {
		String output = "";

		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		String parameterSeventeen =null;
		String parameterNine = null;
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(productsDataToSaveUpdatedDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			String SPsql = "{call sp_InsertUpdateAdvisoryBoxOpportunityProductDetails(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

			PreparedStatement ps = conn.prepareStatement(SPsql);
			System.out.println(jsonInput);
			ps.setObject(1, productsDataToSaveUpdatedDto.getClientId());
			ps.setObject(2, productsDataToSaveUpdatedDto.getDivisionId());
			ps.setObject(3, productsDataToSaveUpdatedDto.getClientRoleId());
			ps.setObject(4, productsDataToSaveUpdatedDto.getUserID());
			ps.setObject(5, productsDataToSaveUpdatedDto.getUserLoginDetailId());
			ps.setString(7, productsDataToSaveUpdatedDto.getOpportunityId());
			ps.setString(6, productsDataToSaveUpdatedDto.getIsActive());
			ps.setString(8, productsDataToSaveUpdatedDto.getProductServiceSubjectCategory());

			for (ProductsListUpdated productsListBO : productsDataToSaveUpdatedDto.getProductsListUpdated()) {

				String output2 = productsListBO.getProductSituation();
				
				if (productsListBO.getProductCategory().equalsIgnoreCase("Hypoteek")) {
					/// ps.setString(6, "Hypotheken");
					ps.setString(9, "FINCMOR");
					parameterNine = "FINCMOR";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Schade")) {
					/// ps.setString(6, "Schade");
					ps.setString(9, "FINCDAM");
					parameterNine="FINCDAM";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Kredieten")) {
					/// ps.setString(6, "Consumptief krediet");
					ps.setString(9, "FINCCRED");
					parameterNine="FINCCRED";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Kapitaalverz")) {
					/// ps.setString(6, "Kapitaalverzekering");
					ps.setString(9, "FINKAP");
					parameterNine="FINKAP";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Uitvaart")) {
					/// ps.setString(6, "Uitvaart");
					ps.setString(9, "FINCFUN");
					parameterNine="FINCFUN";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Uit.lijfr")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uitkerende lijfrente")) {
					/// ps.setString(6, "Uitkerende lijfrente");

					ps.setString(9, "FINCPEN");
					parameterNine="FINCPEN";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Vermogen")) {
					/// ps.setString(6, "Vermogen");
					ps.setString(9, "ASSSAV");
					parameterNine="ASSSAV";
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("ORV")) {
					/// ps.setString(6, "Vermogen");
					ps.setString(9, "FINCORV");
					parameterNine="FINCORV";

				}
				else if (productsListBO.getProductCategory().equalsIgnoreCase("AOV")) {
					/// ps.setString(6, "Vermogen");
					ps.setString(9, "FINCAOV");
					parameterNine="FINCAOV";

				} else {
					ps.setString(9, productsListBO.getProductCategory());
					parameterNine=productsListBO.getProductCategory();
				}
				ps.setString(10, productsListBO.getProductSubCategory());
				ps.setString(11, productsListBO.getProviderName());

				ps.setString(12, productsListBO.getPolicyNumber());
				ps.setString(13, productsListBO.getProductName());
				ps.setString(14, productsListBO.getStartDate());
				ps.setString(15, productsListBO.getDek_An_LooptijdMnd());
				ps.setString(16, productsListBO.getNameFirstInsured());
				/*if (productsListBO.getProductCategory().equalsIgnoreCase("ORV")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Schade")) {
					ps.setString(17, productsDataToSaveUpdatedDto.getNameFirstInsurer());
				    parameterSeventeen = productsDataToSaveUpdatedDto.getNameFirstInsurer();
				} else if (productsListBO.getProductCategory().equalsIgnoreCase("Hypoteek")
						|| productsListBO.getProductCategory().equalsIgnoreCase("AOV")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Kapitaalverz")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Kredieten")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uitvaart")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uit.lijfr")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Vermogen")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Schade")
						|| productsListBO.getProductCategory().equalsIgnoreCase("Uitkerende lijfrente")) {
					ps.setString(17, null);
					parameterSeventeen = null;
				}

				else {
					ps.setString(17, null);
					parameterSeventeen = null;
				}*/
				ps.setString(17, productsDataToSaveUpdatedDto.getNameFirstInsurer());
				if (productsListBO.getProductCategory().equalsIgnoreCase("ORV")) {
					ps.setObject(18, productsListBO.getKap1());
					ps.setObject(19, productsListBO.getKap2());
					ps.setObject(20, productsListBO.getPre_An_LooptijdMnd());
					ps.setObject(21, productsListBO.getFrequency());
					ps.setObject(22, productsListBO.getDeposit());
					ps.setObject(23, productsListBO.getPre_An_Mnd_Stijging());
					ps.setObject(24, productsListBO.getDeposit2());
				} else {
					for (int i = 18; i < 25; i++) {
						ps.setObject(i, null);
					}
				}
				if (productsListBO.getProductCategory().equalsIgnoreCase("Kredieten")) {
					ps.setObject(25, productsListBO.getKre_Bd_Hoofdsom());
					ps.setObject(26, productsListBO.getKre_Bd_Restandhoofdsom());
					ps.setObject(27, productsListBO.getKre_Pc_Rente());
					ps.setObject(28, productsListBO.getKre_Bd_Maandlast());
					ps.setObject(29, productsListBO.getKre_Bd_Box1());
					ps.setObject(30, productsListBO.getKre_Bd_Box3());
					ps.setObject(31, productsListBO.getEndDatePremium());
				} else {
					for (int i = 25; i < 32; i++) {
						ps.setObject(i, null);
					}
				}

				if (productsListBO.getProductCategory().equalsIgnoreCase("AOV")) {
					ps.setObject(32, productsListBO.getKap1());
					ps.setObject(33, productsListBO.getKap2());

				} else {
					for (int i = 32; i < 34; i++) {
						ps.setObject(i, null);
					}
				}

				if (productsListBO.getProductCategory().equalsIgnoreCase("Kapitaalverz")) {
					ps.setObject(34, null);
					ps.setObject(35, productsListBO.getCapitalBuilt());
					ps.setObject(36, productsListBO.getDt_captitalBuilt());
					ps.setObject(37, null);
					ps.setObject(38, productsListBO.getDek_Guid_Origineel());
					ps.setObject(39, productsListBO.getPre_An_LooptijdMnd());
					ps.setObject(40, productsListBO.getDek_An_Mnd_Hooglaag());
					ps.setObject(41, productsListBO.getDeposit2());
					ps.setObject(42, productsListBO.getFrequency());
					ps.setObject(43, productsListBO.getDeposit());
					ps.setObject(44, productsListBO.getKap2());
					ps.setObject(45, productsListBO.getKap_An_Mnd());

				}

				else {
					for (int i = 34; i < 46; i++) {
						ps.setObject(i, null);
					}
				}

				if (productsListBO.getProductCategory().equalsIgnoreCase("Uit.lijfr")) {
					ps.setObject(46, productsListBO.getCd_UITGANGSPUNT());
					ps.setObject(47, productsListBO.getDeposit());
					ps.setObject(48, productsListBO.getLfr_CD_BOXKEUZE());
					ps.setObject(49, productsListBO.getPre_An_LooptijdMnd());
					ps.setObject(50, productsListBO.getEndDtPaymentExtra());
					ps.setObject(51, productsListBO.getHighLowRatio());
					ps.setObject(52, productsListBO.getLfr_Pc_Rentement());
					ps.setObject(53, productsListBO.getLfr_Pc_OvergangPartner());
					ps.setObject(54, productsListBO.getCd_Scenario());
					ps.setObject(55, productsListBO.getBd_Op());

				}

				else {
					for (int i = 46; i < 56; i++) {
						ps.setObject(i, null);
					}
				}
				
				if (productsListBO.getProductCategory().equalsIgnoreCase("VERMOGEN")) {
					ps.setObject(56, productsListBO.getAnnuityClause());
					ps.setObject(57, productsListBO.getDek_Guid_Origineel());
					ps.setObject(58, productsListBO.getKap1());
					ps.setObject(59, productsListBO.getKap_Pc_RekenRente());
					ps.setObject(60, productsListBO.getPre_An_LooptijdMnd());
					ps.setObject(61, productsListBO.getPre_An_LooptijdMnd());
					ps.setObject(62, productsListBO.getFrequency());
					ps.setObject(63, productsListBO.getDeposit());
				}

				else {
					for (int i = 56; i < 64; i++) {
						ps.setObject(i, null);
					}
				}
				
				if (productsListBO.getProductCategory().equalsIgnoreCase("UITVAART")) {
					ps.setObject(64, productsListBO.getKap1());
					ps.setObject(65, productsListBO.getKap2());
					ps.setObject(66, productsListBO.getDek_An_LooptijdMnd());
					ps.setObject(67, productsListBO.getFrequency());
					ps.setObject(68, productsListBO.getDeposit());
					ps.setObject(69, productsListBO.getDeposit2());
				}

				else {
					for (int i = 64; i < 70; i++) {
						ps.setObject(i, null);
					}
				}
				ps.setObject(70, productsListBO.getProductCodeProposal());
				if (productsListBO.getProductCategory().equalsIgnoreCase("Hypoteek")) {
					ps.setObject(71, productsListBO.getLnd_Bd_BoeteHoofdsom());
					ps.setObject(72, productsListBO.getlND_CD_GARANTIE());
					ps.setObject(73, productsListBO.getLnd_Bd_Aftrekbaar());
					ps.setObject(74, productsListBO.getLnd_Dt_Opgave());
					ps.setObject(75, productsListBO.getLnd_Om_Rente());
					ps.setObject(76, productsListBO.getLnd_An_RentevastMnd());
					ps.setObject(77, productsListBO.getLnd_Dt_Rentevast());
					ps.setObject(78, productsListBO.getSce_Value());
					ps.setObject(79, null);
					ps.setObject(80, productsListBO.getLnd_Bd_Aftrekbaar());
					ps.setObject(81, productsListBO.getProductSubCategory());
				}
				else {
					for (int i = 71; i < 82; i++) {
						ps.setObject(i, null);
					}
				}
				String executionCall = "@ClientId uniqueidentifier ="+productsDataToSaveUpdatedDto.getClientId()+", @DivisionId uniqueidentifier ="+productsDataToSaveUpdatedDto.getDivisionId()+", @ClientRoleId uniqueidentifier ="+productsDataToSaveUpdatedDto.getClientRoleId()+", @UserId uniqueidentifier ="+productsDataToSaveUpdatedDto.getUserID()+", @UserLogindetailId uniqueidentifier ="+productsDataToSaveUpdatedDto.getUserLoginDetailId()+", @IsActive char(1)="+productsDataToSaveUpdatedDto.getIsActive()+", @OpportunityId uniqueidentifier ="+productsDataToSaveUpdatedDto.getOpportunityId()+", @ProductServiceSubjectCategory Char(1)="+productsDataToSaveUpdatedDto.getProductServiceSubjectCategory()+", @ProductServiceId Nvarchar(MAX)="+parameterNine+", @SubCategoryName NVARCHAR(100)="+productsListBO.getProductSubCategory()+", @ProviderName NVARCHAR(MAX)="+productsListBO.getProviderName()+", @policyNumber NVARCHAR(MAX)="+productsListBO.getPolicyNumber()+", @ProductName NVARCHAR(MAX)="+productsListBO.getProductName()+", @StartDate NVARCHAR(MAX)="+productsListBO.getStartDate()+", @EndDateDurationInMonths NVARCHAR(MAX)="+productsListBO.getDek_An_LooptijdMnd()+", @FirstInuredName NVARCHAR(MAX)="+productsListBO.getNameFirstInsured()+", @firstInsurerName NVARCHAR(MAX)="+parameterSeventeen+", @O_InsuredAmtAppl1 NVARCHAR(MAX)="+productsListBO.getKap1()+", @O_InsuredAmtAppl2 NVARCHAR(MAX)="+productsListBO.getKap2()+", @O_EndDateOfPremiumPayments NVARCHAR(MAX)="+productsListBO.getPre_An_LooptijdMnd()+", @O_PremiumPaymentTerms NVARCHAR(MAX)="+productsListBO.getPre_An_LooptijdMnd()+", @O_Premium NVARCHAR(MAX)="+productsListBO.getDeposit()+", @O_PremiumGainPercentage NVARCHAR(MAX)="+productsListBO.getPre_An_Mnd_Stijging()+", @O_InitialDeposite NVARCHAR(MAX)="+productsListBO.getDeposit2()+", @K_CreditLimit NVARCHAR(MAX)="+productsListBO.getKre_Bd_Hoofdsom()+", @K_PrincipleAmount NVARCHAR(MAX)="+productsListBO.getKre_Bd_Restandhoofdsom()+", @K_AnnualInterestPercentage NVARCHAR(MAX)="+productsListBO.getKre_Pc_Rente()+", @K_MonthlyPayment NVARCHAR(MAX)="+productsListBO.getKre_Bd_Maandlast()+", @K_PartBox1 NVARCHAR(MAX)="+productsListBO.getKre_Bd_Box1()+", @K_PartBox3 NVARCHAR(MAX)="+productsListBO.getKre_Bd_Box3()+", @K_EndDatePremium NVARCHAR(MAX)="+productsListBO.getEndDatePremium()+", @A_InsuredAmtA NVARCHAR(MAX)="+productsListBO.getKap1()+", @A_InsuredAmtB NVARCHAR(MAX)="+productsListBO.getKap2()+", @KA_ExampleCapital4Percent NVARCHAR(MAX)="+null+", @KA_AccumulatedValue NVARCHAR(MAX)="+productsListBO.getCapitalBuilt()+", @KA_DateOfAccumulatedCapital NVARCHAR(MAX)="+productsListBO.getDt_captitalBuilt()+", @KA_AnnuityClauseListValueId NVARCHAR(MAX)="+null+", @KA_FiscalContinuationListValueId NVARCHAR(MAX)="+productsListBO.getDek_Guid_Origineel()+", @KA_EndDateOfPremiumPayments NVARCHAR(MAX)="+productsListBO.getPre_An_LooptijdMnd()+", @KA_HighLowPremiumPaymentYears NVARCHAR(MAX)="+productsListBO.getDek_An_Mnd_Hooglaag()+", @KA_HighLowPremiumSetup NVARCHAR(MAX)="+productsListBO.getDeposit2()+", @KA_TermOfPremiumPaymentId NVARCHAR(MAX)="+productsListBO.getFrequency()+", @KA_DeductablePremium NVARCHAR(MAX)="+productsListBO.getDeposit()+", @KA_GoalCapital NVARCHAR(MAX)="+productsListBO.getKap2()+", @KA_GoalCapitalEndDate NVARCHAR(MAX)="+productsListBO.getKap_An_Mnd()+", @UL_TypeOfCalculation NVARCHAR(MAX)="+productsListBO.getCd_UITGANGSPUNT()+", @UL_PurchaseAmount NVARCHAR(MAX)="+productsListBO.getDeposit()+", @UL_PolicyTaxBoxListValueId NVARCHAR(MAX)="+productsListBO.getLfr_CD_BOXKEUZE()+", @UL_EndDateOfPayingPensionPlan NVARCHAR(MAX)="+productsListBO.getPre_An_LooptijdMnd()+", @UL_HighPaymentEndDate NVARCHAR(MAX)="+productsListBO.getEndDtPaymentExtra()+", @UL_HighLowRatio NVARCHAR(MAX)="+productsListBO.getHighLowRatio()+", @UL_PayingPensionTransitionPercentage NVARCHAR(MAX)="+productsListBO.getLfr_Pc_Rentement()+", @UL_PensionTransitionSecondPerson NVARCHAR(MAX)="+productsListBO.getLfr_Pc_OvergangPartner()+", @UL_AnnuityAmount NVARCHAR(MAX)="+productsListBO.getCd_Scenario()+", @UL_PayingPensionPlanAmt NVARCHAR(MAX)="+productsListBO.getBd_Op()+", @V_AnnuityClauseListValueId NVARCHAR(MAX)="+productsListBO.getAnnuityClause()+", @V_PolicyTaxBoxlistValueId NVARCHAR(MAX)="+productsListBO.getDek_Guid_Origineel()+", @V_TargetCapital NVARCHAR(MAX)="+ productsListBO.getKap1()+", @V_ReturnTargetCapital NVARCHAR(MAX)="+productsListBO.getKap_Pc_RekenRente()+", @V_EndDateOfPremiumPayments NVARCHAR(MAX)="+productsListBO.getPre_An_LooptijdMnd()+", @V_HighLowPremiumPaymentYears NVARCHAR(MAX)="+productsListBO.getPre_An_LooptijdMnd()+", @V_TermsOfPremiumPaymentsId NVARCHAR(MAX)="+productsListBO.getFrequency()+", @V_HighLowPremiumSetup NVARCHAR(MAX)="+productsListBO.getDeposit()+", @U_ApplicantInsuredAmount NVARCHAR(MAX)="+productsListBO.getKap1()+", @U_PartnerInsuredAmount NVARCHAR(MAX)="+productsListBO.getKap2()+", @U_PremiumPaymentsEndDate NVARCHAR(MAX)="+productsListBO.getDek_An_LooptijdMnd()+", @U_PaymentPeriodListValueId NVARCHAR(MAX)="+ productsListBO.getFrequency()+", @U_Premium NVARCHAR(MAX)="+productsListBO.getDeposit()+", @U_PurchaseMoney NVARCHAR(MAX)="+productsListBO.getDeposit2();
				procedureQueryLogs(executionCall);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					output = rs.getString(1);
					jsonOutput.put("status", output);
				}
			}
		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return output;
	}

	@Override
	public JSONObject getKlnForAdviseData(AdviseDataDto adviseDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		String kln_Id = null, anv_Id = null, adv_Id = null;
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(adviseDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			Statement sta = conn.createStatement();
			String Sql = "select Kln_Id from Aanvrager where Naw_Id=" + adviseDataDto.getNawId();
			ResultSet m_ResultSet = sta.executeQuery(Sql);
			while (m_ResultSet.next()) {
				kln_Id = m_ResultSet.getString("Kln_Id");
				jsonOutput.put("klnId", kln_Id);
			}} catch (Exception e) {
				e.printStackTrace();
				String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
				throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
			} finally {
				closeConnection(conn);
			}
			return jsonOutput;
	}

	@Override
	public JSONObject getListOfAdviseData(AdviseDataDto adviseDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		String kln_Id = null, anv_Id = null, adv_Id = null;
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(adviseDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			Statement sta = conn.createStatement();
			kln_Id =jsonInput.get("klnId").toString();
		String queryAdviseData = "select * from Advies where Kln_Id=" + kln_Id;
		ResultSet m_ResultSetAdviseData = sta.executeQuery(queryAdviseData);
		List adviseList = new ArrayList();
		Map<String, Object> row = null;
		ResultSetMetaData metaData = m_ResultSetAdviseData.getMetaData();
		Integer columnCount = metaData.getColumnCount();
		while (m_ResultSetAdviseData.next()) {
			row = new HashMap<String, Object>();
			for (int i = 1; i <= columnCount; i++) {
				row.put("Adv_Id", m_ResultSetAdviseData.getObject("Adv_Id"));
				row.put("Adv_Dt_Aanmaak", m_ResultSetAdviseData.getObject("Adv_Dt_Aanmaak").toString());
				row.put("Adv_Om", m_ResultSetAdviseData.getObject("Adv_Om"));
				row.put("ADV_OM_PAKKETNAAM", m_ResultSetAdviseData.getObject("ADV_OM_PAKKETNAAM"));
				row.put("Adv_Dt_Mutatie", m_ResultSetAdviseData.getObject("Adv_Dt_Mutatie").toString());
				row.put("Adv_Om_Klant", m_ResultSetAdviseData.getObject("Adv_Om_Klant"));
			}
			row.put("klnId", kln_Id);
			adviseList.add(row);
		}
		System.out.println(adviseList);
		jsonOutput.put("allAdviseData", adviseList);
		//// jsonOutput.put("allAdviseData", null);
	} catch (Exception e) {
		e.printStackTrace();
		String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
		throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
	} finally {
		closeConnection(conn);
	}
	return jsonOutput;
	}
	
	@Override
	public String checkServicesStatus() {
		// TODO Auto-generated method stub
		return "Services are running";
	}

	@Override
	public JSONObject getPartnerNawAnvId(PersonalDataDto personalDataDto) {
		Connection conn = null;
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(personalDataDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			Statement sta = conn.createStatement();
			String Sql = "select av.Naw_Id as partnerNawId,av.anv_id as partnerAnvId from Aanvrager av "
					+ "where (av.Kln_Id=(select kln_id from Aanvrager where naw_id=" + personalDataDto.getNawId()
					+ ") and naw_id!=" + personalDataDto.getNawId()
					+ ") union select 0,0 from Aanvrager where not exists(select av.Naw_Id "
					+ "as partnerNawId,av.anv_id as partnerAnvId from Aanvrager av "
					+ "where (av.Kln_Id=(select kln_id from Aanvrager where naw_id=" + personalDataDto.getNawId()
					+ ")) and naw_id!=" + personalDataDto.getNawId() + ")";
			ResultSet m_ResultSet = sta.executeQuery(Sql);
			Map<String, Object> row = null;
			ResultSetMetaData metaData = m_ResultSet.getMetaData();
			Integer columnCount = metaData.getColumnCount();

			while (m_ResultSet.next()) {
				String partnerNawIdId = m_ResultSet.getString("partnerNawId");
				String partnerAnvId = m_ResultSet.getString("partnerAnvId");

				jsonOutput.put("partnerNawId", partnerNawIdId);
				jsonOutput.put("partnerAnvId", partnerAnvId);
			}

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			closeConnection(conn);
		}
		return jsonOutput;
	}

}