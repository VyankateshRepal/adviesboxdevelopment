package com.zcon.vmh.dto;

public class SearchParametersDto {

	private String serviceName;
	private String indexName;
	private String apiKey;
	private String search;
	private String clientId;
	private String query;
	
	private int top;
	private int skip;
	private String orderby;

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public int getTop() {
		return top;
	}

	public void setTop(int top) {
		this.top = top;
	}

	public int getSkip() {
		return skip;
	}

	public void setSkip(int skip) {
		this.skip = skip;
	}

	public String getOrderby() {
		return orderby;
	}

	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}
	
	@Override
	public String toString() {
		return "SearchParametersDto [serviceName=" + serviceName + ", indexName=" + indexName + ", apiKey=" + apiKey
				+ ", search=" + search + ", clientId=" + clientId + ", query=" + query + "]";
	}

}
