package com.zcon.vmh.dto;

public class SearchContactDto {

	private String lastName;
	private String email;
	private String postCode;
	private String serverName;
	private String userName;
	private String password;
	private String databaseName;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	@Override
	public String toString() {
		return "SearchContactDto [lastName=" + lastName + ", email=" + email + ", postCode=" + postCode
				+ ", serverName=" + serverName + ", userName=" + userName + ", password=" + password + ", databaseName="
				+ databaseName + "]";
	}

}
