package com.zcon.vmh.dto;

import java.sql.Date;

public class ProductsListUpdate {

	private String providerName;

	private Date startDate;

	private String productCategory;

	private String productSubcategory;

	private String productSituation;

	private String nameFirstInsurer;

	private String productId;

	private String productClosed;

	private String nameFirstDebtor;

	private String creditType;

	private String contactID;

	private String nameFirstInsured;

	private String pageLoadDateTime;

	private String policyNumber;

	private String productName;

	private String serviceProvider;

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductSubcategory() {
		return productSubcategory;
	}

	public void setProductSubcategory(String productSubcategory) {
		this.productSubcategory = productSubcategory;
	}

	public String getProductSituation() {
		return productSituation;
	}

	public void setProductSituation(String productSituation) {
		this.productSituation = productSituation;
	}

	public String getNameFirstInsurer() {
		return nameFirstInsurer;
	}

	public void setNameFirstInsurer(String nameFirstInsurer) {
		this.nameFirstInsurer = nameFirstInsurer;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductClosed() {
		return productClosed;
	}

	public void setProductClosed(String productClosed) {
		this.productClosed = productClosed;
	}

	public String getNameFirstDebtor() {
		return nameFirstDebtor;
	}

	public void setNameFirstDebtor(String nameFirstDebtor) {
		this.nameFirstDebtor = nameFirstDebtor;
	}

	public String getCreditType() {
		return creditType;
	}

	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}

	public String getContactID() {
		return contactID;
	}

	public void setContactID(String contactID) {
		this.contactID = contactID;
	}

	public String getNameFirstInsured() {
		return nameFirstInsured;
	}

	public void setNameFirstInsured(String nameFirstInsured) {
		this.nameFirstInsured = nameFirstInsured;
	}

	public String getPageLoadDateTime() {
		return pageLoadDateTime;
	}

	public void setPageLoadDateTime(String pageLoadDateTime) {
		this.pageLoadDateTime = pageLoadDateTime;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	@Override
	public String toString() {
		return "ProductsListUpdate [providerName=" + providerName + ", startDate=" + startDate + ", productCategory="
				+ productCategory + ", productSubcategory=" + productSubcategory + ", productSituation="
				+ productSituation + ", nameFirstInsurer=" + nameFirstInsurer + ", productId=" + productId
				+ ", productClosed=" + productClosed + ", nameFirstDebtor=" + nameFirstDebtor + ", creditType="
				+ creditType + ", contactID=" + contactID + ", nameFirstInsured=" + nameFirstInsured
				+ ", pageLoadDateTime=" + pageLoadDateTime + ", policyNumber=" + policyNumber + ", productName="
				+ productName + ", serviceProvider=" + serviceProvider + "]";
	}

}
