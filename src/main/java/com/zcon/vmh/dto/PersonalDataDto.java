package com.zcon.vmh.dto;

public class PersonalDataDto {

	private String nawId;
	private String serverName;
	private String userName;
	private String password;
	private String databaseName;

	private String clientId;
	private String divisionId;
	private String clientRoleId;
	private String userID;
	private String userLoginDetailId;
	private String contactID;
	
	private String partner_naw_Id;
	
		
	private PersonalDataInfoDto personalData;

	public String getNawId() {
		return nawId;
	}

	public void setNawId(String nawId) {
		this.nawId = nawId;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}

	public String getClientRoleId() {
		return clientRoleId;
	}

	public void setClientRoleId(String clientRoleId) {
		this.clientRoleId = clientRoleId;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserLoginDetailId() {
		return userLoginDetailId;
	}

	public void setUserLoginDetailId(String userLoginDetailId) {
		this.userLoginDetailId = userLoginDetailId;
	}

	public PersonalDataInfoDto getPersonalData() {
		return personalData;
	}

	public void setPersonalData(PersonalDataInfoDto personalData) {
		this.personalData = personalData;
	}

	public String getContactID() {
		return contactID;
	}

	public void setContactID(String contactID) {
		this.contactID = contactID;
	}

	@Override
	public String toString() {
		return "PersonalDataDto [nawId=" + nawId + ", serverName=" + serverName + ", userName=" + userName
				+ ", password=" + password + ", databaseName=" + databaseName + ", clientId=" + clientId
				+ ", divisionId=" + divisionId + ", clientRoleId=" + clientRoleId + ", userID=" + userID
				+ ", userLoginDetailId=" + userLoginDetailId + ", contactID=" + contactID + ", personalData="
				+ personalData + "]";
	}

	public String getPartner_naw_Id() {
		return partner_naw_Id;
	}

	public void setPartner_naw_Id(String partner_naw_Id) {
		this.partner_naw_Id = partner_naw_Id;
	}



	
	

	

	
	
}