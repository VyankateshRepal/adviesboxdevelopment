package com.zcon.vmh.dto;

import java.util.Arrays;

public class ProductsDataDto {

	private String serverName;
	private String userName;
	private String password;
	private String databaseName;
	private String advId;
	private String lenId;
	private String nawId;

	private String clientId;
	private String divisionId;
	private String clientRoleId;
	private String userID;
	private String userLoginDetailId;

	private ProductsDataInfo productsDataInfo;

	//private ProductsDataOutputList[] productsDataOutputList;

	private ProductsList productsList;
	
	//private ProductsListUpdate productsListUpdate;
	
	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getAdvId() {
		return advId;
	}

	public void setAdvId(String advId) {
		this.advId = advId;
	}

	public String getLenId() {
		return lenId;
	}

	public void setLenId(String lenId) {
		this.lenId = lenId;
	}

	public String getNawId() {
		return nawId;
	}

	public void setNawId(String nawId) {
		this.nawId = nawId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}

	public String getClientRoleId() {
		return clientRoleId;
	}

	public void setClientRoleId(String clientRoleId) {
		this.clientRoleId = clientRoleId;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserLoginDetailId() {
		return userLoginDetailId;
	}

	public void setUserLoginDetailId(String userLoginDetailId) {
		this.userLoginDetailId = userLoginDetailId;
	}

	public ProductsDataInfo getProductsDataInfo() {
		return productsDataInfo;
	}

	public void setProductsDataInfo(ProductsDataInfo productsDataInfo) {
		this.productsDataInfo = productsDataInfo;
	}

	/*public ProductsDataOutputList[] getProductsDataOutputList() {
		return productsDataOutputList;
	}

	public void setProductsDataOutputList(ProductsDataOutputList[] productsDataOutputList) {
		this.productsDataOutputList = productsDataOutputList;
	}*/

	@Override
	public String toString() {
		return "ProductsDataDto [serverName=" + serverName + ", userName=" + userName + ", password=" + password
				+ ", databaseName=" + databaseName + ", advId=" + advId + ", lenId=" + lenId + ", nawId=" + nawId
				+ ", clientId=" + clientId + ", divisionId=" + divisionId + ", clientRoleId=" + clientRoleId
				+ ", userID=" + userID + ", userLoginDetailId=" + userLoginDetailId + ", productsDataInfo="
				+ productsDataInfo + ", productsDataOutputList="  + "]";
	}

	public ProductsList getProductsList() {
		return productsList;
	}

	public void setProductsList(ProductsList productsList) {
		this.productsList = productsList;
	}

	/*public ProductsListUpdate getProductsListUpdate() {
		return productsListUpdate;
	}

	public void setProductsListUpdate(ProductsListUpdate productsListUpdate) {
		this.productsListUpdate = productsListUpdate;
	}*/
}
