package com.zcon.vmh.dto;

public class PensionDataDto {

	private String nawId;
	private String serverName;
	private String userName;
	private String password;
	private String databaseName;

	public String getNawId() {
		return nawId;
	}

	public void setNawId(String nawId) {
		this.nawId = nawId;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	@Override
	public String toString() {
		return "PensionDataDto [nawId=" + nawId + ", serverName=" + serverName + ", userName=" + userName
				+ ", password=" + password + ", databaseName=" + databaseName + "]";
	}

}
