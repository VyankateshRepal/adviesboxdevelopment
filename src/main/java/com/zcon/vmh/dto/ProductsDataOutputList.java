package com.zcon.vmh.dto;

public class ProductsDataOutputList {

	private String startDate;

	private String productCategory;

	private String productSubcategory;

	private String nameFirstInsured;

	private String policyNumber;

	private String productName;

	private String serviceProvider;

	private String productClosed;

	private String productId;

	private String nameFirstInsurer;

	private String creditType;

	private String firstDebtors;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductSubcategory() {
		return productSubcategory;
	}

	public void setProductSubcategory(String productSubcategory) {
		this.productSubcategory = productSubcategory;
	}

	public String getNameFirstInsured() {
		return nameFirstInsured;
	}

	public void setNameFirstInsured(String nameFirstInsured) {
		this.nameFirstInsured = nameFirstInsured;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getProductClosed() {
		return productClosed;
	}

	public void setProductClosed(String productClosed) {
		this.productClosed = productClosed;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getNameFirstInsurer() {
		return nameFirstInsurer;
	}

	public void setNameFirstInsurer(String nameFirstInsurer) {
		this.nameFirstInsurer = nameFirstInsurer;
	}

	public String getCreditType() {
		return creditType;
	}

	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}

	public String getFirstDebtors() {
		return firstDebtors;
	}

	public void setFirstDebtors(String firstDebtors) {
		this.firstDebtors = firstDebtors;
	}

	@Override
	public String toString() {
		return "ProductsDataOutputList [startDate=" + startDate + ", productCategory=" + productCategory
				+ ", productSubcategory=" + productSubcategory + ", nameFirstInsured=" + nameFirstInsured
				+ ", policyNumber=" + policyNumber + ", productName=" + productName + ", serviceProvider="
				+ serviceProvider + ", productClosed=" + productClosed + ", productId=" + productId
				+ ", nameFirstInsurer=" + nameFirstInsurer + ", creditType=" + creditType + ", firstDebtors="
				+ firstDebtors + "]";
	}

}
