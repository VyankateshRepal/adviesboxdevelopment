package com.zcon.vmh.dto;

public class HypoteekDataDto {
	
	private String serverName;
	private String userName;
	private String password;
	private String databaseName;
	private String advId;
	private String lenId;
	///private String nawId;
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getAdvId() {
		return advId;
	}
	public void setAdvId(String advId) {
		this.advId = advId;
	}
	public String getLenId() {
		return lenId;
	}
	public void setLenId(String lenId) {
		this.lenId = lenId;
	}
	@Override
	public String toString() {
		return "HypoteekDataDto [serverName=" + serverName + ", userName=" + userName + ", password=" + password
				+ ", databaseName=" + databaseName + ", advId=" + advId + ", lenId=" + lenId + "]";
	}

}
