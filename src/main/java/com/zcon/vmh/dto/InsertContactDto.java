package com.zcon.vmh.dto;

import java.sql.Date;

public class InsertContactDto {
	
	private String serverName;
	private String userName;
	private String password;
	private String databaseName;
	private String firstName;
	private String middleName;
	private String lastName;
	private Date dateOfBirth;
	private String placeOfBirth;
	private String homePhone;
	private String officePhone;
	private String mobilePhone;
	private String primaryEmailAddress;
	private String primaryAddressStreet;
	private String primaryAddressHouseNumber;
	private String primaryAddressCity;
	private String primaryAddressPostalCode;
	private String primaryAddressCountry;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getPrimaryEmailAddress() {
		return primaryEmailAddress;
	}
	public void setPrimaryEmailAddress(String primaryEmailAddress) {
		this.primaryEmailAddress = primaryEmailAddress;
	}
	public String getPrimaryAddressStreet() {
		return primaryAddressStreet;
	}
	public void setPrimaryAddressStreet(String primaryAddressStreet) {
		this.primaryAddressStreet = primaryAddressStreet;
	}
	public String getPrimaryAddressHouseNumber() {
		return primaryAddressHouseNumber;
	}
	public void setPrimaryAddressHouseNumber(String primaryAddressHouseNumber) {
		this.primaryAddressHouseNumber = primaryAddressHouseNumber;
	}
	public String getPrimaryAddressCity() {
		return primaryAddressCity;
	}
	public void setPrimaryAddressCity(String primaryAddressCity) {
		this.primaryAddressCity = primaryAddressCity;
	}
	public String getPrimaryAddressPostalCode() {
		return primaryAddressPostalCode;
	}
	public void setPrimaryAddressPostalCode(String primaryAddressPostalCode) {
		this.primaryAddressPostalCode = primaryAddressPostalCode;
	}
	public String getPrimaryAddressCountry() {
		return primaryAddressCountry;
	}
	public void setPrimaryAddressCountry(String primaryAddressCountry) {
		this.primaryAddressCountry = primaryAddressCountry;
	}
	
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	@Override
	public String toString() {
		return "InsertContactDto [serverName=" + serverName + ", userName=" + userName + ", password=" + password
				+ ", databaseName=" + databaseName + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", dateOfBirth=" + dateOfBirth + ", placeOfBirth=" + placeOfBirth
				+ ", homePhone=" + homePhone + ", officePhone=" + officePhone + ", mobilePhone=" + mobilePhone
				+ ", primaryEmailAddress=" + primaryEmailAddress + ", primaryAddressStreet=" + primaryAddressStreet
				+ ", primaryAddressHouseNumber=" + primaryAddressHouseNumber + ", primaryAddressCity="
				+ primaryAddressCity + ", primaryAddressPostalCode=" + primaryAddressPostalCode
				+ ", primaryAddressCountry=" + primaryAddressCountry + "]";
	}
}
