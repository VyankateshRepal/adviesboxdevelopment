package com.zcon.vmh.dto;

import java.util.List;

public class ProductsDataToSaveDTO {

	private List<ProductsList> productsList;

	private String userID;
	
	private String contactID;

	private String databaseName;

	private String divisionId;

	private String clientRoleId;

	private String userName;

	private String serverName;

	private String password;

	private String clientId;

	private String userLoginDetailId;

	private String nameFirstInsurer;
	
	public List<ProductsList> getProductsList() {
		return productsList;
	}

	public void setProductsList(List<ProductsList> productsList) {
		this.productsList = productsList;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}

	public String getClientRoleId() {
		return clientRoleId;
	}

	public void setClientRoleId(String clientRoleId) {
		this.clientRoleId = clientRoleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUserLoginDetailId() {
		return userLoginDetailId;
	}

	public void setUserLoginDetailId(String userLoginDetailId) {
		this.userLoginDetailId = userLoginDetailId;
	}

	public String getContactID() {
		return contactID;
	}

	public void setContactID(String contactID) {
		this.contactID = contactID;
	}

	public String getNameFirstInsurer() {
		return nameFirstInsurer;
	}

	public void setNameFirstInsurer(String nameFirstInsurer) {
		this.nameFirstInsurer = nameFirstInsurer;
	}

}
