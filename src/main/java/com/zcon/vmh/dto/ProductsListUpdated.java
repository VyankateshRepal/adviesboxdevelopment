package com.zcon.vmh.dto;

import java.sql.Date;

public class ProductsListUpdated {

	private String capitalBuilt;
	private String productCloseValue;
	private String avn2;
	private String avn1;
	private String policyNumber;
	private String kln_Id;
	private String productName;
	private String dek_An_LooptijdMnd;
	private String kapbox;
	private String dek_Guid_Origineel;
	private String Dt_policyValue;
	private String anv6;
	private String lfr_Pc_Rentement;
	private String anv5;
	private String anv4;
	private String productSituation;
	private String anv3;
	private String kap_Bd_Garantie;
	private String providerName;
	private String deposit;
	private String kap_Pc_Doelkapitaal;
	private String pre_An_Mnd_Stijging;
	private String productId;
	private String extended;
	private String kap1;
	private String kap_An_Termijn;
	private String lfr_CD_BOXKEUZE;
	private String kap_Pc_RekenRente;
	private String kap4;
	private String terms;
	private String kap5;
	private String kap2;
	private String kap3;
	private String term;
	private String kap6;
	private String bd_Op;
	private String dek_Om_SoortDepot;
	private String dek_In_VerzorgersClausule;
	private String annuityClause;
	private String kap_Bd_Voorbeeld;
	private String startDate;
	private String cd_Scenario;
	private String dek_An_Mnd_Hooglaag;
	private String anv_Id;
	private String dek_Om_DekkingNaam;
	private String adv_Id;
	private String lfr_CD_SOORT;
	private String lfr_BD_KOOPSOM;
	private String productCategory;
	private String policyValue;
	private String dt_captitalBuilt;
	private String pre_Pc_Stijging;
	private String pre_An_LooptijdMnd;
	private String productSubCategory;
	private String deposit4;
	private String deposit2;
	private String deposit3;
	private String cd_UITGANGSPUNT;
	private String dek_Om_Bestedingsdoel;
	private String bd_Op_;
	private String kap_Mnd;
	private String nameFirstInsured;
	private String frequency;
	private String lfr_Pc_OvergangPartner;
	private String partnerId;
	private String kap_An_Mnd;
	private String naw_Id;
	private String pre_Cd_Frequency;
	private String vrz_Cd_AoPremieVrijstelling;
	private String vrz_CD_AOPVVERVOLG;
	private String kap_LT_EIND;
	private String vrz_An_AoWachttijdWkn;
	private String vrz_Cd_Beroepsklasse;
	private String kap_PC_INDEXATIE;
	// ORV added
	// for hypoteek
	private String lnd_Bd_BoeteHoofdsom;
	private String lND_NR_RANGORDE;
	private String lnd_Om_ProduktNaam;
	private String lnd_Om_Rente;
	private String auto_rentedaling;
	private String lnd_An_Maanden;
	private String lND_CD_GARANTIE;
	private String lND_IN_UWBEMIDDELING;
	private String lnd_Bd_Aflossing_Maand;
	private String lnd_Bd_Aftrekbaar;
	private String lND_NR_AANVRAGER;
	private String lnd_Dt_Rentevast;
	private String lnd_Om_LeningNummer;
	private String sce_Value;
	private String zkn_id;
	private String lnd_Dt_Aanvang;
	private String lnd_An_RentevastMnd;
	private String lnd_Om_Mij;
	private String lnd_Dt_Opgave;
	private String lnd_Pc_RenteAftrek;

	// for kredit
	/// private String anv_Id;
	private String kre_Bd_Restandhoofdsom;
	private String kre_Nr_Leningnummer;
	private String kRE_IN_UWBEMIDDELING;
	private String kre_An_Mnd_Looptijd;
	private String kre_Id;
	private String kre_Om_Geldverstrekker;
	private String kRE_OM_PRODUCTNAAM;
	private String kre_Pc_Box1;
	private String kRE_PC_AFLOSSING;
	private String kre_Bd_Hoofdsom;
	private String kre_Pc_Box3;
	private String kre_Dt_Ingang;
	private String kRE_OM_BESTEDINGSDOEL;
	private String kre_Pc_Rente;
	private String kre_Bd_Box3;
	private String kre_Bd_Maandlast;
	private String kre_Bd_Box1;
	private String durationInMonths;

	private String policyEndDateDurationInMonths;

	private String endDatePremium;

	/// private String dek_Om_SoortDepot;

	private String isActive;
	private String opportunityId;
	private String productServiceSubjectCategory;

	private String endDtPaymentExtra;
	private String highLowRatio;

	private String durationInYears;
	private String endDateCoverage;

	private String kap_Cd_UitkeringAO;

	private String endDatePolicyDuration;

	private String productCodeProposal;
	private String nameFirstInsurer;
	private String endDatePayment;

	private String dek_In_IsFiscaleVoortzetting;
	private String dek_cd_Situatie;

	private String kap_Cd_uitgangspunt;
	private String kap_cd_Indexatie;

	public String getCapitalBuilt() {
		return capitalBuilt;
	}

	public void setCapitalBuilt(String capitalBuilt) {
		this.capitalBuilt = capitalBuilt;
	}

	public String getProductCloseValue() {
		return productCloseValue;
	}

	public void setProductCloseValue(String productCloseValue) {
		this.productCloseValue = productCloseValue;
	}

	public String getAvn2() {
		return avn2;
	}

	public void setAvn2(String avn2) {
		this.avn2 = avn2;
	}

	public String getAvn1() {
		return avn1;
	}

	public void setAvn1(String avn1) {
		this.avn1 = avn1;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getKln_Id() {
		return kln_Id;
	}

	public void setKln_Id(String kln_Id) {
		this.kln_Id = kln_Id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDek_An_LooptijdMnd() {
		return dek_An_LooptijdMnd;
	}

	public void setDek_An_LooptijdMnd(String dek_An_LooptijdMnd) {
		this.dek_An_LooptijdMnd = dek_An_LooptijdMnd;
	}

	public String getKapbox() {
		return kapbox;
	}

	public void setKapbox(String kapbox) {
		this.kapbox = kapbox;
	}

	public String getDek_Guid_Origineel() {
		return dek_Guid_Origineel;
	}

	public void setDek_Guid_Origineel(String dek_Guid_Origineel) {
		this.dek_Guid_Origineel = dek_Guid_Origineel;
	}

	public String getDt_policyValue() {
		return Dt_policyValue;
	}

	public void setDt_policyValue(String dt_policyValue) {
		Dt_policyValue = dt_policyValue;
	}

	public String getAnv6() {
		return anv6;
	}

	public void setAnv6(String anv6) {
		this.anv6 = anv6;
	}

	public String getLfr_Pc_Rentement() {
		return lfr_Pc_Rentement;
	}

	public void setLfr_Pc_Rentement(String lfr_Pc_Rentement) {
		this.lfr_Pc_Rentement = lfr_Pc_Rentement;
	}

	public String getAnv5() {
		return anv5;
	}

	public void setAnv5(String anv5) {
		this.anv5 = anv5;
	}

	public String getAnv4() {
		return anv4;
	}

	public void setAnv4(String anv4) {
		this.anv4 = anv4;
	}

	public String getProductSituation() {
		return productSituation;
	}

	public void setProductSituation(String productSituation) {
		this.productSituation = productSituation;
	}

	public String getAnv3() {
		return anv3;
	}

	public void setAnv3(String anv3) {
		this.anv3 = anv3;
	}

	public String getKap_Bd_Garantie() {
		return kap_Bd_Garantie;
	}

	public void setKap_Bd_Garantie(String kap_Bd_Garantie) {
		this.kap_Bd_Garantie = kap_Bd_Garantie;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getDeposit() {
		return deposit;
	}

	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	public String getKap_Pc_Doelkapitaal() {
		return kap_Pc_Doelkapitaal;
	}

	public void setKap_Pc_Doelkapitaal(String kap_Pc_Doelkapitaal) {
		this.kap_Pc_Doelkapitaal = kap_Pc_Doelkapitaal;
	}

	public String getPre_An_Mnd_Stijging() {
		return pre_An_Mnd_Stijging;
	}

	public void setPre_An_Mnd_Stijging(String pre_An_Mnd_Stijging) {
		this.pre_An_Mnd_Stijging = pre_An_Mnd_Stijging;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getExtended() {
		return extended;
	}

	public void setExtended(String extended) {
		this.extended = extended;
	}

	public String getKap1() {
		return kap1;
	}

	public void setKap1(String kap1) {
		this.kap1 = kap1;
	}

	public String getKap_An_Termijn() {
		return kap_An_Termijn;
	}

	public void setKap_An_Termijn(String kap_An_Termijn) {
		this.kap_An_Termijn = kap_An_Termijn;
	}

	public String getLfr_CD_BOXKEUZE() {
		return lfr_CD_BOXKEUZE;
	}

	public void setLfr_CD_BOXKEUZE(String lfr_CD_BOXKEUZE) {
		this.lfr_CD_BOXKEUZE = lfr_CD_BOXKEUZE;
	}

	public String getKap_Pc_RekenRente() {
		return kap_Pc_RekenRente;
	}

	public void setKap_Pc_RekenRente(String kap_Pc_RekenRente) {
		this.kap_Pc_RekenRente = kap_Pc_RekenRente;
	}

	public String getKap4() {
		return kap4;
	}

	public void setKap4(String kap4) {
		this.kap4 = kap4;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getKap5() {
		return kap5;
	}

	public void setKap5(String kap5) {
		this.kap5 = kap5;
	}

	public String getKap2() {
		return kap2;
	}

	public void setKap2(String kap2) {
		this.kap2 = kap2;
	}

	public String getKap3() {
		return kap3;
	}

	public void setKap3(String kap3) {
		this.kap3 = kap3;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getKap6() {
		return kap6;
	}

	public void setKap6(String kap6) {
		this.kap6 = kap6;
	}

	public String getBd_Op() {
		return bd_Op;
	}

	public void setBd_Op(String bd_Op) {
		this.bd_Op = bd_Op;
	}

	public String getDek_Om_SoortDepot() {
		return dek_Om_SoortDepot;
	}

	public void setDek_Om_SoortDepot(String dek_Om_SoortDepot) {
		this.dek_Om_SoortDepot = dek_Om_SoortDepot;
	}

	public String getDek_In_VerzorgersClausule() {
		return dek_In_VerzorgersClausule;
	}

	public void setDek_In_VerzorgersClausule(String dek_In_VerzorgersClausule) {
		this.dek_In_VerzorgersClausule = dek_In_VerzorgersClausule;
	}

	public String getAnnuityClause() {
		return annuityClause;
	}

	public void setAnnuityClause(String annuityClause) {
		this.annuityClause = annuityClause;
	}

	public String getKap_Bd_Voorbeeld() {
		return kap_Bd_Voorbeeld;
	}

	public void setKap_Bd_Voorbeeld(String kap_Bd_Voorbeeld) {
		this.kap_Bd_Voorbeeld = kap_Bd_Voorbeeld;
	}

	public String getCd_Scenario() {
		return cd_Scenario;
	}

	public void setCd_Scenario(String cd_Scenario) {
		this.cd_Scenario = cd_Scenario;
	}

	public String getDek_An_Mnd_Hooglaag() {
		return dek_An_Mnd_Hooglaag;
	}

	public void setDek_An_Mnd_Hooglaag(String dek_An_Mnd_Hooglaag) {
		this.dek_An_Mnd_Hooglaag = dek_An_Mnd_Hooglaag;
	}

	public String getAnv_Id() {
		return anv_Id;
	}

	public void setAnv_Id(String anv_Id) {
		this.anv_Id = anv_Id;
	}

	public String getDek_Om_DekkingNaam() {
		return dek_Om_DekkingNaam;
	}

	public void setDek_Om_DekkingNaam(String dek_Om_DekkingNaam) {
		this.dek_Om_DekkingNaam = dek_Om_DekkingNaam;
	}

	public String getAdv_Id() {
		return adv_Id;
	}

	public void setAdv_Id(String adv_Id) {
		this.adv_Id = adv_Id;
	}

	public String getLfr_CD_SOORT() {
		return lfr_CD_SOORT;
	}

	public void setLfr_CD_SOORT(String lfr_CD_SOORT) {
		this.lfr_CD_SOORT = lfr_CD_SOORT;
	}

	public String getLfr_BD_KOOPSOM() {
		return lfr_BD_KOOPSOM;
	}

	public void setLfr_BD_KOOPSOM(String lfr_BD_KOOPSOM) {
		this.lfr_BD_KOOPSOM = lfr_BD_KOOPSOM;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getPolicyValue() {
		return policyValue;
	}

	public void setPolicyValue(String policyValue) {
		this.policyValue = policyValue;
	}

	public String getDt_captitalBuilt() {
		return dt_captitalBuilt;
	}

	public void setDt_captitalBuilt(String dt_captitalBuilt) {
		this.dt_captitalBuilt = dt_captitalBuilt;
	}

	public String getPre_Pc_Stijging() {
		return pre_Pc_Stijging;
	}

	public void setPre_Pc_Stijging(String pre_Pc_Stijging) {
		this.pre_Pc_Stijging = pre_Pc_Stijging;
	}

	public String getPre_An_LooptijdMnd() {
		return pre_An_LooptijdMnd;
	}

	public void setPre_An_LooptijdMnd(String pre_An_LooptijdMnd) {
		this.pre_An_LooptijdMnd = pre_An_LooptijdMnd;
	}

	public String getProductSubCategory() {
		return productSubCategory;
	}

	public void setProductSubCategory(String productSubCategory) {
		this.productSubCategory = productSubCategory;
	}

	public String getDeposit4() {
		return deposit4;
	}

	public void setDeposit4(String deposit4) {
		this.deposit4 = deposit4;
	}

	public String getDeposit2() {
		return deposit2;
	}

	public void setDeposit2(String deposit2) {
		this.deposit2 = deposit2;
	}

	public String getDeposit3() {
		return deposit3;
	}

	public void setDeposit3(String deposit3) {
		this.deposit3 = deposit3;
	}

	public String getCd_UITGANGSPUNT() {
		return cd_UITGANGSPUNT;
	}

	public void setCd_UITGANGSPUNT(String cd_UITGANGSPUNT) {
		this.cd_UITGANGSPUNT = cd_UITGANGSPUNT;
	}

	public String getDek_Om_Bestedingsdoel() {
		return dek_Om_Bestedingsdoel;
	}

	public void setDek_Om_Bestedingsdoel(String dek_Om_Bestedingsdoel) {
		this.dek_Om_Bestedingsdoel = dek_Om_Bestedingsdoel;
	}

	public String getBd_Op_() {
		return bd_Op_;
	}

	public void setBd_Op_(String bd_Op_) {
		this.bd_Op_ = bd_Op_;
	}

	public String getKap_Mnd() {
		return kap_Mnd;
	}

	public void setKap_Mnd(String kap_Mnd) {
		this.kap_Mnd = kap_Mnd;
	}

	public String getNameFirstInsured() {
		return nameFirstInsured;
	}

	public void setNameFirstInsured(String nameFirstInsured) {
		this.nameFirstInsured = nameFirstInsured;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getLfr_Pc_OvergangPartner() {
		return lfr_Pc_OvergangPartner;
	}

	public void setLfr_Pc_OvergangPartner(String lfr_Pc_OvergangPartner) {
		this.lfr_Pc_OvergangPartner = lfr_Pc_OvergangPartner;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getKap_An_Mnd() {
		return kap_An_Mnd;
	}

	public void setKap_An_Mnd(String kap_An_Mnd) {
		this.kap_An_Mnd = kap_An_Mnd;
	}

	public String getNaw_Id() {
		return naw_Id;
	}

	public void setNaw_Id(String naw_Id) {
		this.naw_Id = naw_Id;
	}

	public String getPre_Cd_Frequency() {
		return pre_Cd_Frequency;
	}

	public void setPre_Cd_Frequency(String pre_Cd_Frequency) {
		this.pre_Cd_Frequency = pre_Cd_Frequency;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getLnd_Bd_BoeteHoofdsom() {
		return lnd_Bd_BoeteHoofdsom;
	}

	public void setLnd_Bd_BoeteHoofdsom(String lnd_Bd_BoeteHoofdsom) {
		this.lnd_Bd_BoeteHoofdsom = lnd_Bd_BoeteHoofdsom;
	}

	public String getlND_NR_RANGORDE() {
		return lND_NR_RANGORDE;
	}

	public void setlND_NR_RANGORDE(String lND_NR_RANGORDE) {
		this.lND_NR_RANGORDE = lND_NR_RANGORDE;
	}

	public String getLnd_Om_ProduktNaam() {
		return lnd_Om_ProduktNaam;
	}

	public void setLnd_Om_ProduktNaam(String lnd_Om_ProduktNaam) {
		this.lnd_Om_ProduktNaam = lnd_Om_ProduktNaam;
	}

	public String getLnd_Om_Rente() {
		return lnd_Om_Rente;
	}

	public void setLnd_Om_Rente(String lnd_Om_Rente) {
		this.lnd_Om_Rente = lnd_Om_Rente;
	}

	public String getAuto_rentedaling() {
		return auto_rentedaling;
	}

	public void setAuto_rentedaling(String auto_rentedaling) {
		this.auto_rentedaling = auto_rentedaling;
	}

	public String getLnd_An_Maanden() {
		return lnd_An_Maanden;
	}

	public void setLnd_An_Maanden(String lnd_An_Maanden) {
		this.lnd_An_Maanden = lnd_An_Maanden;
	}

	public String getlND_CD_GARANTIE() {
		return lND_CD_GARANTIE;
	}

	public void setlND_CD_GARANTIE(String lND_CD_GARANTIE) {
		this.lND_CD_GARANTIE = lND_CD_GARANTIE;
	}

	public String getlND_IN_UWBEMIDDELING() {
		return lND_IN_UWBEMIDDELING;
	}

	public void setlND_IN_UWBEMIDDELING(String lND_IN_UWBEMIDDELING) {
		this.lND_IN_UWBEMIDDELING = lND_IN_UWBEMIDDELING;
	}

	public String getLnd_Bd_Aflossing_Maand() {
		return lnd_Bd_Aflossing_Maand;
	}

	public void setLnd_Bd_Aflossing_Maand(String lnd_Bd_Aflossing_Maand) {
		this.lnd_Bd_Aflossing_Maand = lnd_Bd_Aflossing_Maand;
	}

	public String getLnd_Bd_Aftrekbaar() {
		return lnd_Bd_Aftrekbaar;
	}

	public void setLnd_Bd_Aftrekbaar(String lnd_Bd_Aftrekbaar) {
		this.lnd_Bd_Aftrekbaar = lnd_Bd_Aftrekbaar;
	}

	public String getlND_NR_AANVRAGER() {
		return lND_NR_AANVRAGER;
	}

	public void setlND_NR_AANVRAGER(String lND_NR_AANVRAGER) {
		this.lND_NR_AANVRAGER = lND_NR_AANVRAGER;
	}

	public String getLnd_Dt_Rentevast() {
		return lnd_Dt_Rentevast;
	}

	public void setLnd_Dt_Rentevast(String lnd_Dt_Rentevast) {
		this.lnd_Dt_Rentevast = lnd_Dt_Rentevast;
	}

	public String getLnd_Om_LeningNummer() {
		return lnd_Om_LeningNummer;
	}

	public void setLnd_Om_LeningNummer(String lnd_Om_LeningNummer) {
		this.lnd_Om_LeningNummer = lnd_Om_LeningNummer;
	}

	public String getSce_Value() {
		return sce_Value;
	}

	public void setSce_Value(String sce_Value) {
		this.sce_Value = sce_Value;
	}

	public String getZkn_id() {
		return zkn_id;
	}

	public void setZkn_id(String zkn_id) {
		this.zkn_id = zkn_id;
	}

	public String getLnd_Dt_Aanvang() {
		return lnd_Dt_Aanvang;
	}

	public void setLnd_Dt_Aanvang(String lnd_Dt_Aanvang) {
		this.lnd_Dt_Aanvang = lnd_Dt_Aanvang;
	}

	public String getLnd_An_RentevastMnd() {
		return lnd_An_RentevastMnd;
	}

	public void setLnd_An_RentevastMnd(String lnd_An_RentevastMnd) {
		this.lnd_An_RentevastMnd = lnd_An_RentevastMnd;
	}

	public String getLnd_Om_Mij() {
		return lnd_Om_Mij;
	}

	public void setLnd_Om_Mij(String lnd_Om_Mij) {
		this.lnd_Om_Mij = lnd_Om_Mij;
	}

	public String getLnd_Dt_Opgave() {
		return lnd_Dt_Opgave;
	}

	public void setLnd_Dt_Opgave(String lnd_Dt_Opgave) {
		this.lnd_Dt_Opgave = lnd_Dt_Opgave;
	}

	public String getLnd_Pc_RenteAftrek() {
		return lnd_Pc_RenteAftrek;
	}

	public void setLnd_Pc_RenteAftrek(String lnd_Pc_RenteAftrek) {
		this.lnd_Pc_RenteAftrek = lnd_Pc_RenteAftrek;
	}

	public String getKre_Bd_Restandhoofdsom() {
		return kre_Bd_Restandhoofdsom;
	}

	public void setKre_Bd_Restandhoofdsom(String kre_Bd_Restandhoofdsom) {
		this.kre_Bd_Restandhoofdsom = kre_Bd_Restandhoofdsom;
	}

	public String getKre_Nr_Leningnummer() {
		return kre_Nr_Leningnummer;
	}

	public void setKre_Nr_Leningnummer(String kre_Nr_Leningnummer) {
		this.kre_Nr_Leningnummer = kre_Nr_Leningnummer;
	}

	public String getkRE_IN_UWBEMIDDELING() {
		return kRE_IN_UWBEMIDDELING;
	}

	public void setkRE_IN_UWBEMIDDELING(String kRE_IN_UWBEMIDDELING) {
		this.kRE_IN_UWBEMIDDELING = kRE_IN_UWBEMIDDELING;
	}

	public String getKre_An_Mnd_Looptijd() {
		return kre_An_Mnd_Looptijd;
	}

	public void setKre_An_Mnd_Looptijd(String kre_An_Mnd_Looptijd) {
		this.kre_An_Mnd_Looptijd = kre_An_Mnd_Looptijd;
	}

	public String getKre_Id() {
		return kre_Id;
	}

	public void setKre_Id(String kre_Id) {
		this.kre_Id = kre_Id;
	}

	public String getKre_Om_Geldverstrekker() {
		return kre_Om_Geldverstrekker;
	}

	public void setKre_Om_Geldverstrekker(String kre_Om_Geldverstrekker) {
		this.kre_Om_Geldverstrekker = kre_Om_Geldverstrekker;
	}

	public String getkRE_OM_PRODUCTNAAM() {
		return kRE_OM_PRODUCTNAAM;
	}

	public void setkRE_OM_PRODUCTNAAM(String kRE_OM_PRODUCTNAAM) {
		this.kRE_OM_PRODUCTNAAM = kRE_OM_PRODUCTNAAM;
	}

	public String getKre_Pc_Box1() {
		return kre_Pc_Box1;
	}

	public void setKre_Pc_Box1(String kre_Pc_Box1) {
		this.kre_Pc_Box1 = kre_Pc_Box1;
	}

	public String getkRE_PC_AFLOSSING() {
		return kRE_PC_AFLOSSING;
	}

	public void setkRE_PC_AFLOSSING(String kRE_PC_AFLOSSING) {
		this.kRE_PC_AFLOSSING = kRE_PC_AFLOSSING;
	}

	public String getKre_Bd_Hoofdsom() {
		return kre_Bd_Hoofdsom;
	}

	public void setKre_Bd_Hoofdsom(String kre_Bd_Hoofdsom) {
		this.kre_Bd_Hoofdsom = kre_Bd_Hoofdsom;
	}

	public String getKre_Pc_Box3() {
		return kre_Pc_Box3;
	}

	public void setKre_Pc_Box3(String kre_Pc_Box3) {
		this.kre_Pc_Box3 = kre_Pc_Box3;
	}

	public String getKre_Dt_Ingang() {
		return kre_Dt_Ingang;
	}

	public void setKre_Dt_Ingang(String kre_Dt_Ingang) {
		this.kre_Dt_Ingang = kre_Dt_Ingang;
	}

	public String getkRE_OM_BESTEDINGSDOEL() {
		return kRE_OM_BESTEDINGSDOEL;
	}

	public void setkRE_OM_BESTEDINGSDOEL(String kRE_OM_BESTEDINGSDOEL) {
		this.kRE_OM_BESTEDINGSDOEL = kRE_OM_BESTEDINGSDOEL;
	}

	public String getKre_Pc_Rente() {
		return kre_Pc_Rente;
	}

	public void setKre_Pc_Rente(String kre_Pc_Rente) {
		this.kre_Pc_Rente = kre_Pc_Rente;
	}

	public String getKre_Bd_Box3() {
		return kre_Bd_Box3;
	}

	public void setKre_Bd_Box3(String kre_Bd_Box3) {
		this.kre_Bd_Box3 = kre_Bd_Box3;
	}

	public String getKre_Bd_Maandlast() {
		return kre_Bd_Maandlast;
	}

	public void setKre_Bd_Maandlast(String kre_Bd_Maandlast) {
		this.kre_Bd_Maandlast = kre_Bd_Maandlast;
	}

	public String getKre_Bd_Box1() {
		return kre_Bd_Box1;
	}

	public void setKre_Bd_Box1(String kre_Bd_Box1) {
		this.kre_Bd_Box1 = kre_Bd_Box1;
	}

	public String getPolicyEndDateDurationInMonths() {
		return policyEndDateDurationInMonths;
	}

	public void setPolicyEndDateDurationInMonths(String policyEndDateDurationInMonths) {
		this.policyEndDateDurationInMonths = policyEndDateDurationInMonths;
	}

	public String getEndDatePremium() {
		return endDatePremium;
	}

	public void setEndDatePremium(String endDatePremium) {
		this.endDatePremium = endDatePremium;
	}

	public String getDurationInMonths() {
		return durationInMonths;
	}

	public void setDurationInMonths(String durationInMonths) {
		this.durationInMonths = durationInMonths;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getOpportunityId() {
		return opportunityId;
	}

	public void setOpportunityId(String opportunityId) {
		this.opportunityId = opportunityId;
	}

	public String getProductServiceSubjectCategory() {
		return productServiceSubjectCategory;
	}

	public void setProductServiceSubjectCategory(String productServiceSubjectCategory) {
		this.productServiceSubjectCategory = productServiceSubjectCategory;
	}

	public String getEndDtPaymentExtra() {
		return endDtPaymentExtra;
	}

	public void setEndDtPaymentExtra(String endDtPaymentExtra) {
		this.endDtPaymentExtra = endDtPaymentExtra;
	}

	public String getHighLowRatio() {
		return highLowRatio;
	}

	public void setHighLowRatio(String highLowRatio) {
		this.highLowRatio = highLowRatio;
	}

	public String getDurationInYears() {
		return durationInYears;
	}

	public void setDurationInYears(String durationInYears) {
		this.durationInYears = durationInYears;
	}

	public String getEndDateCoverage() {
		return endDateCoverage;
	}

	public void setEndDateCoverage(String endDateCoverage) {
		this.endDateCoverage = endDateCoverage;
	}

	public String getKap_Cd_UitkeringAO() {
		return kap_Cd_UitkeringAO;
	}

	public void setKap_Cd_UitkeringAO(String kap_Cd_UitkeringAO) {
		this.kap_Cd_UitkeringAO = kap_Cd_UitkeringAO;
	}

	public String getEndDatePolicyDuration() {
		return endDatePolicyDuration;
	}

	public void setEndDatePolicyDuration(String endDatePolicyDuration) {
		this.endDatePolicyDuration = endDatePolicyDuration;
	}

	public String getProductCodeProposal() {
		return productCodeProposal;
	}

	public void setProductCodeProposal(String productCodeProposal) {
		this.productCodeProposal = productCodeProposal;
	}

	public String getNameFirstInsurer() {
		return nameFirstInsurer;
	}

	public void setNameFirstInsurer(String nameFirstInsurer) {
		this.nameFirstInsurer = nameFirstInsurer;
	}

	public String getEndDatePayment() {
		return endDatePayment;
	}

	public void setEndDatePayment(String endDatePayment) {
		this.endDatePayment = endDatePayment;
	}

	public String getDek_In_IsFiscaleVoortzetting() {
		return dek_In_IsFiscaleVoortzetting;
	}

	public void setDek_In_IsFiscaleVoortzetting(String dek_In_IsFiscaleVoortzetting) {
		this.dek_In_IsFiscaleVoortzetting = dek_In_IsFiscaleVoortzetting;
	}

	public String getDek_cd_Situatie() {
		return dek_cd_Situatie;
	}

	public void setDek_cd_Situatie(String dek_cd_Situatie) {
		this.dek_cd_Situatie = dek_cd_Situatie;
	}

	public String getVrz_Cd_AoPremieVrijstelling() {
		return vrz_Cd_AoPremieVrijstelling;
	}

	public void setVrz_Cd_AoPremieVrijstelling(String vrz_Cd_AoPremieVrijstelling) {
		this.vrz_Cd_AoPremieVrijstelling = vrz_Cd_AoPremieVrijstelling;
	}

	public String getVrz_An_AoWachttijdWkn() {
		return vrz_An_AoWachttijdWkn;
	}

	public void setVrz_An_AoWachttijdWkn(String vrz_An_AoWachttijdWkn) {
		this.vrz_An_AoWachttijdWkn = vrz_An_AoWachttijdWkn;
	}

	public String getVrz_Cd_Beroepsklasse() {
		return vrz_Cd_Beroepsklasse;
	}

	public void setVrz_Cd_Beroepsklasse(String vrz_Cd_Beroepsklasse) {
		this.vrz_Cd_Beroepsklasse = vrz_Cd_Beroepsklasse;
	}

	public String getVrz_CD_AOPVVERVOLG() {
		return vrz_CD_AOPVVERVOLG;
	}

	public void setVrz_CD_AOPVVERVOLG(String vrz_CD_AOPVVERVOLG) {
		this.vrz_CD_AOPVVERVOLG = vrz_CD_AOPVVERVOLG;
	}

	public String getKap_LT_EIND() {
		return kap_LT_EIND;
	}

	public void setKap_LT_EIND(String kap_LT_EIND) {
		this.kap_LT_EIND = kap_LT_EIND;
	}

	public String getKap_Cd_uitgangspunt() {
		return kap_Cd_uitgangspunt;
	}

	public void setKap_Cd_uitgangspunt(String kap_Cd_uitgangspunt) {
		this.kap_Cd_uitgangspunt = kap_Cd_uitgangspunt;
	}

	public String getKap_PC_INDEXATIE() {
		return kap_PC_INDEXATIE;
	}

	public void setKap_PC_INDEXATIE(String kap_PC_INDEXATIE) {
		this.kap_PC_INDEXATIE = kap_PC_INDEXATIE;
	}

	public String getKap_cd_Indexatie() {
		return kap_cd_Indexatie;
	}

	public void setKap_cd_Indexatie(String kap_cd_Indexatie) {
		this.kap_cd_Indexatie = kap_cd_Indexatie;
	}

	@Override
	public String toString() {
		return "ProductsListUpdated [capitalBuilt=" + capitalBuilt + ", productCloseValue=" + productCloseValue
				+ ", avn2=" + avn2 + ", avn1=" + avn1 + ", policyNumber=" + policyNumber + ", kln_Id=" + kln_Id
				+ ", productName=" + productName + ", dek_An_LooptijdMnd=" + dek_An_LooptijdMnd + ", kapbox=" + kapbox
				+ ", dek_Guid_Origineel=" + dek_Guid_Origineel + ", Dt_policyValue=" + Dt_policyValue + ", anv6=" + anv6
				+ ", lfr_Pc_Rentement=" + lfr_Pc_Rentement + ", anv5=" + anv5 + ", anv4=" + anv4 + ", productSituation="
				+ productSituation + ", anv3=" + anv3 + ", kap_Bd_Garantie=" + kap_Bd_Garantie + ", providerName="
				+ providerName + ", deposit=" + deposit + ", kap_Pc_Doelkapitaal=" + kap_Pc_Doelkapitaal
				+ ", pre_An_Mnd_Stijging=" + pre_An_Mnd_Stijging + ", productId=" + productId + ", extended=" + extended
				+ ", kap1=" + kap1 + ", kap_An_Termijn=" + kap_An_Termijn + ", lfr_CD_BOXKEUZE=" + lfr_CD_BOXKEUZE
				+ ", kap_Pc_RekenRente=" + kap_Pc_RekenRente + ", kap4=" + kap4 + ", terms=" + terms + ", kap5=" + kap5
				+ ", kap2=" + kap2 + ", kap3=" + kap3 + ", term=" + term + ", kap6=" + kap6 + ", bd_Op=" + bd_Op
				+ ", dek_Om_SoortDepot=" + dek_Om_SoortDepot + ", dek_In_VerzorgersClausule="
				+ dek_In_VerzorgersClausule + ", annuityClause=" + annuityClause + ", kap_Bd_Voorbeeld="
				+ kap_Bd_Voorbeeld + ", startDate=" + startDate + ", cd_Scenario=" + cd_Scenario
				+ ", dek_An_Mnd_Hooglaag=" + dek_An_Mnd_Hooglaag + ", anv_Id=" + anv_Id + ", dek_Om_DekkingNaam="
				+ dek_Om_DekkingNaam + ", adv_Id=" + adv_Id + ", lfr_CD_SOORT=" + lfr_CD_SOORT + ", lfr_BD_KOOPSOM="
				+ lfr_BD_KOOPSOM + ", productCategory=" + productCategory + ", policyValue=" + policyValue
				+ ", dt_captitalBuilt=" + dt_captitalBuilt + ", pre_Pc_Stijging=" + pre_Pc_Stijging
				+ ", pre_An_LooptijdMnd=" + pre_An_LooptijdMnd + ", productSubCategory=" + productSubCategory
				+ ", deposit4=" + deposit4 + ", deposit2=" + deposit2 + ", deposit3=" + deposit3 + ", cd_UITGANGSPUNT="
				+ cd_UITGANGSPUNT + ", dek_Om_Bestedingsdoel=" + dek_Om_Bestedingsdoel + ", bd_Op_=" + bd_Op_
				+ ", kap_Mnd=" + kap_Mnd + ", nameFirstInsured=" + nameFirstInsured + ", frequency=" + frequency
				+ ", lfr_Pc_OvergangPartner=" + lfr_Pc_OvergangPartner + ", partnerId=" + partnerId + ", kap_An_Mnd="
				+ kap_An_Mnd + ", naw_Id=" + naw_Id + ", pre_Cd_Frequency=" + pre_Cd_Frequency
				+ ", vrz_Cd_AoPremieVrijstelling=" + vrz_Cd_AoPremieVrijstelling + ", vrz_CD_AOPVVERVOLG="
				+ vrz_CD_AOPVVERVOLG + ", kap_LT_EIND=" + kap_LT_EIND + ", vrz_An_AoWachttijdWkn="
				+ vrz_An_AoWachttijdWkn + ", vrz_Cd_Beroepsklasse=" + vrz_Cd_Beroepsklasse + ", kap_PC_INDEXATIE="
				+ kap_PC_INDEXATIE + ", lnd_Bd_BoeteHoofdsom=" + lnd_Bd_BoeteHoofdsom + ", lND_NR_RANGORDE="
				+ lND_NR_RANGORDE + ", lnd_Om_ProduktNaam=" + lnd_Om_ProduktNaam + ", lnd_Om_Rente=" + lnd_Om_Rente
				+ ", auto_rentedaling=" + auto_rentedaling + ", lnd_An_Maanden=" + lnd_An_Maanden + ", lND_CD_GARANTIE="
				+ lND_CD_GARANTIE + ", lND_IN_UWBEMIDDELING=" + lND_IN_UWBEMIDDELING + ", lnd_Bd_Aflossing_Maand="
				+ lnd_Bd_Aflossing_Maand + ", lnd_Bd_Aftrekbaar=" + lnd_Bd_Aftrekbaar + ", lND_NR_AANVRAGER="
				+ lND_NR_AANVRAGER + ", lnd_Dt_Rentevast=" + lnd_Dt_Rentevast + ", lnd_Om_LeningNummer="
				+ lnd_Om_LeningNummer + ", sce_Value=" + sce_Value + ", zkn_id=" + zkn_id + ", lnd_Dt_Aanvang="
				+ lnd_Dt_Aanvang + ", lnd_An_RentevastMnd=" + lnd_An_RentevastMnd + ", lnd_Om_Mij=" + lnd_Om_Mij
				+ ", lnd_Dt_Opgave=" + lnd_Dt_Opgave + ", lnd_Pc_RenteAftrek=" + lnd_Pc_RenteAftrek
				+ ", kre_Bd_Restandhoofdsom=" + kre_Bd_Restandhoofdsom + ", kre_Nr_Leningnummer=" + kre_Nr_Leningnummer
				+ ", kRE_IN_UWBEMIDDELING=" + kRE_IN_UWBEMIDDELING + ", kre_An_Mnd_Looptijd=" + kre_An_Mnd_Looptijd
				+ ", kre_Id=" + kre_Id + ", kre_Om_Geldverstrekker=" + kre_Om_Geldverstrekker + ", kRE_OM_PRODUCTNAAM="
				+ kRE_OM_PRODUCTNAAM + ", kre_Pc_Box1=" + kre_Pc_Box1 + ", kRE_PC_AFLOSSING=" + kRE_PC_AFLOSSING
				+ ", kre_Bd_Hoofdsom=" + kre_Bd_Hoofdsom + ", kre_Pc_Box3=" + kre_Pc_Box3 + ", kre_Dt_Ingang="
				+ kre_Dt_Ingang + ", kRE_OM_BESTEDINGSDOEL=" + kRE_OM_BESTEDINGSDOEL + ", kre_Pc_Rente=" + kre_Pc_Rente
				+ ", kre_Bd_Box3=" + kre_Bd_Box3 + ", kre_Bd_Maandlast=" + kre_Bd_Maandlast + ", kre_Bd_Box1="
				+ kre_Bd_Box1 + ", durationInMonths=" + durationInMonths + ", policyEndDateDurationInMonths="
				+ policyEndDateDurationInMonths + ", endDatePremium=" + endDatePremium + ", isActive=" + isActive
				+ ", opportunityId=" + opportunityId + ", productServiceSubjectCategory="
				+ productServiceSubjectCategory + ", endDtPaymentExtra=" + endDtPaymentExtra + ", highLowRatio="
				+ highLowRatio + ", durationInYears=" + durationInYears + ", endDateCoverage=" + endDateCoverage
				+ ", kap_Cd_UitkeringAO=" + kap_Cd_UitkeringAO + ", endDatePolicyDuration=" + endDatePolicyDuration
				+ ", productCodeProposal=" + productCodeProposal + ", nameFirstInsurer=" + nameFirstInsurer
				+ ", endDatePayment=" + endDatePayment + ", dek_In_IsFiscaleVoortzetting="
				+ dek_In_IsFiscaleVoortzetting + ", dek_cd_Situatie=" + dek_cd_Situatie + ", kap_Cd_uitgangspunt="
				+ kap_Cd_uitgangspunt + ", kap_cd_Indexatie=" + kap_cd_Indexatie + "]";
	}

}
