package com.zcon.vmh.dto;

import java.sql.Date;

public class PersonalDataInfoDto {

	private String anv_Om_TelefoonMobiel;

	private int naw_Id;

	private String anv_Om_TelefoonPrive;

	private String anv_Om_LegitimatieAfgifte;

	// private int advisoryBoxContactRefID;

	private String naw_Om_Straat;

	private String naw_Om_Postcode;

	private String anv_Om_Email;

	private String naw_Nr_Huis;

	private String naw_Om_Voorletters;

	private String anv_Om_TelefoonWerk;

	private String naw_Om_Voornamen;

	private Date anv_Dt_Geboorte;

	private String naw_om_iban;

	private String anv_Om_PlaatsGeboorte;

	private String naw_Om_Naam;

	private String anv_Cd_Nationaliteit;

	private String Gender;
	private String MarritalStatus;
	private String City;
	private String LegitimationIssue;
	private String toBIC;
	private String SocialSecurityNumber;

	private String anv_In_Roken;

	private String naw_Om_Voorvoegsel;

	private String anv_Cd_BurgelijkeStaat;
	
	private String naw_Cd_Geslacht;

	public String getAnv_Om_TelefoonMobiel() {
		return anv_Om_TelefoonMobiel;
	}

	public void setAnv_Om_TelefoonMobiel(String anv_Om_TelefoonMobiel) {
		this.anv_Om_TelefoonMobiel = anv_Om_TelefoonMobiel;
	}

	public int getNaw_Id() {
		return naw_Id;
	}

	public void setNaw_Id(int naw_Id) {
		this.naw_Id = naw_Id;
	}

	public String getAnv_Om_TelefoonPrive() {
		return anv_Om_TelefoonPrive;
	}

	public void setAnv_Om_TelefoonPrive(String anv_Om_TelefoonPrive) {
		this.anv_Om_TelefoonPrive = anv_Om_TelefoonPrive;
	}

	public String getAnv_Om_LegitimatieAfgifte() {
		return anv_Om_LegitimatieAfgifte;
	}

	public void setAnv_Om_LegitimatieAfgifte(String anv_Om_LegitimatieAfgifte) {
		this.anv_Om_LegitimatieAfgifte = anv_Om_LegitimatieAfgifte;
	}

	/*
	 * public int getAdvisoryBoxContactRefID() { return advisoryBoxContactRefID;
	 * }
	 * 
	 * public void setAdvisoryBoxContactRefID(int advisoryBoxContactRefID) {
	 * this.advisoryBoxContactRefID = advisoryBoxContactRefID; }
	 */

	public String getNaw_Om_Straat() {
		return naw_Om_Straat;
	}

	public void setNaw_Om_Straat(String naw_Om_Straat) {
		this.naw_Om_Straat = naw_Om_Straat;
	}

	public String getNaw_Om_Postcode() {
		return naw_Om_Postcode;
	}

	public void setNaw_Om_Postcode(String naw_Om_Postcode) {
		this.naw_Om_Postcode = naw_Om_Postcode;
	}

	public String getAnv_Om_Email() {
		return anv_Om_Email;
	}

	public void setAnv_Om_Email(String anv_Om_Email) {
		this.anv_Om_Email = anv_Om_Email;
	}

	public String getNaw_Nr_Huis() {
		return naw_Nr_Huis;
	}

	public void setNaw_Nr_Huis(String naw_Nr_Huis) {
		this.naw_Nr_Huis = naw_Nr_Huis;
	}

	public String getNaw_Om_Voorletters() {
		return naw_Om_Voorletters;
	}

	public void setNaw_Om_Voorletters(String naw_Om_Voorletters) {
		this.naw_Om_Voorletters = naw_Om_Voorletters;
	}

	public String getAnv_Om_TelefoonWerk() {
		return anv_Om_TelefoonWerk;
	}

	public void setAnv_Om_TelefoonWerk(String anv_Om_TelefoonWerk) {
		this.anv_Om_TelefoonWerk = anv_Om_TelefoonWerk;
	}

	public String getNaw_Om_Voornamen() {
		return naw_Om_Voornamen;
	}

	public void setNaw_Om_Voornamen(String naw_Om_Voornamen) {
		this.naw_Om_Voornamen = naw_Om_Voornamen;
	}

	public Date getAnv_Dt_Geboorte() {
		return anv_Dt_Geboorte;
	}

	public void setAnv_Dt_Geboorte(Date anv_Dt_Geboorte) {
		this.anv_Dt_Geboorte = anv_Dt_Geboorte;
	}

	public String getNaw_om_iban() {
		return naw_om_iban;
	}

	public void setNaw_om_iban(String naw_om_iban) {
		this.naw_om_iban = naw_om_iban;
	}

	public String getAnv_Om_PlaatsGeboorte() {
		return anv_Om_PlaatsGeboorte;
	}

	public void setAnv_Om_PlaatsGeboorte(String anv_Om_PlaatsGeboorte) {
		this.anv_Om_PlaatsGeboorte = anv_Om_PlaatsGeboorte;
	}

	public String getNaw_Om_Naam() {
		return naw_Om_Naam;
	}

	public void setNaw_Om_Naam(String naw_Om_Naam) {
		this.naw_Om_Naam = naw_Om_Naam;
	}

	public String getAnv_Cd_Nationaliteit() {
		return anv_Cd_Nationaliteit;
	}

	public void setAnv_Cd_Nationaliteit(String anv_Cd_Nationaliteit) {
		this.anv_Cd_Nationaliteit = anv_Cd_Nationaliteit;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getMarritalStatus() {
		return MarritalStatus;
	}

	public void setMarritalStatus(String marritalStatus) {
		MarritalStatus = marritalStatus;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getLegitimationIssue() {
		return LegitimationIssue;
	}

	public void setLegitimationIssue(String legitimationIssue) {
		LegitimationIssue = legitimationIssue;
	}

	public String getToBIC() {
		return toBIC;
	}

	public void setToBIC(String toBIC) {
		this.toBIC = toBIC;
	}

	public String getSocialSecurityNumber() {
		return SocialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		SocialSecurityNumber = socialSecurityNumber;
	}

	public String getAnv_In_Roken() {
		return anv_In_Roken;
	}

	public void setAnv_In_Roken(String anv_In_Roken) {
		this.anv_In_Roken = anv_In_Roken;
	}

	public String getNaw_Om_Voorvoegsel() {
		return naw_Om_Voorvoegsel;
	}

	public void setNaw_Om_Voorvoegsel(String naw_Om_Voorvoegsel) {
		this.naw_Om_Voorvoegsel = naw_Om_Voorvoegsel;
	}

	public String getAnv_Cd_BurgelijkeStaat() {
		return anv_Cd_BurgelijkeStaat;
	}

	public void setAnv_Cd_BurgelijkeStaat(String anv_Cd_BurgelijkeStaat) {
		this.anv_Cd_BurgelijkeStaat = anv_Cd_BurgelijkeStaat;
	}

	public String getNaw_Cd_Geslacht() {
		return naw_Cd_Geslacht;
	}

	public void setNaw_Cd_Geslacht(String naw_Cd_Geslacht) {
		this.naw_Cd_Geslacht = naw_Cd_Geslacht;
	}

	@Override
	public String toString() {
		return "PersonalDataInfoDto [anv_Om_TelefoonMobiel=" + anv_Om_TelefoonMobiel + ", naw_Id=" + naw_Id
				+ ", anv_Om_TelefoonPrive=" + anv_Om_TelefoonPrive + ", anv_Om_LegitimatieAfgifte="
				+ anv_Om_LegitimatieAfgifte + ", naw_Om_Straat=" + naw_Om_Straat + ", naw_Om_Postcode="
				+ naw_Om_Postcode + ", anv_Om_Email=" + anv_Om_Email + ", naw_Nr_Huis=" + naw_Nr_Huis
				+ ", naw_Om_Voorletters=" + naw_Om_Voorletters + ", anv_Om_TelefoonWerk=" + anv_Om_TelefoonWerk
				+ ", naw_Om_Voornamen=" + naw_Om_Voornamen + ", anv_Dt_Geboorte=" + anv_Dt_Geboorte + ", naw_om_iban="
				+ naw_om_iban + ", anv_Om_PlaatsGeboorte=" + anv_Om_PlaatsGeboorte + ", naw_Om_Naam=" + naw_Om_Naam
				+ ", anv_Cd_Nationaliteit=" + anv_Cd_Nationaliteit + ", Gender=" + Gender + ", MarritalStatus="
				+ MarritalStatus + ", City=" + City + ", LegitimationIssue=" + LegitimationIssue + ", toBIC=" + toBIC
				+ ", SocialSecurityNumber=" + SocialSecurityNumber + ", anv_In_Roken=" + anv_In_Roken
				+ ", naw_Om_Voorvoegsel=" + naw_Om_Voorvoegsel + ", anv_Cd_BurgelijkeStaat=" + anv_Cd_BurgelijkeStaat
				+ ", naw_Cd_Geslacht=" + naw_Cd_Geslacht + "]";
	}

}
