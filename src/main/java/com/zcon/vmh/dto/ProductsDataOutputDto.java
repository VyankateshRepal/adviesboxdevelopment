package com.zcon.vmh.dto;

import java.util.Arrays;

public class ProductsDataOutputDto {

	private ProductsDataOutputList[] productsDataOutputList;

	public ProductsDataOutputList[] getProductsDataOutputList() {
		return productsDataOutputList;
	}

	public void setProductsDataOutputList(ProductsDataOutputList[] productsDataOutputList) {
		this.productsDataOutputList = productsDataOutputList;
	}

	@Override
	public String toString() {
		return "ProductsDataOutputDto [productsDataOutputList=" + Arrays.toString(productsDataOutputList) + "]";
	}

}
