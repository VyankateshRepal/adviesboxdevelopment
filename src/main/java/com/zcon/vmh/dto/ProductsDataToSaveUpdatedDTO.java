package com.zcon.vmh.dto;

import java.util.List;

public class ProductsDataToSaveUpdatedDTO {

	private List<ProductsListUpdated> productsListUpdated;

	private String userID;

	private String contactID;

	private String databaseName;

	private String divisionId;

	private String clientRoleId;

	private String userName;

	private String serverName;

	private String password;

	private String clientId;

	private String userLoginDetailId;

	private String nameFirstInsurer;
	
	 private String isActive;
	 private String opportunityId;
	 private String productServiceSubjectCategory;
	 private String productCategory;
	 
	 private String productSubCategory;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getContactID() {
		return contactID;
	}

	public void setContactID(String contactID) {
		this.contactID = contactID;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}

	public String getClientRoleId() {
		return clientRoleId;
	}

	public void setClientRoleId(String clientRoleId) {
		this.clientRoleId = clientRoleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUserLoginDetailId() {
		return userLoginDetailId;
	}

	public void setUserLoginDetailId(String userLoginDetailId) {
		this.userLoginDetailId = userLoginDetailId;
	}

	public String getNameFirstInsurer() {
		return nameFirstInsurer;
	}

	public void setNameFirstInsurer(String nameFirstInsurer) {
		this.nameFirstInsurer = nameFirstInsurer;
	}

	public List<ProductsListUpdated> getProductsListUpdated() {
		return productsListUpdated;
	}

	public void setProductsListUpdated(List<ProductsListUpdated> productsListUpdated) {
		this.productsListUpdated = productsListUpdated;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getOpportunityId() {
		return opportunityId;
	}

	public void setOpportunityId(String opportunityId) {
		this.opportunityId = opportunityId;
	}

	public String getProductServiceSubjectCategory() {
		return productServiceSubjectCategory;
	}

	public void setProductServiceSubjectCategory(String productServiceSubjectCategory) {
		this.productServiceSubjectCategory = productServiceSubjectCategory;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductSubCategory() {
		return productSubCategory;
	}

	public void setProductSubCategory(String productSubCategory) {
		this.productSubCategory = productSubCategory;
	}

	@Override
	public String toString() {
		return "ProductsDataToSaveUpdatedDTO [productsListUpdated=" + productsListUpdated + ", userID=" + userID
				+ ", contactID=" + contactID + ", databaseName=" + databaseName + ", divisionId=" + divisionId
				+ ", clientRoleId=" + clientRoleId + ", userName=" + userName + ", serverName=" + serverName
				+ ", password=" + password + ", clientId=" + clientId + ", userLoginDetailId=" + userLoginDetailId
				+ ", nameFirstInsurer=" + nameFirstInsurer + ", isActive=" + isActive + ", opportunityId="
				+ opportunityId + ", productServiceSubjectCategory=" + productServiceSubjectCategory
				+ ", productCategory=" + productCategory + ", productSubCategory=" + productSubCategory + "]";
	}



	



	

}
